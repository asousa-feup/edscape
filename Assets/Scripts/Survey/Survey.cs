using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Survey : MonoBehaviour
{
    private SurveyData surveyObj;
    private GameSettings _gameSettings;
    private string gameDataFileName;
    public GameObject prefabLoadPanel;
    private GameObject _loadPanel;
    private bool _finalSurvey = false;

    public GameObject webviewPanel;

    private void Awake()
    {
        _loadPanel = Instantiate(prefabLoadPanel, new Vector3(0, 0, 0), Quaternion.identity, FindObjectOfType<Canvas>().transform);
        _loadPanel.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        
        _finalSurvey = PlayerPrefs.GetInt("finalSurvey") == 1;

        if (!_finalSurvey) // Initial survey
        {
            PlayerPrefs.SetInt("finalSurvey", 1);
            gameDataFileName = PlayerPrefs.GetString("initialSurveyPath");
        }
        else // Final survey
        {
            gameDataFileName = PlayerPrefs.GetString("finalSurveyPath");
        }
        LoadGameData();

    }

    private void LoadGameData()
    {
        // Check if the survey path is an URL
        if (IsPathAnURL(gameDataFileName)){
            webviewPanel.SetActive(true);
            webviewPanel.transform.Find("Browser2D").GetComponent<SimpleWebBrowser.WebBrowser2D>().Navigate(gameDataFileName);
            Destroy(_loadPanel);
        }else{
            //path where is the file
            string filePath = Path.Combine(Application.streamingAssetsPath, gameDataFileName);
            StartCoroutine(GetFile(filePath));
        }
    }

    // Function that return true if the path is an url, and false otherwise
    bool IsPathAnURL(string path){
        Uri uriResult;
        // First we test if it is possible to create an URI from the path
        // Second and third we test if it has an http or https scheme
        return Uri.TryCreate(path, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
    }

    IEnumerator GetFile(string filePath)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(filePath))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            var dataAsJson = webRequest.downloadHandler.text;

            //verification
            try
            {
                //conversion from json to game data
                SurveyData loadedData = JsonUtility.FromJson<SurveyData>(dataAsJson);
                surveyObj = loadedData;
                FindObjectOfType<SurveyController>().SetSurveyData(surveyObj, _loadPanel, _finalSurvey);
            }
            catch
            {
                Debug.LogError("N�o foi possivel carregar dados!");
            }
        }
    }

    public SurveyData GetSurveyData ()
    {
        return surveyObj;
    }

    public void Submit(){
        if (!_finalSurvey)
            SceneManager.LoadScene("Room");
        else
        {
            GameRoomController _gameRoomController = FindObjectOfType<GameRoomController>();
            _gameRoomController.SetEndApplicationPanel();
            _gameRoomController.ResetEventSystemInRoom();
            SceneManager.UnloadSceneAsync("Survey");
        }
    }
}

[Serializable] public class SurveyData 
{
    public SurveyQuestions[] questions;
    public string[] openQuestions;
}

[Serializable] public class SurveyQuestions
{
    public string questionText;
    public string[] answers;
    public string selectedAnswer;
    public int selectedAnswerIdx;
}
