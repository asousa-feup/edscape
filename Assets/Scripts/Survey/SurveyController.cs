using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class SurveyController : MonoBehaviour
{
    public GameObject panel;
    public GameObject answersPanel;
    public GameObject answerContentArea;
    public GameObject inputAnswerPanel;
    public Transform answerToggleParent;
    public TextMeshProUGUI inputAnswer;
    public Text questionText;
    public Text numbQuestion;
    public GameObject toggleTemplate;
    public GameObject submitPanel;
    public GameObject submitBtn;
    public GameObject nextBtn;
    public GameObject previousBtn;
    public TMP_InputField openInputField;

    //answer buttons
    private List<GameObject> answerButtonGameObjects = new List<GameObject>();

    private SurveyData surveyData;
    private SurveyQuestions[] questions;
    [SerializeField] private int currentQuestion;
    [SerializeField] private int qtdQuestions;  //openquestions + choicequestions
    private string[] openAnswer ;
    [SerializeField] private int qtdOpenQuestion = 0;
    [SerializeField] private int qtdChoiseQuestion = 0;
    private GameObject _loadPanel;
    private bool _finalSurvey;
    private bool first = true;

    private string url = "https://web.fe.up.pt/~efeupinho/web_badges/unity/unitySurvey.php";
    private string url_legacy = "https://web.fe.up.pt/~efeupinho/web_badges/unity/unitySurveyLegacy.php";
    private string userEmail;
    private string survey_id;
    private string[] survey_legacy_id; 
    /*
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    void OnMouseEnter()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }

    void OnMouseExit()
    {
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }*/

    public void Awake()
    {
        userEmail = PlayerPrefs.GetString("userEmail");
    }
    
    private void Update()
    {   
        // If the survey data was not assigned
        if(surveyData == null)
            return;

        if((surveyData.openQuestions == null) || ((surveyData.openQuestions != null) && (currentQuestion < qtdChoiseQuestion)))
        {
            bool isAnswered = ToggleisOn();
            if (isAnswered)
            {
                nextBtn.GetComponent<Button>().interactable = true;
            }
            else
            {
                nextBtn.GetComponent<Button>().interactable = false;
            }
        }
        else
        {
            if(openInputField.text != "")
            {
                nextBtn.GetComponent<Button>().interactable = true;
            }
            else
            {
                nextBtn.GetComponent<Button>().interactable = false;
            }
        }

        bool allAnswered=false;
        for(int i = 0; i < qtdQuestions; i++)
        {
            if (i < qtdChoiseQuestion)
            {
                if (questions[i].selectedAnswer == "")
                {
                    allAnswered = false;
                    submitBtn.GetComponent<Button>().interactable = false;
                    break;
                }
                else
                {
                    allAnswered = true;
                }
            }
            else
            {
                if (openAnswer[i-qtdChoiseQuestion] == null || openAnswer[i - qtdChoiseQuestion] == "")
                {           
                    allAnswered = false;
                    submitBtn.GetComponent<Button>().interactable = false;
                    break;
                }
                else
                {
                    allAnswered = true;
                }
            }

        }

        if (allAnswered)
        {
            submitBtn.GetComponent<Button>().interactable = true;
        }       
    }

    public void SetSurveyData(SurveyData surveyObj, GameObject loadPanel, bool finalSurvey)
    {
        _finalSurvey = finalSurvey;
        _loadPanel = loadPanel;
        surveyData = surveyObj;
        questions = surveyData.questions;

        foreach (SurveyQuestions q in questions)
        {
            q.selectedAnswerIdx = -1;
            q.selectedAnswer = "";
        }

        currentQuestion = 0;
        
        Debug.Log(surveyData.openQuestions);
        if(surveyData.openQuestions == null)
        {
            qtdChoiseQuestion = questions.Length;
            qtdQuestions = questions.Length;
        }
        else
        {
            qtdChoiseQuestion = questions.Length;
            qtdOpenQuestion = surveyData.openQuestions.Length;
            qtdQuestions = qtdChoiseQuestion + qtdOpenQuestion;
            openAnswer = new string[qtdOpenQuestion];
        }

        survey_legacy_id = new string[qtdQuestions];

        toggleTemplate.SetActive(false);
        panel.SetActive(false);
        submitPanel.SetActive(false);
        
        Destroy(_loadPanel);
        
        ShowQuestion();
    }
    
    private void ToggleAnswers()
    {

        var idx = -1;

        for(int i=0; i<questions[currentQuestion].answers.Length; i++)
        {

            if (questions[currentQuestion].selectedAnswer != "")
            {
                idx = questions[currentQuestion].selectedAnswerIdx;
                Debug.Log("selected answer id: " + idx);
            }
            GameObject answerOption = Instantiate(toggleTemplate, answerToggleParent, false);

            answerOption.SetActive(true);
            answerOption.GetComponent<Toggle>().enabled = true;
            if(idx == i)
            {
                answerOption.GetComponent<Toggle>().isOn = true;
            }
            else
            {
                answerOption.GetComponent<Toggle>().isOn = false;
            }
           
            answerOption.transform.Find("Label").GetComponent<Text>().enabled = true;
            answerOption.transform.Find("Label").GetComponent<Text>().text = questions[currentQuestion].answers[i];
            answerOption.transform.Find("Background").GetComponent<Image>().enabled = true;

        }
    }

    private void RemoveAnswerButtons()
    {
        foreach (Transform child in answerContentArea.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    private void ShowQuestion()
    {
        if(currentQuestion +1 == qtdQuestions)
        {
            nextBtn.SetActive(false);
        }
        else
        {
            nextBtn.SetActive(true);
        }

        if(currentQuestion == 0)
        {
            previousBtn.SetActive(false);
        }
        else
        {
            previousBtn.SetActive(true);
        }

        RemoveAnswerButtons();
        numbQuestion.text = currentQuestion + 1 + "/" + qtdQuestions;
        if (currentQuestion < qtdChoiseQuestion)
        {
            answersPanel.SetActive(true);
            inputAnswerPanel.SetActive(false);
            // string[] aux = questions[currentQuestion].questionText.Split('"');
            //questionText.text = aux[0] + "<b>\"" + aux[1] + "\"<b>" + aux[2]; ;
            questionText.text = questions[currentQuestion].questionText;
            Debug.Log(questions[currentQuestion].questionText);
            ToggleAnswers();
        }
        else 
        {
            questionText.text = surveyData.openQuestions[currentQuestion-qtdChoiseQuestion];
            answersPanel.SetActive(false);
            inputAnswerPanel.SetActive(true);
       
            openInputField.text = "";
            openInputField.text = openAnswer[currentQuestion - qtdChoiseQuestion];
        }
    }

    private bool ToggleisOn()
    {
        bool selected = false;
        if (answerContentArea.GetComponent<ToggleGroup>() != null)
        {
            for (int i = 0; i < answerContentArea.transform.childCount; i++)
            {
                if (answerContentArea.transform.GetChild(i).GetComponent<Toggle>().isOn)
                {
                    questions[currentQuestion].selectedAnswer = answerContentArea.transform.GetChild(i).Find("Label").GetComponent<Text>().text;
                    questions[currentQuestion].selectedAnswerIdx = i;
                    selected = true;
                    Debug.Log(currentQuestion + ":" + questions[currentQuestion].selectedAnswerIdx);
                    break;
                }
            }
            if (!selected)
            {
                questions[currentQuestion].selectedAnswer = "";
                questions[currentQuestion].selectedAnswerIdx = -1;
            }
        }
        return selected;
    }

    public void ReadOpenAnswer(string s)
    {
        openAnswer[currentQuestion-qtdChoiseQuestion] = s;
        Debug.Log(openAnswer);
    }

    public void nextQuestion()
    {
        if(currentQuestion < qtdChoiseQuestion)
        {
            ToggleisOn();
        }
        if (first)
        {
            StartCoroutine(SendSurvey(getSurvey(), true, false, getCurrentQuestion(), getCurrentAnswer(),currentQuestion));
            first = false;
        }
        else
        {
            StartCoroutine(SendSurvey(getSurvey(), false, false, getCurrentQuestion(), getCurrentAnswer(),currentQuestion));
        }
        currentQuestion++;
        ShowQuestion();

    }

    public void previousQuestion()
    {
        if(currentQuestion > 0) 
        {
            if (currentQuestion < qtdChoiseQuestion)
            {
                ToggleisOn();
            }
            StartCoroutine(SendSurvey(getSurvey(),false,false, getCurrentQuestion(), getCurrentAnswer(),currentQuestion));
            currentQuestion--;
            ShowQuestion();
        }
    }

    private string getCurrentQuestion()
    {
        string s = "";

        if(currentQuestion < qtdChoiseQuestion)
        {
            s = questions[currentQuestion].questionText;
        }
        else
        {
            s = surveyData.openQuestions[currentQuestion - qtdChoiseQuestion];
        }

        return s;
    }

    private string getCurrentAnswer()
    {
        string s = "";

        if(currentQuestion < qtdChoiseQuestion)
        {
            s = questions[currentQuestion].selectedAnswer;
        }
        else
        {
            s = openAnswer[currentQuestion - qtdChoiseQuestion];
        }
        return s;
    }

    private string getSurvey()
    {
        string s = "";

        for (int i = 0; i < questions.Length; i++)
        {
            s = s + questions[i].questionText + "\n";
            s = s + questions[i].selectedAnswer + "\n\n";
        }

        for (int i=0; i < qtdOpenQuestion; i++)
        {
            s = s + surveyData.openQuestions[i] + "\n";
            s = s + openAnswer[i] + "\n\n";
        }
        

        return s;
    }
    public void submitButton()
    {

        if (currentQuestion < qtdChoiseQuestion)
        {
            ToggleisOn();
        }

        string s = getSurvey();
        
        StartCoroutine(SendSurvey(s,false,true, getCurrentQuestion(), getCurrentAnswer(),currentQuestion));

        StartCoroutine(WaitTime(0.5f));
    }

    IEnumerator WaitTime(float time)
    {
        submitPanel.SetActive(true);
        yield return new WaitForSeconds(time);
        submitPanel.SetActive(false);
    }

    IEnumerator SendOneQuestion(string question, string response,int currentQ)
    {
        WWWForm form = new WWWForm();
        form.AddField("survey_id", survey_id);
        form.AddField("question", question);
        form.AddField("response", response);
        form.AddField("numb", currentQ+1);

        Debug.Log("survey_id:" + survey_id + ", question:" + question + ", response: " + response + ", numb:" + (currentQ+1));
        if (survey_legacy_id.Length.Equals(0) || string.IsNullOrEmpty(survey_legacy_id[currentQ]))
        {
            form.AddField("id", -1);
            Debug.Log("legacy_id: -1");
        }
        else
        {
            form.AddField("id", survey_legacy_id[currentQ]);
            Debug.Log("legacy_id: " + survey_legacy_id[currentQ] + ", numb: " + (currentQ+1));
        }
        
        form.AddField("issuers", PlayerPrefs.GetString("issuers"));
        form.AddField("game_version", PlayerPrefs.GetString("game_version"));
        
        using (UnityWebRequest www = UnityWebRequest.Post(url_legacy, form))
        {
            Debug.Log("result:" + www.result);
            Debug.Log("uri:" + www.uri);

            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();
            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.Log(www.error);
            }
            else
            {
                string handler = www.downloadHandler.text;

                string[] aux = handler.Split('|');
                if (aux[1].Equals("success"))
                {
                    survey_legacy_id[currentQ] = aux[2];
                    Debug.Log("question:" + currentQuestion +" , survey_legacy_id: " + survey_legacy_id[currentQ]);
                }

                Debug.Log("one question handler: " + handler);
            }
        }
    }
    IEnumerator SendSurvey(string s,bool first, bool submit, string q, string a, int numb)           
    {
        WWWForm form = new WWWForm();
        form.AddField("user", userEmail);
        form.AddField("survey", s);
        if (first)
        {
            form.AddField("first", "true");
        }
        else
        {
            form.AddField("survey_id", survey_id);
        }
        
        form.AddField("issuers", PlayerPrefs.GetString("issuers"));
        form.AddField("game_version", PlayerPrefs.GetString("game_version"));
       
        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            //Debug.Log("result:" + www.result);
            //Debug.Log("uri:" + www.uri);

            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();

            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.Log(www.error);
            }
            else
            {
                string handler = www.downloadHandler.text;


                string[] aux = handler.Split('|');
                if (aux[1].Equals("success"))
                {
                    survey_id = aux[2];
                    Debug.Log("survey_id: " + survey_id);
                }
                
                //Debug.Log("handler: " + handler);

                if (submit)
                {
                    GetComponent<Survey>().Submit();
                }

                StartCoroutine(SendOneQuestion(q, a,numb));

            }
        }
        
    }
    
}
