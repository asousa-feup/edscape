using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class URLController : MiniGameController
{
    [SerializeField] private GameObject webviewPanel;

    private string url;

    private new void Awake()
    {
        base.Awake();
        url = _gameController.GetMiniGamePath();
        LoadGameData();
    }

    private void LoadGameData()
    {
        // Check if the survey path is an URL
        if (IsPathAnURL(url)){
            webviewPanel.SetActive(true);
            webviewPanel.transform.Find("Browser2D").GetComponent<SimpleWebBrowser.WebBrowser2D>().Navigate(url);
        }
    }

    // Function that return true if the path is an url, and false otherwise
    bool IsPathAnURL(string path){
        Uri uriResult;
        // First we test if it is possible to create an URI from the path
        // Second and third we test if it has an http or https scheme
        return Uri.TryCreate(path, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
    }

    void Start(){
        DestroyLoadCanvas();
    }
    
    // Function that is called when the user clicks on exit button
    public void Exit()
    {
        SetMiniGameInfo(true, 0, false, false, 0, 0);
        ReturnToRoom();
    }

    // Function that is called when the user clicks on continue button
    public void Continue()
    {
        SetMiniGameInfo(false, 0, true, false, 0, 0);
        ReturnToRoom();
    }

    protected override void ReturnToRoom()
    {
        SceneManager.UnloadSceneAsync("URL");
    }
}
