using System;
using System.Collections;
using System.Collections.Generic;
using Paroxe.PdfRenderer;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PDFController : MiniGameController
{
    public PDFViewer pdfViewer;
    private bool _done = false;
    public GameObject zoomButton;
    
    private new void Awake()
    {
        #if UNITY_WEBGL
            zoomButton.SetActive(false);
        #else
            zoomButton.SetActive(true);
        #endif
        
        
        base.Awake();
        
        // Load PDF
        StartCoroutine(GetPDF( Application.streamingAssetsPath + "/" + _gameController.GetMiniGamePath()));
    }

    IEnumerator GetPDF(string filePath)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(filePath))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            var data = webRequest.downloadHandler.data;
            
            try
            {
                var asset = ScriptableObject.CreateInstance<PDFAsset>();
                asset.Load(data);
                pdfViewer.LoadDocumentFromAsset(asset);
            }
            catch
            {
                Debug.LogError("Não foi possivel carregar o PDF.");
            }
        }
    }
    
    // Update is called once per frame
    private new void Update()
    {
        base.Update();

        if (!_done && pdfViewer.IsLoaded)
        {
            _gameController.DestroyLoadPanel(canvas);
            _done = true;
        }


    }

    public void ReturnToRoom()
    {
        StartCoroutine(Wait());
    }
    
    IEnumerator Wait()
    {
        _gameController.SuspendClues(true);
        yield return new WaitForSeconds(0.5F);
        _gameController.SuspendClues(false);
        
        _gameController.SetRoomTime(_timeInSec);
        SceneManager.UnloadSceneAsync("PDFViewer");
        PlayerPrefs.SetInt("inMainGame", 1);
    }

}
