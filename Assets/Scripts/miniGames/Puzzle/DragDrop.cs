using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragDrop : MonoBehaviour
{
    //delegate is basically a reference type that can hold the reference to a method instead of some primitive types or object
    public delegate SlotItem DragEndedDelagate(DragDrop draggableObject);
    public Camera camera;

    public DragEndedDelagate dragEndedCallBack;


    [SerializeField] private bool isPlaced = false;
    private bool isDragged = false;
    private SlotItem placedSlotItem;

    private Vector3 mouseDragStartPosition;
    private Vector3 spriteDragStartPosition;
    private Vector3 spriteDragPosition;

    private bool _onMouseDown= false;
    private bool _onMouseUp = false;
    
    private void Awake()
    {
        spriteDragStartPosition = transform.position;
    }

    public void SetDragStartPosition(Vector3 vec)
    {
        spriteDragStartPosition = vec;
    }

    private void OnMouseDown()
    {
        //Debug.Log("OnMouseDown");
        isDragged = true;
        _onMouseDown = true;
        
        mouseDragStartPosition = camera.ScreenToWorldPoint(Input.mousePosition);
        spriteDragPosition = transform.position;
        spriteDragPosition.z = 0;
    }

    private void OnMouseDrag()
    {
        
        //Debug.Log("OnMouseDrag");
        if (isPlaced)
        {
            _onMouseUp = false;
            //Debug.Log(name + "+++++++START PLACED :)))) +++++++++");
            isPlaced = false;
            placedSlotItem.SetSlotItem(false,null);
            placedSlotItem = null;
        }else{
            //Debug.Log(name + "+++++++START+++++++++");
        }
        
        if (isDragged)
        {
            //transform.position = spriteDragStartPosition + (Camera.main.ScreenToWorldPoint(Input.mousePosition) - mouseDragStartPosition);
            var vec = camera.ScreenToWorldPoint(Input.mousePosition); ;
            transform.position = spriteDragPosition + (vec - mouseDragStartPosition);
            var tmp = transform.position;
            tmp.z = 0;
            transform.position = tmp;
        }
    }

    private void OnMouseUp()
    {
        if (_onMouseDown)
        {
            //Debug.Log(name + "+++++++ENDS+++++++++");
            _onMouseDown = false;
            _onMouseUp = true;
            isDragged = false;
            placedSlotItem = dragEndedCallBack(this);
            //Debug.Log(name + "placed in" + placedSlotItem);
            if(placedSlotItem != null)
            {
                isPlaced = true;
            }
            /* else
             {
                 transform.position = spriteDragStartPosition;
             }*/
        }

    }

    public void GoOriginalPosition()
    {
        //Debug.Log(name + "go original position..");
        if(isPlaced){
            isPlaced = false;
            placedSlotItem.SetSlotItem(false,null);
            placedSlotItem = null;
        }
        transform.position = spriteDragStartPosition;
        var tmp = transform.position;
        tmp.z = 0;
        transform.position = tmp;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        throw new System.NotImplementedException();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        throw new System.NotImplementedException();
    }
    
}
