using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotItem : MonoBehaviour
{
    [SerializeField] private bool isPlaced;
    [SerializeField] private bool isCorrect;
    [SerializeField] private DragDrop placedObject;
    [SerializeField] private DragDrop correctObject;


    private void Start()
    {
        isPlaced = false;
        isCorrect = false;
        placedObject = null;
    }

    public void SetSlotItem(bool value, DragDrop placedObject)
    {
        //Debug.Log(name + "---é placed: " + value);
        isPlaced = value;
        this.placedObject = placedObject;
        SetIsCorrect();
    }

    public bool GetIsPlaced()
    {
        return isPlaced;
    }

    public DragDrop GetPlacedObject()
    {
        return placedObject;
    }

    public DragDrop GetCorrectObject()
    {
        return correctObject;
    }

    private void SetIsCorrect()
    {

        if(placedObject == correctObject)
        {
            //Debug.Log(name + "---é correta...");
            isCorrect = true;
        }
        else
        {
            //Debug.Log(name + "---é falsa...");
            isCorrect = false;
        }
    }

    public bool GetIsCorrect()
    {
        return isCorrect;
    }

   
}
