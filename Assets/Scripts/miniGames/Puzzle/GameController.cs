using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using TMPro;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;


public class GameController : MiniGameController
{
    public PuzzleController puzzle;
    public Text timerText;
    public Button endButton;
    public Button hintButton;
    public GameObject gamePanel;
    public GameObject endPanel;
    public GameObject slots;
    public GameObject items;
    public Slider timerSlider;
    public Image sliderFill;
    public int rightPieces;
    private int hintUsed;
    private int hintPenalty;
    private int score;
    private int penaltyTimeFail;
    private int penaltyTimeExit;
    private int npieces = 16;

    [SerializeField] private float timeRemaining;  //time in seg
    [SerializeField] private float gameTimer;
    private bool isActive;

    //checks if the position has already been used or not 
    List<int> usedPosition = new List<int>();
    private string randomPuzzlePositionFile;
    private List<Position> piecesPositions;

    private bool _exit = true;
    
    public Color32 timeColor = new Color32(0xDB, 0xBB, 0x1A, 0xFF);

    private new void Awake()
    {
        base.Awake();
        randomPuzzlePositionFile = _gameController.GetMiniGamePath();
        LoadGameData();
    }

    private new void Update()
    {
        base.Update();
        
        if (isActive)
        {
            if (timeRemaining <= 5)
            {
                sliderFill.GetComponent<Image>().color = Color.Lerp(timeColor, Color.red, 1);
            }
            else
            {
                sliderFill.GetComponent<Image>().color = Color.Lerp(timeColor, Color.red, 0);
            }

            timeRemaining -= Time.deltaTime;
            timerSlider.value = timeRemaining;
            timerText.text = Mathf.Round(timeRemaining).ToString();

            if (timeRemaining <= 0 || isAllRight())
            {
                isActive = false;
                EndGame();
            }
        }
    }
    private void LoadGameData()
    {
        //path where is the file
        string filePath = Application.streamingAssetsPath +"/"+ randomPuzzlePositionFile;
        StartCoroutine(GetFile(filePath));
        //Debug.Log(filePath);

    }
    
    IEnumerator GetFile(string filePath)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(filePath))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            var dataAsJson = webRequest.downloadHandler.text;

            //verification
            try
            {
                //conversion from json to game data
                PuzzleConfiguration loadedData = JsonUtility.FromJson<PuzzleConfiguration>(dataAsJson);
                _miniGameData = loadedData;
                SetFeedbacksButtons();
                piecesPositions = loadedData.puzzlePositions;
                gameTimer = (float) loadedData.gameTimer;
                //Debug.Log("game: " + gameTimer);
                hintPenalty = loadedData.pointsRemovedForAskingHint;
                penaltyTimeFail = loadedData.penaltyTimeFail;
                penaltyTimeExit = loadedData.penaltyTimeExit;
                SetImage(loadedData.imagePath);

            }
            catch
            {
                Debug.LogError("Não foi possivel carregar dados!");
            }
        }
    }

    private void SetImage(string loadedDataImagePath)
    {
        // Load image
        StartCoroutine(GetImage( Application.streamingAssetsPath + "/" + loadedDataImagePath));

    }

    IEnumerator GetImage(string filePath)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(filePath))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            var data = webRequest.downloadHandler.data;

            try
            {
                Texture2D tex = new Texture2D(2, 2);
                tex.LoadImage(data);
                Sprite fromTex = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height),
                    new Vector2(0.5f, 0.5f), 100.0f);

                foreach (var piece in puzzle.GetPuzzleItems())
                {
                    // Access image inside piece
                    var renders = piece.gameObject.GetComponentsInChildren<SpriteRenderer>();
                    // Returns self first
                    renders[1].sprite = fromTex;
                }

                RandomPieces();

                slots.SetActive(false);
                items.SetActive(false);

                isActive = false;
                hintUsed = 0;
                timeRemaining = gameTimer;
                timerSlider.maxValue = gameTimer;
                timerSlider.value = gameTimer;

                SetIntros();
            }
            catch
            {
                Debug.LogError("Não foi possivel carregar imagem para o puzzle");
            }


        }
    }
    
    public override void StartGame()
    {
        isActive = true;
        gamePanel.SetActive(true);
        slots.SetActive(true);
        items.SetActive(true);
    }

    private void RandomPieces()
    {
        for(int i= 0; i< puzzle.GetPuzzleItems().Count; i++)
        {
            int random = Random.Range(0, puzzle.GetPuzzleItems().Count);

            //for no repeat
            while (usedPosition.Contains(random))
            {
                random = Random.Range(0, puzzle.GetPuzzleItems().Count);
            }

            puzzle.GetPuzzleItems()[i].transform.position = new Vector3(piecesPositions[random].x, piecesPositions[random].y, -1);
            puzzle.GetPuzzleItems()[i].SetDragStartPosition(new Vector3(piecesPositions[random].x, piecesPositions[random].y, -1));
            usedPosition.Add(random);
        }
    }
    
    public void HintRemoveWrongsAnswer()
    {
        DragDrop item;
        hintUsed++;
        score -= hintPenalty;

        foreach (SlotItem slotItem in puzzle.GetSlotItems())
        {
            if (slotItem.GetIsPlaced())
            {
                if (!slotItem.GetIsCorrect())
                {
                    item = slotItem.GetPlacedObject();
                    //Debug.Log(slotItem.name + " goOriginalPosition");
                    item.GoOriginalPosition();
                }
            }
        }

    }
    public void Replay()
    {
        isActive = true;
        timeRemaining = gameTimer;
        gamePanel.SetActive(true);
        slots.SetActive(true);
        items.SetActive(true);
        endPanel.SetActive(false);
    }
    
    public void Exit()
    {
        // Set final intros if win 
        if (score == 100 - hintUsed * hintPenalty)
            SetFinalIntros();
        else
            ReturnToRoom();
    }
    
    protected override void ReturnToRoom()
    {
        SetMiniGameInfo(_exit, penaltyTimeExit, score == 100, score == 100 - hintUsed * hintPenalty, _miniGameData.penaltyTimeHelpNeeded, penaltyTimeFail);
        SceneManager.UnloadSceneAsync("Puzzle");
    }
    
    private bool isAllRight()
    {
        int count = GetCount();
        rightPieces = count;
        if(count == puzzle.GetSlotItems().Count)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    private int GetCount()
    {
        int count = 0;
        foreach(SlotItem slot in puzzle.GetSlotItems())
        {
            if (slot.GetIsCorrect())
            {
                count++;
            }
        }

        return count;
    }
    
    public void EndGame()
    {
        _exit = false;
            
        int count = GetCount();
        score = (count * 100 / 16) - (hintUsed * hintPenalty);
        
        int wrongAnswers = 16 - count;
        UpdateFeedbackAndScore(score, 100, hintUsed, wrongAnswers, rightPieces, 0);

        /*gamePanel.SetActive(false);
        slots.SetActive(false);
        items.SetActive(false);*/
        endPanel.SetActive(true);
    }
}
