using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PuzzleConfiguration : MiniGameData
{
    
    public string imagePath;
    public int gameTimer;
    public List<Position> puzzlePositions;
}

[Serializable]
public class Position
{
    public float x;
    public float y;
}