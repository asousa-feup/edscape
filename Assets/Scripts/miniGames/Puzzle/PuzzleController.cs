using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleController : MonoBehaviour
{

    public List<SlotItem> slotItems;
    public List<DragDrop> puzzleItems;
    public float snapRange = 1.0f;


    void Start()
    {
        foreach(DragDrop draggeble in puzzleItems)
        {
            draggeble.dragEndedCallBack = OnDragEnded;
        }
    }

    public List<SlotItem> GetSlotItems()
    {
        return slotItems;
    }

    public List<DragDrop> GetPuzzleItems()
    {
        return puzzleItems;
    }

    //find the snap point that is closest to this draggable and if the distance is smaller than the snapRange then update the position of the draggable
    private SlotItem OnDragEnded(DragDrop draggable)
    {
        float closesDistance = -1;
        Transform closesSlotItem = null;
        SlotItem placedSlotItem = null;
        DragDrop previousPlacedSlotItem;

        foreach (SlotItem slotItem in slotItems)
        {
            float currentDistance = Vector2.Distance(draggable.transform.position, slotItem.transform.position);
            if (closesSlotItem == null || currentDistance < closesDistance)
            {
                closesSlotItem = slotItem.transform;
                closesDistance = currentDistance;
                placedSlotItem = slotItem;
            }
        }
        
        //Debug.Log(draggable.name + "vai associar...");

        if(closesSlotItem != null && closesDistance <= snapRange)
        {
            draggable.transform.position = new Vector3(closesSlotItem.position.x, closesSlotItem.position.y, -1);
            if (placedSlotItem.GetIsPlaced() == true)
            {
                //Debug.Log(draggable.name + "tinha lá outra peça..");
                previousPlacedSlotItem = placedSlotItem.GetPlacedObject();
                previousPlacedSlotItem.GoOriginalPosition();
            }
            //Debug.Log(draggable.name + "associou..");
            placedSlotItem.SetSlotItem(true,draggable);
            return placedSlotItem;

        }
        
        return null;
    }
}
