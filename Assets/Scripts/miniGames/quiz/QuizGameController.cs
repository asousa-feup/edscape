using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class QuizGameController : MiniGameController
{
    public TextMeshProUGUI questionText;
    public Text timerText;
    public Text finalScoreText;
    public Text finalFeedbackText;
    public Text hintText;
    public Slider timerSlider;
    public Image sliderFill;
    public Button hintButton;


    public SimpleObjectPool answerButtonObjectPool;
    public Transform answerButtonParent;
    public GameObject questionPanel;
    public GameObject roundEndPanel;
    public GameObject feedbackPanel;
    public GameObject UIPanel;

    private DataController dataController;
    private RoundData currentRoundData;
    private QuestionData[] questionPool;
    private AnswerButton[] answerPool;
    

    //private bool wrongAnswer;
    private bool isRoundActive = false;
    private float timeRemaining = -350.0f;
    private int questionIndex;
    private int playerScore;
    private int passScore;
    private bool isCorrect;
    private int hintCount;
    private int hintPenalty;
    private int wrongAnswerCount;
    private int rightAnswerCount;

    private int penaltyTimeFail;
    private int penaltyTimeExit;
    private Text _slideTime;
    
    public Color32 timeColor = new Color32(0xDB, 0xBB, 0x1A, 0xFF);
    public Color32 activeBackground = new Color32(182, 214, 140, 255);
    public Color32 feedbackBackground = new Color32(106, 125, 80, 255);


    //checks if the question has already been used or not 
    List<int> usedValeus = new List<int>();

    //answer button
    private List<GameObject> answerButtonGameObjects = new List<GameObject>();
    
    private bool _exited = false;
    
    private new void Awake()
    {
        base.Awake();
        _slideTime = timerSlider.GetComponentInChildren<Text>();
        roundEndPanel.SetActive(false);
        feedbackPanel.SetActive(false);
        timerSlider.interactable = false;
    }


    void Start()
    {
        dataController = FindObjectOfType<DataController>();
        currentRoundData = dataController.GetCurrentRoundData();
        _miniGameData = dataController.GetGameData();
        SetFeedbacksButtons();
        questionPool = currentRoundData.questions;
        timeRemaining = currentRoundData.timeLimitInSeconds;
        timerSlider.value = currentRoundData.timeLimitInSeconds;
        timerSlider.maxValue = currentRoundData.timeLimitInSeconds; 
        hintPenalty= _miniGameData.pointsRemovedForAskingHint;
        penaltyTimeFail = _miniGameData.penaltyTimeFail;
        penaltyTimeExit = _miniGameData.penaltyTimeExit;
        passScore = questionPool.Length * currentRoundData.pointsAddedForCorrectAnswer;

            
        UpdateTimeRemainingDisplay();


        playerScore = 0;
        questionIndex = 0;
        hintCount = 0;
        wrongAnswerCount = 0;
        rightAnswerCount = 0;

        SetIntros();
        
        
    }
    
    public void Replay()
    {
        _exited = false;
        roundEndPanel.SetActive(false);
        feedbackPanel.SetActive(false);
        timeRemaining = currentRoundData.timeLimitInSeconds;
        timerSlider.value = currentRoundData.timeLimitInSeconds;
        UpdateTimeRemainingDisplay();
        playerScore = 0;
        questionIndex = 0;
        hintCount = 0;
        wrongAnswerCount = 0;
        rightAnswerCount = 0;
        finalScoreText.text = playerScore.ToString() + "/" + passScore;
        usedValeus = new List<int>();
        StartGame();
    }

    private void UpdateTimeRemainingDisplay()
    {
        _slideTime.text = ((int) timeRemaining).ToString();
    }

    // Update is called once per frame
    private new void Update()
    {
        base.Update();

        if (isRoundActive && timeRemaining > -300.0f)
        {
            if (timeRemaining <= 5) 
            {
                sliderFill.GetComponent<Image>().color = Color.Lerp(timeColor, Color.red, 1);
            }
            else
            {
                sliderFill.GetComponent<Image>().color = Color.Lerp(timeColor, Color.red, 0);
            }

               
            if(feedbackPanel.activeSelf){ }    
            else
            {
                timeRemaining -= Time.deltaTime;
                timerSlider.value = timeRemaining;
                UpdateTimeRemainingDisplay();
            }
            
            if (timeRemaining <= 0)
            {
                EndRound();
            }

        }
    }

    private void ShowQuestion()
    {
        RemoveAnswerButtons();
        hintButton.interactable = true;

        int random = Random.Range(0, questionPool.Length);

        //for no repeat
        while (usedValeus.Contains(random))
        {
            random = Random.Range(0, questionPool.Length);
        }
        
        QuestionData questionData = questionPool[questionIndex];
        usedValeus.Add(random);
        questionText.text = questionData.questionText;

        answerPool = new AnswerButton[questionData.answers.Length];
        
        for (int i = 0; i < questionData.answers.Length; i++)
        {
            GameObject answerButtonGameObject = answerButtonObjectPool.GetObject(answerButtonParent);
            answerButtonGameObjects.Add(answerButtonGameObject);
            
            //answerButtonGameObject.transform.SetParent(answerButtonParent);
           
            AnswerButton answerButton = answerButtonGameObject.GetComponent<AnswerButton>();
            answerButton.Setup(questionData.answers[i]);
            answerButton.answerButton.interactable = true;
            answerPool[i] = answerButton;
        }

        isRoundActive = true;
    }

    //Remove buttons from previous question
    private void RemoveAnswerButtons()
    {
        while (answerButtonGameObjects.Count > 0)
        {
            answerButtonObjectPool.ReturnObject(answerButtonGameObjects[0]);
            answerButtonGameObjects.RemoveAt(0);
        }
    }
    
    public void BackButtonClicked()
    {

        for (int i = 0; i < answerPool.Length; i++)
        {
            if (answerPool[i].GetIsClicked())
            {
                answerPool[i].answerButton.interactable = false;
            }
            else
            {
                answerPool[i].answerButton.interactable = true;
            }
           
        }
        feedbackPanel.SetActive(false);
        questionPanel.GetComponent<Image>().color = activeBackground;
 
         if (isCorrect)
         {
             NextQuestion();
         }
    }

    public void NextQuestion()
    {
        this.isCorrect = false;

        if (questionPool.Length > questionIndex + 1)
        {
            questionIndex++;
            ShowQuestion();
        }
        else
        {
            EndRound();
        }
    }

    public void AnswerButtonClicked(Button answerButton, bool isCorrect, string feedback, Text answerText)
    {
        // Send information to textLog
        _gameController.SendInformationJustToBd("[Mini game " + _gameController.GetMiniGamePath() + "] Answer: " + answerText.text + ", Correct: " + isCorrect + ", Feedback: " + feedback + ".");

        for (int i = 0; i < answerPool.Length; i++)
        {
            answerPool[i].answerButton.interactable = false;
        }

        if (!isCorrect)
        {
            wrongAnswerCount++;
            questionPanel.GetComponent<Image>().color = feedbackBackground;
            feedbackPanel.SetActive(true);
            finalFeedbackText.text = feedback;
            playerScore += currentRoundData.pointsAddedForIncorrectAnswer; 
            //scoreText.text = "Score: " + playerScore.ToString();
            finalScoreText.text = playerScore.ToString() + "/" + passScore;

        }
        else
        {
            rightAnswerCount++;
            questionPanel.GetComponent<Image>().color = feedbackBackground; 
            feedbackPanel.SetActive(true);
            finalFeedbackText.text = feedback;
            
            playerScore += currentRoundData.pointsAddedForCorrectAnswer; 
            //scoreText.text = "Score: " + playerScore.ToString();
            finalScoreText.text = playerScore.ToString() + "/" + passScore;
            this.isCorrect = true;
        }

    }

    public void HintButtonClicked()
    {
        hintButton.interactable = false;
        hintCount++;
        playerScore -= hintPenalty;
        finalScoreText.text = playerScore.ToString() + "/" + passScore;
        foreach(AnswerButton button in answerPool)
        {
            if (!button.GetIsClicked() && !button.GetIsCorrect())
            {
                button.SetIsClicked(true);
                button.answerButton.interactable = false;
                break;
            }
        }
    }

    public void ExitButtonClicked()
    {
        _exited = true;
        ReturnToMenu();
    }
    
    public void EndRound()
    {
        isRoundActive = false;

        dataController.SendNewHighScore(playerScore);

        UpdateFeedbackAndScore(playerScore, passScore, hintCount, wrongAnswerCount , rightAnswerCount, currentRoundData.pointsAddedForIncorrectAnswer);
        
        questionPanel.SetActive(false);
        UIPanel.SetActive(false);
        roundEndPanel.SetActive(true);
    }
    
    public void ReturnToMenu()
    {
        // Set final intros if win 
        if (playerScore == passScore - hintCount * hintPenalty + wrongAnswerCount * currentRoundData.pointsAddedForIncorrectAnswer)
            SetFinalIntros();
        else
            ReturnToRoom();
    }
    
    protected override void ReturnToRoom()
    {
        SetMiniGameInfo(_exited, penaltyTimeExit, playerScore == passScore, playerScore == passScore - hintCount * hintPenalty + wrongAnswerCount * currentRoundData.pointsAddedForIncorrectAnswer, _miniGameData.penaltyTimeHelpNeeded, penaltyTimeFail);
        SceneManager.UnloadSceneAsync("Quiz");
        SceneManager.UnloadSceneAsync("QuizGame");
    }
       

    public override void StartGame()
    {
        UIPanel.SetActive(true);
        questionPanel.SetActive(true);
        ShowQuestion();
        isCorrect = false;
        //wrongAnswer = false;
    }
}
