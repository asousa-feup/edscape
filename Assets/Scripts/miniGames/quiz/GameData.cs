using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
//Convert xml/json/yaml to games data
public class GameData : MiniGameData
{
    public RoundData[] allRoundData;
}
