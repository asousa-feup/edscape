using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using UnityEngine.Video;

public class MenuController : MonoBehaviour
{
    public GameObject introPanel;
    private DataController data;
    
    private double _timeInSec = 0;
    public TextMeshProUGUI time;
    private GameRoomController _gameController;

    public Canvas canvas;

    // Start is called before the first frame update
    void Start()
    {
        data = FindObjectOfType<DataController>();
        introPanel.SetActive(false);
        _gameController = FindObjectOfType<GameRoomController>();
        //_gameController.CreateLoadPanel(canvas);
        _timeInSec = _gameController.GetRoomTime();
    }

    private void Update()
    {
        // Update game time
        _timeInSec -= Time.deltaTime;
        UpdateTime();
    }
    
    private void UpdateTime()
    {
        if (_timeInSec < 0)
            _timeInSec = 0;

        var hours = Math.Floor(_timeInSec/3600f);
        var minutes = Math.Floor((_timeInSec%3600f)/60f);
        var seconds = Math.Floor(_timeInSec % 60f);

        time.text = hours + ":" + minutes + ":" + seconds;
    }
    
    public void StartGame(int round)
    {
        data.SetRoundData(round);
        _gameController.SetRoomTime(_timeInSec);
        SceneManager.UnloadSceneAsync("Quiz");
        SceneManager.UnloadSceneAsync("Menu");
        SceneManager.LoadScene("Game", LoadSceneMode.Additive);
    }

    public void IntroButton()
    {
        introPanel.SetActive(true);
        // Todo: carregar video
        introPanel.GetComponent<VideoPlayer>().url = Path.Combine(Application.streamingAssetsPath, "Videos/VideoCountdown.mp4");
        //_gameController.DestroyLoadPanel(canvas);
    }

    public void BackButton()
    {
        introPanel.SetActive(false);
    }
}
