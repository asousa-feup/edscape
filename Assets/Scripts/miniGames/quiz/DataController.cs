using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.Networking;


public class DataController : MonoBehaviour
{

    private RoundData[] allRoundData;

    private int roundIndex;
    private int playerHighScore;
    private GameRoomController _gameController;
    private string gameDataFileName;

    public Canvas canvas;

    private GameData _quizObj;
    
    private void Awake()
    {
        _gameController = FindObjectOfType<GameRoomController>();
        _gameController.CreateLoadPanel(canvas);
        gameDataFileName = _gameController.GetMiniGamePath();
    }
    
    // Start is called before the first frame update
    void Start()
    {
        LoadPlayerProgress();
        LoadGameData();

    }

    public void SetRoundData(int round)
    {
        roundIndex = round;
    }


    public RoundData GetCurrentRoundData()
    {
        return allRoundData[roundIndex];
    }
    
    public GameData GetGameData()
    {
        return _quizObj;
    }
    

    private void LoadGameData()
    {
        //path where is the file
        string filePath = Path.Combine(Application.streamingAssetsPath, gameDataFileName);
        StartCoroutine(GetFile(filePath));
    }
    
    IEnumerator GetFile(string filePath)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(filePath))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            var dataAsJson = webRequest.downloadHandler.text;
            
            //verification
            try
            {
                //conversion from json to game data
                GameData loadedData = JsonUtility.FromJson<GameData>(dataAsJson);
                _quizObj = loadedData;
                allRoundData = loadedData.allRoundData;
                
                SceneManager.LoadScene("QuizGame", LoadSceneMode.Additive);
                _gameController.DestroyLoadPanel(canvas);
                

            }
            catch
            {
                Debug.LogError("Não foi possivel carregar dados!");
            }
        }
    }

    public void SendNewHighScore(int newScore)
    {
        if(newScore > playerHighScore)
        {
            playerHighScore = newScore;
            SavePlayerProgress();
        }
    }


    public int GetHighScore()
    {
        return playerHighScore;
    }


    //load saved data
    private void LoadPlayerProgress()
    {
        if (PlayerPrefs.HasKey("highScore"))
        {
            playerHighScore = PlayerPrefs.GetInt("highScore");
        }
    }


    private void SavePlayerProgress()
    {
        PlayerPrefs.SetInt("highScore", playerHighScore);
    }
}
