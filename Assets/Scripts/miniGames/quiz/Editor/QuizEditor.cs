using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class QuizEditor : EditorWindow 
{
    public GameData gameData;

    private string gameDataFilePath = "/StreamingAssets/data.json";

    //create a window "Game Data Editor/Quiz"
    [MenuItem("Window/Game Data Editor/Quiz")]
    static void Init()
    {
        QuizEditor windows = (QuizEditor)EditorWindow.GetWindow(typeof(QuizEditor));
        windows.Show();
    }

    //Create load and save buttons on "Game Data Editor" window
    private void OnGUI()
    {
        if(gameData != null)
        {
            SerializedObject serializedObject = new SerializedObject(this);
            SerializedProperty serializedProperty = serializedObject.FindProperty("gameData");

            EditorGUILayout.PropertyField(serializedProperty, true);

            serializedObject.ApplyModifiedProperties();

            if(GUILayout.Button("Save Data"))
            {
                SaveGameData();
            }
        }

        if(GUILayout.Button("Load Data"))
        {
            LoadGameData();
        }
    }

    private void LoadGameData()
    {
        string filePath = Application.dataPath + gameDataFilePath;

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            gameData = JsonUtility.FromJson<GameData>(dataAsJson);
        }
        else
        {
            gameData = new GameData();
        }
    }

    private void SaveGameData()
    {
        //conversion from game data to json
        string dataAsJson = JsonUtility.ToJson(gameData);

        string filePath = Application.dataPath + gameDataFilePath;

        //if file is already exists it subscribes else it creates a new one
        File.WriteAllText(filePath, dataAsJson);
    }
}
