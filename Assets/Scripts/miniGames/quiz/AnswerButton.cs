using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class AnswerButton : MonoBehaviour
{
    public Text answerText;
    public Button answerButton;
    private AnswerData answerData;

    private bool isClicked;

    private QuizGameController gameController;

    // Start is called before the first frame update
    void Start()
    {
        gameController = FindObjectOfType<QuizGameController>();
    }


    public void Setup(AnswerData data)
    {
        answerData = data;
        answerText.text = answerData.answerText;
        isClicked = false;
    }

    public void HandleClick()
    {
        isClicked = true;
        gameController.AnswerButtonClicked(answerButton,answerData.isCorrect, answerData.feedback, answerText);
    }

    public bool GetIsClicked()
    {
        return isClicked;
    }

    public void SetIsClicked(bool value)
    {
        isClicked = value; 
    }

    public bool GetIsCorrect()
    {
        return answerData.isCorrect;
    }

}
