using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HiddenText : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject tipPanel;

    private void Awake()
    {
        tipPanel.SetActive(false);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        tipPanel.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tipPanel.SetActive(false);
    }
}
