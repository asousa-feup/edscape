using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LabelPuzzleController : MonoBehaviour
{
    public GameObject GamePanel;
    public GameObject EndPanel;
    public Text scoreText;
    public Text timerText;

    // [SerializeField] private float snapRange = 15.0f;
    [SerializeField] private float timeRemaining;
    [SerializeField] private int correctPoint;
    private bool isActive;
    private int score;

    


    void Start()
    {
        /*foreach (DragDrop draggeble in draggableObjects)
        {
            draggeble.dragEndedCallBack = OnDragEnded;
        }*/

        isActive = true;
        score = 0;
    }

    void Update()
    {
        if (isActive)
        {
            Debug.Log("time: " + timeRemaining);
            timeRemaining -= Time.deltaTime;
            timerText.text = "Time: " + Mathf.Round(timeRemaining).ToString();

            if (timeRemaining <= 0)
            {
                EndRound();
            }

        }
    }
    public void EndRound()
    {
        isActive = false;

        scoreText.text = "Score: " + score.ToString();

        GamePanel.SetActive(false);
        EndPanel.SetActive(true);
    }

    public void SetGameTimer(int time)
    {
        timeRemaining = (float)time;
    }

    public void SetCorrectPoint(int point)
    {
        correctPoint = point;
    }

   

}
