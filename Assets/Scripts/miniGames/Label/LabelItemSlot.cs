using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class LabelItemSlot : MonoBehaviour, IDropHandler
{
    [SerializeField] private bool isPlaced;
    [SerializeField] private LabelDragDrop placedObject;
    [SerializeField] private Vector2 position;

    [SerializeField] private bool isCorrect;
    [SerializeField] private LabelDragDrop correctItem;
    


    private void Start()
    {
        isPlaced = false;
        isCorrect = false;
        placedObject = null;
        position = GetComponent<RectTransform>().anchoredPosition;
    }

    public void OnDrop(PointerEventData eventData)
    {
        //Debug.Log("OnDrop");
        
        if(eventData.pointerDrag != null) 
        {
           //eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
        }

    }

    public void SetItemSlotInfo(bool value, LabelDragDrop placedObject, bool isCorrect)
    {
        isPlaced = value;
        this.placedObject = placedObject;
        this.isCorrect = isCorrect;
    }

    public bool GetIsPlaced()
    {
        return isPlaced;
    }

    public LabelDragDrop GetPlacedObject()
    {
        return placedObject;
    }

    public LabelDragDrop GetCorrectItem()
    {
        return correctItem;
    }

    public void SetCorrectItem(LabelDragDrop dragDrop)
    {
        correctItem = dragDrop;
    }
    
    public void SetIsCorrect(bool value)
    {
        isCorrect = value;
    }

    public bool GetIsCorrect()
    {
        return isCorrect;
    }

    public void SetPosition(Vector2 slotJsonPosition)
    {
        position = slotJsonPosition;
    }
}
