using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class LabelDragDrop : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    //delegate is basically a reference type that can hold the reference to a method instead of some primitive types or object
    public delegate void DragEndedDelagate(LabelDragDrop draggableObject);

    public DragEndedDelagate dragEndedCallBack;

    [SerializeField] private Canvas canvas;
    [SerializeField] private bool isPlaced;
    [SerializeField] private LabelDragDrop Item;
    private bool isDragged;
    private LabelItemSlot placedItemSlot;

    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;

    private Vector2 spriteDragStartPosition;
    public string name;

    private void Awake()
    {
        isDragged = isPlaced = false;
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        spriteDragStartPosition = rectTransform.anchoredPosition;
    }

    public void SetStartPosition(Vector2 vec)
    {
        spriteDragStartPosition = vec;
    }

    public void GoOriginalPosition()
    {
        if(isPlaced){
            isPlaced = false;
            placedItemSlot.SetItemSlotInfo(false, null, false);
            placedItemSlot = null;
        }
        rectTransform.anchoredPosition = spriteDragStartPosition;
    }
    

    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
    {
        //Debug.Log("OnBeginDrag");
        isDragged = true;
        canvasGroup.alpha = 0.8f;
        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (isPlaced)
        {
            isPlaced = false;
            placedItemSlot.SetItemSlotInfo(false, null,false);
            placedItemSlot = null;
        }

        if (isDragged)
        {
            rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //Debug.Log("OnEndDrag");
        isDragged = false;
        dragEndedCallBack(this);
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;

        if (placedItemSlot == null)
        {
            rectTransform.anchoredPosition = spriteDragStartPosition;
        }
        else
        {
            isPlaced = true;
        }
    }

    public void SetPlacedItemSlot(LabelItemSlot itemSlot)
    {
        placedItemSlot = itemSlot;
    }

    public LabelDragDrop GetItem()
    {
        return Item;
    }
    
    public void SetCanvas(Canvas c)
    {
        canvas = c;
    }

    public bool IsBeingDragged()
    {
        return isDragged;
    }
}
