using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using static LabelConfigController;
using Random = UnityEngine.Random;

public class LabelGameController : MiniGameController
{
    public Text timerText;
    private List<LabelItemSlot> itemSlots = new List<LabelItemSlot>();
    private List<LabelDragDrop> draggableObjects = new List<LabelDragDrop>();
    private List<LabelDragDrop> correctPlaced = new List<LabelDragDrop>();
    public GameObject gamePanel;
    public GameObject endPanel;
    public Button endButton;
    public Button hintButton;
    public Slider timerSlider;
    public Image sliderFill;

    [SerializeField] private float snapRange = 50.0f;
    [SerializeField] private float timeRemaining;  //time in seg
    private float gameTimer;

    private bool isActive = false;
    [SerializeField] private int score;
    private int totalScore;
    private int correctPoint;
    private int hintPenalty;
    private int usedHint;

    private bool _exited = false;
    private ConfigGame _puzzleObj;
    
    public Color32 timeColor = new Color32(0xDB, 0xBB, 0x1A, 0xFF);

    // Json info
    [Header("Json info:")]
    public Image backgroundImage;
    public GameObject slotPrefab;
    public GameObject itemPrefab;
    public Transform slotsParent;
    public Transform itemsParent;
    private int _penaltyTimeFail = 0;
    private int _penaltyTimeExit = 0;

    private new void Awake()
    {
        base.Awake();
        LoadGameData();

    }
    
    void Start2()
    {
        foreach(LabelDragDrop draggeble in draggableObjects)
        {
            draggeble.dragEndedCallBack = OnDragEnded;
        }
        timeRemaining = gameTimer;
        totalScore = correctPoint * draggableObjects.Count;
        score = 0;
        usedHint = 0;

        SetIntros();
    }
    
    private void LoadGameData()
    {
        StartCoroutine(GetFile(Application.streamingAssetsPath +"/"+ _gameController.GetMiniGamePath()));
    }
    
    IEnumerator GetFile(string filePath)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(filePath))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            var dataAsJson = webRequest.downloadHandler.text;

            //verification
            try
            {
                ConfigGame loadedData = JsonUtility.FromJson<ConfigGame>(dataAsJson);
                _miniGameData = loadedData;
                _puzzleObj = loadedData;
                SetFeedbacksButtons();
                gameTimer = loadedData.timeLimitInSeconds;
                gamePanel.gameObject.GetComponentInChildren<Slider>().maxValue = gameTimer;
                correctPoint = loadedData.pointsAddedForCorrectAnswer;
                hintPenalty = loadedData.pointsRemovedForAskingHint;
                _penaltyTimeFail = loadedData.penaltyTimeFail;
                _penaltyTimeExit = loadedData.penaltyTimeExit;

                // Items
                var count = loadedData.puzzleItems.Count;
                var up = true;
                var initial_diff = Math.Abs(5f * count - 80f);
                var y = count % 2 == 0 ? -27.99855f -initial_diff/2 : -27.99855f;
                var position = new Vector2(250, y);
                var diff = initial_diff;
                
                for (int i = 0; i < loadedData.puzzleItems.Count; i++) {
                    puzzleItem temp = loadedData.puzzleItems[i];
                    int randomIndex = Random.Range(i, loadedData.puzzleItems.Count);
                    loadedData.puzzleItems[i] = loadedData.puzzleItems[randomIndex];
                    loadedData.puzzleItems[randomIndex] = temp;
                }
                
                foreach (var itemJson in loadedData.puzzleItems)
                {
                    var obj = Instantiate(itemPrefab, new Vector3(0, 0,0), Quaternion.identity, itemsParent);
                    obj.GetComponent<RectTransform>().anchoredPosition = position;
                    obj.GetComponentInChildren<Text>().text = itemJson.name;
                    var dragDrop = obj.GetComponent<LabelDragDrop>();
                    dragDrop.name = itemJson.name;
                    dragDrop.SetCanvas(canvas); 
                    dragDrop.SetStartPosition(position);
                    draggableObjects.Add(dragDrop);
                    
                    position = new Vector2(250, y + (up ? diff : -diff));

                    if (!up)
                        diff += initial_diff;

                    up = !up;
                }

                // Slots
                foreach (var slotJson in loadedData.puzzleSlots)
                {
                    var obj = Instantiate(slotPrefab, new Vector3(0, 0,0), Quaternion.identity, slotsParent);
                    obj.GetComponent<RectTransform>().anchoredPosition = slotJson.position;
                    var itemSlot = obj.GetComponent<LabelItemSlot>();
                    itemSlot.SetPosition(slotJson.position);
                    
                    // Add correct item to slot
                    foreach (var item in draggableObjects.Where(item => item.name.Equals(slotJson.correctItem)))
                    {
                        itemSlot.SetCorrectItem(item);
                    }
                    
                    itemSlots.Add(itemSlot);
                }

                SetImage(loadedData.imagePath);

            }
            catch
            {
                Debug.LogError("Não foi possivel carregar dados!");
            }
        }
    }
    
    private void SetImage(string loadedDataImagePath)
    {
        // Load image
        StartCoroutine(GetImage( Application.streamingAssetsPath + "/" + loadedDataImagePath));

    }

    IEnumerator GetImage(string filePath)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(filePath))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            var data = webRequest.downloadHandler.data;
            
            try
            {
                Texture2D tex = new Texture2D(2, 2);
                tex.LoadImage(data);
                Sprite fromTex = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                backgroundImage.sprite = fromTex;
            }
            catch
            {
                Debug.LogError("Não foi possivel carregar imagem para o label game");
            }
            
            Start2();
        }
    }

    private new void Update()
    {
        base.Update();

        if (isActive)
        {
            if (timeRemaining <= 5)
            {
                sliderFill.GetComponent<Image>().color = Color.Lerp(timeColor, Color.red, 1);
            }
            else
            {
                sliderFill.GetComponent<Image>().color = Color.Lerp(timeColor, Color.red, 0);
            }

            timeRemaining -= Time.deltaTime;
            timerSlider.value = timeRemaining;
            timerText.text = Mathf.Round(timeRemaining).ToString();
            
            //Debug.Log(score);

            if (timeRemaining <= 0 || score == totalScore || (usedHint != 0 && score == totalScore - usedHint*hintPenalty))
            {
                isActive = false;
                EndRound();
            }
        }
    }

    public override void StartGame()
    {
        isActive = true;
        gamePanel.SetActive(true);
    }
    
    public void Exit()
    {
        _exited = true;
        ReturnToMenu();
    }

    public void EndRound()
    {
        isActive = false;
        
        // Free if grabbing
        foreach(LabelDragDrop draggeble in draggableObjects)
        {
            if (draggeble.IsBeingDragged())
            {
                draggeble.OnEndDrag(null);
                //draggeble.GoOriginalPosition();
            }
        }
        
        int wrongAnswers = draggableObjects.Count - ((score + (usedHint * hintPenalty)) / correctPoint);
        UpdateFeedbackAndScore(score, totalScore, usedHint, wrongAnswers, draggableObjects.Count-wrongAnswers, 0);
        
        //gamePanel.SetActive(false);
        endPanel.SetActive(true);
    }

    public void Replay()
    {
        isActive = true;
        timeRemaining = gameTimer;
        gamePanel.SetActive(true);
        endPanel.SetActive(false);
    }

    public void HintRemoveWrongsAnswer()
    {
        LabelDragDrop item;

        usedHint++;
        score -= hintPenalty;

        foreach (LabelItemSlot itemSlot in itemSlots)
        {
            if (itemSlot.GetIsPlaced()) {
                if (!itemSlot.GetIsCorrect())
                {
                    item = itemSlot.GetPlacedObject();
                    item.GoOriginalPosition();
                }
            }
            
        }

    }

    //find the snap point that is closest to this draggable and if the distance is smaller than the snapRange then update the position of the draggable
    private void OnDragEnded(LabelDragDrop draggable)
    {
        float closesDistance = -1;
        RectTransform closesItemSlot = null;
        LabelItemSlot placedItemSlot = null;
        LabelDragDrop previousPlacedSnapPoint = null;
        
        /*if(!correctPlaced.Contains(draggable))
            correctPlaced.Add(draggable);
        else
        {
            //score -= correctPoint;
            //Debug.Log("desfez correto se correto.." + score);
            correctPlaced.Remove(draggable);
        }*/
        
        
        if (correctPlaced.Contains(draggable))
        {
            score -= correctPoint;
            correctPlaced.Remove(draggable);
            //Debug.Log(draggable.GetComponentInChildren<Text>().text + " remove from correctPlaced");

        }


        foreach (LabelItemSlot itemSlot in itemSlots)
        {
            
            float currentDistance = Vector2.Distance(draggable.GetComponent<RectTransform>().anchoredPosition, itemSlot.GetComponent<RectTransform>().anchoredPosition);

            //Debug.Log("draggable: " + draggable.GetComponent<RectTransform>().anchoredPosition + " |itemSlot:  " + itemSlot.GetComponent<RectTransform>().anchoredPosition);
            if (closesItemSlot == null || currentDistance < closesDistance)
            {
                closesItemSlot = itemSlot.GetComponent<RectTransform>();
                closesDistance = currentDistance;
                placedItemSlot = itemSlot;
            }
        }

        if (closesItemSlot != null && closesDistance <= snapRange)
        {
            draggable.GetComponent<RectTransform>().anchoredPosition = closesItemSlot.anchoredPosition;
            draggable.SetPlacedItemSlot(placedItemSlot);

            if (placedItemSlot.GetIsPlaced() == true)
            {
                //Debug.Log(draggable.GetComponentInChildren<Text>().text + " está lá peça....");
                previousPlacedSnapPoint = placedItemSlot.GetPlacedObject();
                if (placedItemSlot.GetIsCorrect())
                {
                    score -= correctPoint;
                    correctPlaced.Remove(previousPlacedSnapPoint);
                    //Debug.Log(previousPlacedSnapPoint.GetComponentInChildren<Text>().text + " remove from correctPlaced");
                    //Debug.Log(draggable.GetComponentInChildren<Text>().text + " undo correto.." + score);
                }
                previousPlacedSnapPoint.GoOriginalPosition();
            }

            if (placedItemSlot.GetCorrectItem() == draggable.GetItem())
            {
                placedItemSlot.SetItemSlotInfo(true, draggable, true);
                score += correctPoint;
                //Debug.Log(draggable.GetComponentInChildren<Text>().text + " add correto.." + score);
                if (!correctPlaced.Contains(draggable))
                {
                    correctPlaced.Add(draggable);
                    //Debug.Log(draggable.GetComponentInChildren<Text>().text + " add to correctPlaced");
                }

            }
            else
            {
                placedItemSlot.SetItemSlotInfo(true, draggable, false);
                //Debug.Log(draggable.GetComponentInChildren<Text>().text + " add peça errada.." + score);
            }
        }
    }

    public void ReturnToMenu()
    {
        // Set final intros if win 
        if (score == totalScore - usedHint * hintPenalty)
            SetFinalIntros();
        else
            ReturnToRoom();
    }

    protected override void ReturnToRoom()
    {
        SetMiniGameInfo(_exited, _penaltyTimeExit, score == totalScore, score == totalScore - usedHint*hintPenalty, _miniGameData.penaltyTimeHelpNeeded, _penaltyTimeFail);
        SceneManager.UnloadSceneAsync("LabelGame");
    }

}
