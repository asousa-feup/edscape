using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LabelConfigController : MonoBehaviour
{
    public LabelPuzzleController puzzleController;

    [System.Serializable]
    public class ConfigGame : MiniGameData
    {
        public string imagePath;
        public int pointsAddedForCorrectAnswer;
        public int pointsRemovedForAskingHint;
        public int timeLimitInSeconds;
        public List<puzzleItem> puzzleItems;
        public List<puzzleSlot> puzzleSlots;
    }

    [System.Serializable]
    public class puzzleItem
    {
        public string name;
    }

    [System.Serializable]
    public class puzzleSlot
    {
        public string correctItem;
        public Vector2 position;
    }
}

       