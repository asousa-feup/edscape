using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class MiniGameController : MonoBehaviour
{
    // Access room game
    protected GameRoomController _gameController;
    public TextMeshProUGUI time;
    protected double _timeInSec = 0;
    
    // Panels
    public Canvas canvas;
    private VideoManager _videoManager;
    private TextManager _textManager;
    public TextMeshProUGUI feedbackText;
    public Text scoreText;
    public Button replayButton;
    public GameObject hintsObj;
    public GameObject wrongAnswersObj;
    public GameObject rightAnswersObj;
    
    public Text hintFeedback;
    public Text exitFeedback;

    // Mini Game Itself
    protected MiniGameData _miniGameData;
    
    private bool _afterInitialIntros = false;

    protected void Awake()
    {
        _gameController = FindObjectOfType<GameRoomController>();
        _gameController.CreateLoadPanel(canvas);
        _videoManager = gameObject.GetComponent<VideoManager>();
        _textManager = gameObject.GetComponent<TextManager>();
        _timeInSec = _gameController.GetRoomTime();
    }

    public virtual void StartGame() { }

    protected void SetFeedbacksButtons()
    {
        hintFeedback.text = _miniGameData.hintButtonFeedback;
        exitFeedback.text = _miniGameData.exitButtonFeedback;
    }
        
    protected void Update()
    {
        // Update game time
        _timeInSec -= Time.deltaTime;
        UpdateTime();
    }
    
    protected void UpdateFeedbackAndScore(int score, int maxScore, int hintsUsed, int wrongAnswers, int rightAnswers, int penaltyWrongAnswers)
    {
        // For badges...
        _gameController.AddHintsCount(hintsUsed);
        
        replayButton.gameObject.SetActive(false);
        hintsObj.SetActive(false);
        wrongAnswersObj.SetActive(false);
        rightAnswersObj.SetActive(false);

        _gameController.SendInformationJustToBd("[Mini game " + _gameController.GetMiniGamePath() + "] Score: " + score + ", MaxScore: " + maxScore + 
                                        ", HintsUsed: " + hintsUsed + ", WrongAnswers: " + wrongAnswers +
                                        ", RightAnswers: " + rightAnswers + ".");
        
        //Change scores to 100 proportion
        scoreText.text = (int)(score*100/maxScore) + "/" + 100;

        if (score == maxScore)
        {
            feedbackText.text = _miniGameData.endGameSucceededFeedbackText;
            return;
        }
        if (score == maxScore - hintsUsed * _miniGameData.pointsRemovedForAskingHint + wrongAnswers * penaltyWrongAnswers)
            feedbackText.text = _miniGameData.endGameSucceededWithHelpFeedbackText;
        else
        {
            feedbackText.text = _miniGameData.endGameFailedFeedbackText;
            replayButton.gameObject.SetActive(true);
        }
        
        hintsObj.SetActive(true);
        hintsObj.GetComponentInChildren<Text>().text = hintsUsed.ToString();
        wrongAnswersObj.SetActive(true);
        wrongAnswersObj.GetComponentInChildren<Text>().text = wrongAnswers.ToString();
        rightAnswersObj.SetActive(true);
        rightAnswersObj.GetComponentInChildren<Text>().text = rightAnswers.ToString();
    }


    private void UpdateTime()
    {
        if (_timeInSec < 0)
            _timeInSec = 0;

        var hours = Math.Floor(_timeInSec/3600f);
        var minutes = Math.Floor((_timeInSec%3600f)/60f);
        var seconds = Math.Floor(_timeInSec % 60f);

        time.text = (hours < 10 ? "0" + hours : hours.ToString()) + ":" + 
                    (minutes < 10 ? "0" + minutes : minutes.ToString()) + ":" +
                    (seconds < 10 ? "0" + seconds : seconds.ToString());
    }

    private bool StartIntros(string videoPath, string title, string text)
    {
        var asText = !string.IsNullOrEmpty(title) || !string.IsNullOrEmpty(text);
        var asVideo = !string.IsNullOrEmpty(videoPath);
        
        // Prepare video if associated
        if (asVideo)
            _videoManager.PrepareIntroduction(videoPath, null, this);
        
        // Show text if associated
        if (asText)
        {
            _textManager.ShowText(title, text, asVideo, false, null, this);
            return true;
        }
        // Show just video associated
        if (asVideo)
        {
            _videoManager.StartVideo(false);
            return true;
        }
        
        return false;
    }
    
    protected void SetIntros()
    {
        // Start game if not intros
        if (!StartIntros(_miniGameData.videoPath, _miniGameData.introTitle, _miniGameData.introText))
        {
            DestroyLoadCanvas();
            InteractAfterIntros();
        }
    }
    
    protected void SetFinalIntros()
    {
        // Continue to room if no final intros
        if (!StartIntros(_miniGameData.finalVideoPathIfWin, _miniGameData.finalIntroTitleIfWin,
            _miniGameData.finalIntroTextIfWin))
            InteractAfterIntros();
    }
    
    
    protected void StopText()
    {
        _textManager.StopShowingText();
        /*if (string.IsNullOrEmpty(_miniGameData.videoPath))
            InteractAfterIntros();*/
    }
    
    protected void StopVideo()
    { 
        // Inside video manager already starts game
        _videoManager.StopVideo();
    }
    

    public void DestroyLoadCanvas()
    {
        _gameController.DestroyLoadPanel(canvas);
    }

    public void InteractAfterIntros()
    {
        if (_afterInitialIntros)
        {
            InteractAfterFinalIntros();
            return;
        }
        _afterInitialIntros = true;
        StartGame();
    }

    private void InteractAfterFinalIntros()
    {
        _afterInitialIntros = false;
        
        InputSystem.EnableDevice(Mouse.current);
        _gameController.SuspendClues(false);
        
        ReturnToRoom();
    }

    protected virtual void ReturnToRoom() { }

    protected void SetMiniGameInfo(bool exited, int penaltyTimeExit, bool winWithoutHelp, bool winWithHelp, int penaltyTimeNeededHelp, int penaltyTimeFail)
    {
        PlayerPrefs.SetInt("hintsUsed", 0);
        PlayerPrefs.SetInt("exitButton", 0);
        
        int penalty;
        var reason = "";
        
        if (exited) 
        {
            reason = "exited -> exit penalty";
            penalty = penaltyTimeExit;
            PlayerPrefs.SetInt("exitButton", 1);
        }
        else if (winWithoutHelp) 
        {
            reason = "won without help -> adds object to inventory";
            penalty = 0;
            PlayerPrefs.SetInt("exitButton", 0);
        }
        else if (winWithHelp) 
        {
            reason = "won with help -> adds object to inventory + help needed penalty";
            PlayerPrefs.SetInt("hintsUsed", 1);
            penalty = penaltyTimeNeededHelp;
            PlayerPrefs.SetInt("exitButton", 0);
        }
        else 
        {
            reason = "time ended and answers are not correct -> fail penalty";
            penalty = penaltyTimeFail;
            PlayerPrefs.SetInt("exitButton", 2);
        }
        
        PlayerPrefs.SetInt("penalty", penalty);

        _gameController.SetRoomTime(_timeInSec);
        PlayerPrefs.SetInt("inMainGame", 1);
    }
}

[System.Serializable]
public class MiniGameData
{
    public string introTitle;
    public string introText;
    public string videoPath;
    public string finalIntroTitleIfWin;
    public string finalIntroTextIfWin;
    public string finalVideoPathIfWin;
    public int pointsRemovedForAskingHint;
    public int penaltyTimeFail;
    public int penaltyTimeExit;
    public int penaltyTimeHelpNeeded;
    public string endGameSucceededFeedbackText;
    public string endGameFailedFeedbackText;
    public string endGameSucceededWithHelpFeedbackText;
    public string hintButtonFeedback;
    public string exitButtonFeedback;
}

