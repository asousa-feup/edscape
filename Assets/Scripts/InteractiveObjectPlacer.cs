using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InteractiveObjectPlacer
{
    private static System.Random random = new System.Random((int) System.DateTime.Now.Ticks);
    private static PlacementArea randomPlacementArea = null;
    private static Dictionary<string,List<PlacementArea>> placementAreasForEachObject = new Dictionary<string,List<PlacementArea>>(){
        {
            "Book_1", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.5f, 0.849f, 4.384f}, // Minimum Initial Position
                    new float[] {0.752f, 0.849f, 4.742f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.752f, 0.849f, 5.175f}, // Minimum Initial Position
                    new float[] {0.752f, 0.849f, 5.175f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.5f, 0.849f, 6.641f}, // Minimum Initial Position
                    new float[] {1f, 0.849f, 6.641f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, -90f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.5f, 0.849f, 6.956f}, // Minimum Initial Position
                    new float[] {0.5f, 0.849f, 6.956f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, -90f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.5f, 0.849f, 7.246f}, // Minimum Initial Position
                    new float[] {0.5f, 0.849f, 7.903f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.765f, 0.849f, 7.903f}, // Minimum Initial Position
                    new float[] {0.765f, 0.849f, 7.903f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.743f, 0.849f, 8.274f}, // Minimum Initial Position
                    new float[] {5.974f, 0.849f, 8.522f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.275f, 0.849f, 5.914f}, // Minimum Initial Position
                    new float[] {8.5f, 0.849f, 6.027f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.971f, 0.849f, 4.458f}, // Minimum Initial Position
                    new float[] {8.249f, 0.849f, 4.802f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.432f, 0.849f, 4.847f}, // Minimum Initial Position
                    new float[] {8.432f, 0.849f, 4.847f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {1.423f, 0.623f, 7.357f}, // Minimum Initial Position
                    new float[] {1.423f, 0.623f, 7.357f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {1.345f, 0.623f, 4.716f}, // Minimum Initial Position
                    new float[] {1.345f, 0.623f, 4.716f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, -90f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.288f, 0.623f, 5.253f}, // Minimum Initial Position
                    new float[] {7.288f, 0.623f, 5.253f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.294f, 0.7153f, 4.069f}, // Minimum Initial Position
                    new float[] {8.294f, 0.7153f, 4.069f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.294f, 0.5147f, 6.273f}, // Minimum Initial Position
                    new float[] {8.294f, 0.5147f, 6.273f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.9141f, 1.533f, 8.3735f}, // Minimum Initial Position
                    new float[] {7.9873f, 1.533f, 8.3735f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f},  // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null) // Maximum Final Rotation
            }
        },
        {
            "Book_2", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.5f, 0.849f, 4.384f}, // Minimum Initial Position
                    new float[] {0.752f, 0.849f, 4.742f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.752f, 0.849f, 5.175f}, // Minimum Initial Position
                    new float[] {0.752f, 0.849f, 5.175f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.5f, 0.849f, 6.641f}, // Minimum Initial Position
                    new float[] {1f, 0.849f, 6.641f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, -90f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.5f, 0.849f, 6.956f}, // Minimum Initial Position
                    new float[] {0.5f, 0.849f, 6.956f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, -90f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.5f, 0.849f, 7.246f}, // Minimum Initial Position
                    new float[] {0.5f, 0.849f, 7.903f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.765f, 0.849f, 7.903f}, // Minimum Initial Position
                    new float[] {0.765f, 0.849f, 7.903f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.743f, 0.849f, 8.274f}, // Minimum Initial Position
                    new float[] {5.974f, 0.849f, 8.522f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.275f, 0.849f, 5.914f}, // Minimum Initial Position
                    new float[] {8.5f, 0.849f, 6.027f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.971f, 0.849f, 4.458f}, // Minimum Initial Position
                    new float[] {8.249f, 0.849f, 4.802f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.432f, 0.849f, 4.847f}, // Minimum Initial Position
                    new float[] {8.432f, 0.849f, 4.847f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {1.423f, 0.623f, 7.357f}, // Minimum Initial Position
                    new float[] {1.423f, 0.623f, 7.357f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {1.345f, 0.623f, 4.716f}, // Minimum Initial Position
                    new float[] {1.345f, 0.623f, 4.716f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, -90f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.288f, 0.623f, 5.253f}, // Minimum Initial Position
                    new float[] {7.288f, 0.623f, 5.253f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.294f, 0.7153f, 4.069f}, // Minimum Initial Position
                    new float[] {8.294f, 0.7153f, 4.069f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.294f, 0.5147f, 6.273f}, // Minimum Initial Position
                    new float[] {8.294f, 0.5147f, 6.273f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 0f},  // Minimum Initial Rotation
                    new float[] {90f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.9141f, 1.533f, 8.3735f}, // Minimum Initial Position
                    new float[] {7.9873f, 1.533f, 8.3735f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f},  // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null) // Maximum Final Rotation
            }
        },
        {
            "BookOpen", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.735f, 0.834f, 4.22f}, // Minimum Initial Position
                    new float[] {0.96f, 0.834f, 5.19f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 45f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 135f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.5f, 0.834f, 6.583f}, // Minimum Initial Position
                    new float[] {1.018f, 0.834f, 6.583f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.5f, 0.834f, 7.2f}, // Minimum Initial Position
                    new float[] {0.7f, 0.834f, 7.95f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 45f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 135f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {1.035f, 0.834f, 7.898f}, // Minimum Initial Position
                    new float[] {1.035f, 0.834f, 7.898f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.666f, 0.834f, 8.064f}, // Minimum Initial Position
                    new float[] {6f, 0.834f, 8.5f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 135f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 225f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.074f, 0.834f, 5.87f}, // Minimum Initial Position
                    new float[] {8.5f, 0.834f, 6.05f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -135f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -45f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.2925f, 0.834f, 5.325f}, // Minimum Initial Position
                    new float[] {8.2925f, 0.834f, 5.325f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.366f, 0.834f, 4.951f}, // Minimum Initial Position
                    new float[] {8.366f, 0.834f, 4.951f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.066f, 0.834f, 4.729f}, // Minimum Initial Position
                    new float[] {8.225f, 0.834f, 4.729f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -135f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -45f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.327f, 0.6861f, 4.168f}, // Minimum Initial Position
                    new float[] {8.327f, 0.6861f, 4.168f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null) // Maximum Final Rotation
            }
        },
        {
            "BooksHorizontal", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.361f, 0.8123f, 3.6117f}, // Minimum Initial Position
                    new float[] {0.361f, 0.8123f, 4.68f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.361f, 0.8123f, 6.342f}, // Minimum Initial Position
                    new float[] {0.361f, 0.8123f, 7.853f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.4069f, 0.8123f, 8.6718f}, // Minimum Initial Position
                    new float[] {5.9292f, 0.8123f, 8.6718f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 180f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 180f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.6595f, 0.8123f, 6.3362f}, // Minimum Initial Position
                    new float[] {8.6595f, 0.8123f, 5.907f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.6595f, 0.8123f, 5.1463f}, // Minimum Initial Position
                    new float[] {8.6595f, 0.8123f, 4.8085f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null) // Maximum Final Rotation
            }
        },
        {
            "BooksVertical", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.361f, 0.8123f, 3.83f}, // Minimum Initial Position
                    new float[] {0.361f, 0.8123f, 5.027f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.361f, 0.8123f, 6.5566f}, // Minimum Initial Position
                    new float[] {0.361f, 0.8123f, 8.2084f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.694f, 0.8123f, 8.216f}, // Minimum Initial Position
                    new float[] {6.009f, 0.8123f, 8.537f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.212f, 0.8123f, 5.899f}, // Minimum Initial Position
                    new float[] {8.552f, 0.8123f, 6.063f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.212f, 0.8123f, 4.717f}, // Minimum Initial Position
                    new float[] {8.212f, 0.8123f, 4.717f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.3467f, 0.6848f, 4.046f}, // Minimum Initial Position
                    new float[] {8.3467f, 0.6848f, 4.046f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 180f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 180f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.327f, 0.4848f, 6.4384f}, // Minimum Initial Position
                    new float[] {8.327f, 0.4848f, 6.4384f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null) // Maximum Final Rotation
            }
        },
        {
            "Calculator", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.7f, 0.8141f, 3.97f}, // Minimum Initial Position
                    new float[] {0.95f, 0.8141f, 5.412f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -135f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -45f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.956f, 0.8141f, 3.678f}, // Minimum Initial Position
                    new float[] {0.956f, 0.8141f, 3.678f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.302f, 0.8141f, 3.735f}, // Minimum Initial Position
                    new float[] {0.591f, 0.8141f, 3.735f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.298f, 0.8141f, 3.947f}, // Minimum Initial Position
                    new float[] {0.298f, 0.8141f, 4.979f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.32f, 0.8141f, 6.47f}, // Minimum Initial Position
                    new float[] {0.95f, 0.8141f, 6.65f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -135f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -45f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.822f, 0.8141f, 6.859f}, // Minimum Initial Position
                    new float[] {0.933f, 0.8141f, 6.859f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.32f, 0.8141f, 7.06f}, // Minimum Initial Position
                    new float[] {0.712f, 0.8141f, 8.08f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -135f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -45f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.948f, 0.8141f, 7.661f}, // Minimum Initial Position
                    new float[] {0.948f, 0.8141f, 8.129f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.552f, 0.8141f, 8.08f}, // Minimum Initial Position
                    new float[] {6.15f, 0.8141f, 8.719f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -45f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 45f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.06f, 0.8141f, 5.72f}, // Minimum Initial Position
                    new float[] {8.71f, 0.8141f, 6.206f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 45f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 135f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.387f, 0.8141f, 5.106f}, // Minimum Initial Position
                    new float[] {8.387f, 0.8141f, 5.527f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.41f, 0.8141f, 4.9f}, // Minimum Initial Position
                    new float[] {8.7092f, 0.8141f, 5.0303f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 45f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 135f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.06f, 0.8141f, 4.57f}, // Minimum Initial Position
                    new float[] {8.35f, 0.8141f, 4.881f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 45f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 135f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.422f, 0.6845f, 4.165f}, // Minimum Initial Position
                    new float[] {8.422f, 0.6845f, 4.165f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.423f, 0.484f, 6.363f}, // Minimum Initial Position
                    new float[] {8.423f, 0.484f, 6.363f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.5634f, 0.531f, 8.4835f}, // Minimum Initial Position
                    new float[] {7.5634f, 0.531f, 8.4835f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.6347f, 0.9056f, 8.4783f}, // Minimum Initial Position
                    new float[] {7.6347f, 0.9056f, 8.4783f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.3132f, 1.2807f, 8.4786f}, // Minimum Initial Position
                    new float[] {8.3132f, 1.2807f, 8.4786f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
        {
            "Coin", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.473f, 0.826f, 4.0275f}, // Minimum Initial Position
                    new float[] {1.034f, 0.826f, 5.466f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.205f, 0.826f, 3.634f}, // Minimum Initial Position
                    new float[] {0.666f, 0.826f, 3.8248f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.205f, 0.826f, 3.924f}, // Minimum Initial Position
                    new float[] {0.378f, 0.826f, 5.0232f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.2151f, 0.9623f, 5.141f}, // Minimum Initial Position
                    new float[] {0.364f, 0.9623f, 5.141f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.205f, 0.826f, 6.3645f}, // Minimum Initial Position
                    new float[] {1.034f, 0.826f, 6.7627f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.205f, 0.826f, 6.9633f}, // Minimum Initial Position
                    new float[] {0.8005f, 0.826f, 8.1927f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.865f, 0.826f, 7.5749f}, // Minimum Initial Position
                    new float[] {1.034f, 0.826f, 8.1927f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.4282f, 0.826f, 7.979f}, // Minimum Initial Position
                    new float[] {6.2653f, 0.826f, 8.8201f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.982f, 0.826f, 5.627f}, // Minimum Initial Position
                    new float[] {8.8056f, 0.826f, 6.3081f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.32f, 0.826f, 5.047f}, // Minimum Initial Position
                    new float[] {8.469f, 0.826f, 5.543f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.979f, 0.826f, 4.4761f}, // Minimum Initial Position
                    new float[] {8.479f, 0.826f, 4.9947f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.6728f, 0.826f, 4.4734f}, // Minimum Initial Position
                    new float[] {8.809f, 0.826f, 5.074f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.1971f, 0.6952f, 3.9399f}, // Minimum Initial Position
                    new float[] {8.6536f, 0.6952f, 4.3799f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.1963f, 0.4944f, 6.2811f}, // Minimum Initial Position
                    new float[] {8.653f, 0.4944f, 6.4383f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.5611f, 0.5433f, 8.4626f}, // Minimum Initial Position
                    new float[] {7.5611f, 0.5433f, 8.4626f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.638f, 0.9164f, 8.456f}, // Minimum Initial Position
                    new float[] {7.638f, 0.9164f, 8.456f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.4693f, 0.9164f, 8.4518f}, // Minimum Initial Position
                    new float[] {8.4693f, 0.9164f, 8.4518f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.2986f, 0.5413f, 8.4772f}, // Minimum Initial Position
                    new float[] {8.2986f, 0.5413f, 8.4772f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.488f, 1.2916f, 8.47f}, // Minimum Initial Position
                    new float[] {7.488f, 1.2916f, 8.47f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {6.6212f, 0.4491f, 8.4107f}, // Minimum Initial Position
                    new float[] {6.8778f, 0.4491f, 8.6791f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {6.446f, 0.01f, 8.731f}, // Minimum Initial Position
                    new float[] {6.446f, 0.01f, 8.731f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.09f, 0.01f, 8.369f}, // Minimum Initial Position
                    new float[] {7.09f, 0.01f, 8.731f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.8151f, 0.01f, 8.3861f}, // Minimum Initial Position
                    new float[] {8.8151f, 0.01f, 8.3861f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.764f, 0.01f, 6.361f}, // Minimum Initial Position
                    new float[] {8.764f, 0.01f, 6.361f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.764f, 0.01f, 4.171f}, // Minimum Initial Position
                    new float[] {8.764f, 0.01f, 4.171f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.2318f, 0.01f, 0.7691f}, // Minimum Initial Position
                    new float[] {0.2318f, 0.01f, 0.7691f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.641f, 0.01f, 0.276f}, // Minimum Initial Position
                    new float[] {0.641f, 0.01f, 0.276f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    null, // Minimum Initial Rotation
                    null, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
        {
            "Door", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {9f, 0f, 0.86f}, // Minimum Initial Position
                    new float[] {9f, 0f, 0.86f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Minimum Final Rotation
                    new float[] {0f, 0f, 0f}) // Maximum Final Rotation
            }
        },
        {
            "Flashlight", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.3182f, 0.8462f, 3.743f}, // Minimum Initial Position
                    new float[] {0.527f, 0.8462f, 3.743f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 180f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, 180f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.285f, 0.8462f, 3.743f}, // Minimum Initial Position
                    new float[] {0.285f, 0.8462f, 4.904f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.602f, 0.8462f, 4.1415f}, // Minimum Initial Position
                    new float[] {0.901f, 0.8462f, 5.326f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.343f, 0.8462f, 6.573f}, // Minimum Initial Position
                    new float[] {0.901f, 0.8462f, 6.573f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.901f, 0.8462f, 7.671f}, // Minimum Initial Position
                    new float[] {0.901f, 0.8462f, 8.076f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.5223f, 0.8462f, 8.087f}, // Minimum Initial Position
                    new float[] {6.163f, 0.8462f, 8.717f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.07f, 0.8462f, 5.724f}, // Minimum Initial Position
                    new float[] {8.709f, 0.8462f, 6.214f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.38f, 0.8462f, 5.148f}, // Minimum Initial Position
                    new float[] {8.38f, 0.8462f, 5.475f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.062f, 0.8462f, 4.57f}, // Minimum Initial Position
                    new float[] {8.386f, 0.8462f, 4.897f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.571f, 0.8462f, 4.931f}, // Minimum Initial Position
                    new float[] {8.571f, 0.8462f, 4.931f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.74f, 0.8462f, 4.583f}, // Minimum Initial Position
                    new float[] {8.74f, 0.8462f, 4.974f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.431f, 0.72f, 4.167f}, // Minimum Initial Position
                    new float[] {8.431f, 0.72f, 4.167f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.409f, 0.52f, 6.365f}, // Minimum Initial Position
                    new float[] {8.409f, 0.52f, 6.365f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.561f, 0.567f, 8.513f}, // Minimum Initial Position
                    new float[] {7.561f, 0.567f, 8.513f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.634f, 0.9419f, 8.513f}, // Minimum Initial Position
                    new float[] {7.634f, 0.9419f, 8.513f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.311f, 1.3164f, 8.513f}, // Minimum Initial Position
                    new float[] {8.311f, 1.3164f, 8.513f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.471f, 0.941f, 8.513f}, // Minimum Initial Position
                    new float[] {8.471f, 0.941f, 8.513f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, -90f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, -90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
        {
            "HardDrive", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.1801f, 0.8125f, 3.6891f}, // Minimum Initial Position
                    new float[] {0.562f, 0.8125f, 3.8494f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.901f, 0.8125f, 3.759f}, // Minimum Initial Position
                    new float[] {0.901f, 0.8125f, 3.759f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.221f, 0.8125f, 3.988f}, // Minimum Initial Position
                    new float[] {0.221f, 0.8125f, 5.043f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.589f, 0.8125f, 3.926f}, // Minimum Initial Position
                    new float[] {0.93f, 0.8125f, 5.483f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.2169f, 0.952f, 5.1777f}, // Minimum Initial Position
                    new float[] {0.2169f, 0.952f, 5.1777f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.1801f, 0.8125f, 6.409f}, // Minimum Initial Position
                    new float[] {0.93f, 0.8125f, 6.7898f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.854f, 0.8125f, 6.898f}, // Minimum Initial Position
                    new float[] {0.854f, 0.8125f, 6.898f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.1801f, 0.8125f, 7.0075f}, // Minimum Initial Position
                    new float[] {0.6869f, 0.8125f, 8.218f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.877f, 0.8125f, 7.6235f}, // Minimum Initial Position
                    new float[] {0.877f, 0.8125f, 8.218f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.484f, 0.8125f, 8.085f}, // Minimum Initial Position
                    new float[] {6.2924f, 0.8125f, 8.831f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.0769f, 0.8125f, 5.6009f}, // Minimum Initial Position
                    new float[] {8.8371f, 0.8125f, 6.255f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.453f, 0.8125f, 5.026f}, // Minimum Initial Position
                    new float[] {8.453f, 0.8125f, 5.496f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.0736f, 0.8125f, 4.4465f}, // Minimum Initial Position
                    new float[] {8.5036f, 0.8125f, 4.9413f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.66f, 0.8125f, 4.746f}, // Minimum Initial Position
                    new float[] {8.66f, 0.8125f, 4.9413f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.822f, 0.8125f, 4.4496f}, // Minimum Initial Position
                    new float[] {8.822f, 0.8125f, 5.077f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.628f, 0.8125f, 4.48f}, // Minimum Initial Position
                    new float[] {8.628f, 0.8125f, 4.48f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.685f, 0.8294f, 5.1943f}, // Minimum Initial Position
                    new float[] {8.685f, 0.8294f, 5.1943f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.685f, 0.8294f, 5.415f}, // Minimum Initial Position
                    new float[] {8.685f, 0.8294f, 5.415f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.7229f, 0.8294f, 5.409f}, // Minimum Initial Position
                    new float[] {8.7229f, 0.8294f, 5.409f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.2985f, 0.684f, 3.9175f}, // Minimum Initial Position
                    new float[] {8.674f, 0.684f, 4.3237f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.302f, 0.4849f, 6.2593f}, // Minimum Initial Position
                    new float[] {8.6709f, 0.4849f, 6.381f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.604f, 0.5322f, 8.547f}, // Minimum Initial Position
                    new float[] {7.604f, 0.5322f, 8.547f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.338f, 0.5322f, 8.547f}, // Minimum Initial Position
                    new float[] {8.338f, 0.5322f, 8.547f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.5087f, 0.9078f, 8.547f}, // Minimum Initial Position
                    new float[] {8.5087f, 0.9078f, 8.547f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.672f, 0.9078f, 8.547f}, // Minimum Initial Position
                    new float[] {7.672f, 0.9078f, 8.547f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.5265f, 1.2816f, 8.547f}, // Minimum Initial Position
                    new float[] {7.5265f, 1.2816f, 8.547f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.3501f, 1.2816f, 8.547f}, // Minimum Initial Position
                    new float[] {8.3501f, 1.2816f, 8.547f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
        {
            "Key", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.5695f, 0.8189f, 4.117f}, // Minimum Initial Position
                    new float[] {0.959f, 0.8189f, 5.3938f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.282f, 0.8189f, 3.709f}, // Minimum Initial Position
                    new float[] {0.594f, 0.8189f, 3.741f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.2961f, 0.8189f, 3.924f}, // Minimum Initial Position
                    new float[] {0.2961f, 0.8189f, 4.945f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.2339f, 0.9561f, 5.141f}, // Minimum Initial Position
                    new float[] {0.2339f, 0.9561f, 5.141f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 0f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.282f, 0.8189f, 6.439f}, // Minimum Initial Position
                    new float[] {0.959f, 0.8189f, 6.687f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.282f, 0.8189f, 7.036f}, // Minimum Initial Position
                    new float[] {0.728f, 0.8189f, 8.115f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.865f, 0.8189f, 7.646f}, // Minimum Initial Position
                    new float[] {0.959f, 0.8189f, 8.115f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.508f, 0.8189f, 8.0588f}, // Minimum Initial Position
                    new float[] {6.185f, 0.8189f, 8.7418f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.05f, 0.8189f, 5.701f}, // Minimum Initial Position
                    new float[] {8.73f, 0.8189f, 6.229f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.398f, 0.8189f, 5.047f}, // Minimum Initial Position
                    new float[] {8.398f, 0.8189f, 5.543f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.053f, 0.8189f, 4.55f}, // Minimum Initial Position
                    new float[] {8.402f, 0.8189f, 4.915f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.7399f, 0.8189f, 4.5459f}, // Minimum Initial Position
                    new float[] {8.7399f, 0.8189f, 5.0439f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.2803f, 0.692f, 4.037f}, // Minimum Initial Position
                    new float[] {8.5708f, 0.692f, 4.2944f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.2853f, 0.4893f, 6.3508f}, // Minimum Initial Position
                    new float[] {8.5632f, 0.4893f, 6.3508f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.5625f, 0.5404f, 8.5099f}, // Minimum Initial Position
                    new float[] {7.5625f, 0.5404f, 8.5099f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.6397f, 0.9142f, 8.5257f}, // Minimum Initial Position
                    new float[] {7.6397f, 0.9142f, 8.5257f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                   new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.4691f, 0.9142f, 8.5257f}, // Minimum Initial Position
                    new float[] {8.4691f, 0.9142f, 8.5257f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.2968f, 0.5404f, 8.5257f}, // Minimum Initial Position
                    new float[] {8.2968f, 0.5404f, 8.5257f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.4854f, 1.2893f, 8.5257f}, // Minimum Initial Position
                    new float[] {7.4854f, 1.2893f, 8.5257f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {6.6496f, 0.447f, 8.5712f}, // Minimum Initial Position
                    new float[] {6.6496f, 0.447f, 8.5712f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {6.8593f, 0.447f, 8.5712f}, // Minimum Initial Position
                    new float[] {6.8593f, 0.447f, 8.5712f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {6.7047f, 0.447f, 8.4387f}, // Minimum Initial Position
                    new float[] {6.7047f, 0.447f, 8.4387f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 0f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {6.7047f, 0.447f, 8.6619f}, // Minimum Initial Position
                    new float[] {6.7047f, 0.447f, 8.6619f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 0f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {6.438f, 0.0087f, 8.774f}, // Minimum Initial Position
                    new float[] {6.438f, 0.0087f, 8.774f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 0f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.09f, 0.0087f, 8.44f}, // Minimum Initial Position
                    new float[] {7.09f, 0.0087f, 8.783f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.811f, 0.0087f, 8.4417f}, // Minimum Initial Position
                    new float[] {8.811f, 0.0087f, 8.4417f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.764f, 0.0087f, 6.402f}, // Minimum Initial Position
                    new float[] {8.764f, 0.0087f, 6.402f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.764f, 0.0087f, 4.171f}, // Minimum Initial Position
                    new float[] {8.764f, 0.0087f, 4.171f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.2318f, 0.0087f, 0.7691f}, // Minimum Initial Position
                    new float[] {0.2318f, 0.0087f, 0.7691f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.641f, 0.0087f, 0.276f}, // Minimum Initial Position
                    new float[] {0.641f, 0.0087f, 0.276f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 0f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
        {
            "Laptop", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.325f, 0.818f, 4.631f}, // Minimum Initial Position
                    new float[] {0.902f, 0.818f, 4.631f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.613f, 0.818f, 4.165f}, // Minimum Initial Position
                    new float[] {0.79f, 0.818f, 4.165f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -135f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -135f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.677f, 0.818f, 5.189f}, // Minimum Initial Position
                    new float[] {0.8f, 0.818f, 5.189f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -45f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -45f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.778f, 0.818f, 6.613f}, // Minimum Initial Position
                    new float[] {0.778f, 0.818f, 6.613f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -135f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -135f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.315f, 0.818f, 7.264f}, // Minimum Initial Position
                    new float[] {0.675f, 0.818f, 7.264f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.468f, 0.818f, 7.919f}, // Minimum Initial Position
                    new float[] {0.8f, 0.818f, 7.919f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -45f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -45f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.25f, 0.818f, 5.993f}, // Minimum Initial Position
                    new float[] {8.569f, 0.818f, 5.993f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 45f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 45f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.254f, 0.818f, 4.71f}, // Minimum Initial Position
                    new float[] {8.254f, 0.818f, 4.71f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 135f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 135f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
            
        },
        {
            "LegalPad", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.321f, 0.8125f, 3.735f}, // Minimum Initial Position
                    new float[] {0.567f, 0.8125f, 3.735f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.283f, 0.8125f, 3.964f}, // Minimum Initial Position
                    new float[] {0.283f, 0.8125f, 4.899f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.712f, 0.8125f, 3.932f}, // Minimum Initial Position
                    new float[] {0.936f, 0.8125f, 5.393f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.325f, 0.8125f, 6.434f}, // Minimum Initial Position
                    new float[] {0.936f, 0.8125f, 6.699f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.896f, 0.8125f, 6.852f}, // Minimum Initial Position
                    new float[] {0.896f, 0.8125f, 6.852f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.325f, 0.8125f, 7.025f}, // Minimum Initial Position
                    new float[] {0.706f, 0.8125f, 8.117f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.935f, 0.8125f, 7.642f}, // Minimum Initial Position
                    new float[] {0.935f, 0.8125f, 8.121f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.501f, 0.8125f, 8.086f}, // Minimum Initial Position
                    new float[] {6.195f, 0.8125f, 8.71f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.069f, 0.8125f, 5.692f}, // Minimum Initial Position
                    new float[] {8.711f, 0.8125f, 6.242f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.394f, 0.8125f, 5.14f}, // Minimum Initial Position
                    new float[] {8.394f, 0.8125f, 5.474f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.593f, 0.8125f, 4.955f}, // Minimum Initial Position
                    new float[] {8.593f, 0.8125f, 4.955f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.079f, 0.8125f, 4.546f}, // Minimum Initial Position
                    new float[] {8.332f, 0.8125f, 4.919f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.424f, 0.6854f, 4.157f}, // Minimum Initial Position
                    new float[] {8.424f, 0.6854f, 4.157f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.421f, 0.4855f, 6.3621f}, // Minimum Initial Position
                    new float[] {8.421f, 0.4855f, 6.3621f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.5659f, 0.5323f, 8.4934f}, // Minimum Initial Position
                    new float[] {7.5659f, 0.5323f, 8.4934f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.6418f, 0.9069f, 8.4934f}, // Minimum Initial Position
                    new float[] {7.6418f, 0.9069f, 8.4934f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.315f, 1.281f, 8.4934f}, // Minimum Initial Position
                    new float[] {8.315f, 1.281f, 8.4934f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.636f, 1.3754f, 8.4934f}, // Minimum Initial Position
                    new float[] {7.8326f, 1.3754f, 8.4934f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
        {
            "Monitor", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.401f, 0.812f, 4.588f}, // Minimum Initial Position
                    new float[] {0.401f, 0.812f, 4.588f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.401f, 0.812f, 7.24f}, // Minimum Initial Position
                    new float[] {0.401f, 0.812f, 7.24f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
        {
            "NoteBlue", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.269f, 0.8222f, 3.728f}, // Minimum Initial Position
                    new float[] {0.618f, 0.8222f, 3.728f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.269f, 0.8222f, 3.99f}, // Minimum Initial Position
                    new float[] {0.269f, 0.8222f, 4.973f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.961f, 0.8222f, 3.719f}, // Minimum Initial Position
                    new float[] {0.961f, 0.8222f, 3.719f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.6704f, 0.8222f, 3.9263f}, // Minimum Initial Position
                    new float[] {0.9693f, 0.8222f, 5.404f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.279f, 0.8222f, 6.421f}, // Minimum Initial Position
                    new float[] {0.9693f, 0.8222f, 6.698f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.811f, 0.8222f, 6.864f}, // Minimum Initial Position
                    new float[] {0.9693f, 0.8222f, 6.864f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.279f, 0.8222f, 7.021f}, // Minimum Initial Position
                    new float[] {0.736f, 0.8222f, 8.129f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.944f, 0.8222f, 7.643f}, // Minimum Initial Position
                    new float[] {0.944f, 0.8222f, 8.129f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.495f, 0.8222f, 8.045f}, // Minimum Initial Position
                    new float[] {6.214f, 0.8222f, 8.759f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.0313f, 0.8222f, 5.685f}, // Minimum Initial Position
                    new float[] {8.757f, 0.8222f, 6.259f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.397f, 0.8222f, 5.063f}, // Minimum Initial Position
                    new float[] {8.397f, 0.8222f, 5.555f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.0313f, 0.8222f, 4.526f}, // Minimum Initial Position
                    new float[] {8.421f, 0.8222f, 4.932f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.749f, 0.8222f, 4.537f}, // Minimum Initial Position
                    new float[] {8.749f, 0.8222f, 5.061f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.562f, 0.8222f, 4.954f}, // Minimum Initial Position
                    new float[] {8.562f, 0.8222f, 4.954f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.6213f, 0.8377f, 5.236f}, // Minimum Initial Position
                    new float[] {8.6213f, 0.8377f, 5.236f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.6213f, 0.8377f, 5.454f}, // Minimum Initial Position
                    new float[] {8.6213f, 0.8377f, 5.454f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.249f, 0.6944f, 3.989f}, // Minimum Initial Position
                    new float[] {8.597f, 0.6944f, 4.325f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.257f, 0.4944f, 6.3629f}, // Minimum Initial Position
                    new float[] {8.589f, 0.4944f, 6.3629f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.5664f, 0.5394f, 8.4765f}, // Minimum Initial Position
                    new float[] {7.5664f, 0.5394f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.2991f, 0.5394f, 8.4765f}, // Minimum Initial Position
                    new float[] {8.2991f, 0.5394f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.471f, 0.9162f, 8.4765f}, // Minimum Initial Position
                    new float[] {8.471f, 0.9162f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.6416f, 0.9162f, 8.4765f}, // Minimum Initial Position
                    new float[] {7.6416f, 0.9162f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.311f, 1.293f, 8.4765f}, // Minimum Initial Position
                    new float[] {8.311f, 1.293f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.487f, 1.293f, 8.4765f}, // Minimum Initial Position
                    new float[] {7.487f, 1.293f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
        {
            "NoteGreen", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.269f, 0.8222f, 3.728f}, // Minimum Initial Position
                    new float[] {0.618f, 0.8222f, 3.728f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.269f, 0.8222f, 3.99f}, // Minimum Initial Position
                    new float[] {0.269f, 0.8222f, 4.973f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.961f, 0.8222f, 3.719f}, // Minimum Initial Position
                    new float[] {0.961f, 0.8222f, 3.719f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.6704f, 0.8222f, 3.9263f}, // Minimum Initial Position
                    new float[] {0.9693f, 0.8222f, 5.404f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.279f, 0.8222f, 6.421f}, // Minimum Initial Position
                    new float[] {0.9693f, 0.8222f, 6.698f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.811f, 0.8222f, 6.864f}, // Minimum Initial Position
                    new float[] {0.9693f, 0.8222f, 6.864f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.279f, 0.8222f, 7.021f}, // Minimum Initial Position
                    new float[] {0.736f, 0.8222f, 8.129f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.944f, 0.8222f, 7.643f}, // Minimum Initial Position
                    new float[] {0.944f, 0.8222f, 8.129f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.495f, 0.8222f, 8.045f}, // Minimum Initial Position
                    new float[] {6.214f, 0.8222f, 8.759f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.0313f, 0.8222f, 5.685f}, // Minimum Initial Position
                    new float[] {8.757f, 0.8222f, 6.259f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.397f, 0.8222f, 5.063f}, // Minimum Initial Position
                    new float[] {8.397f, 0.8222f, 5.555f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.0313f, 0.8222f, 4.526f}, // Minimum Initial Position
                    new float[] {8.421f, 0.8222f, 4.932f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.749f, 0.8222f, 4.537f}, // Minimum Initial Position
                    new float[] {8.749f, 0.8222f, 5.061f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.562f, 0.8222f, 4.954f}, // Minimum Initial Position
                    new float[] {8.562f, 0.8222f, 4.954f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.6213f, 0.8377f, 5.236f}, // Minimum Initial Position
                    new float[] {8.6213f, 0.8377f, 5.236f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.6213f, 0.8377f, 5.454f}, // Minimum Initial Position
                    new float[] {8.6213f, 0.8377f, 5.454f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.249f, 0.6944f, 3.989f}, // Minimum Initial Position
                    new float[] {8.597f, 0.6944f, 4.325f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.257f, 0.4944f, 6.3629f}, // Minimum Initial Position
                    new float[] {8.589f, 0.4944f, 6.3629f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.5664f, 0.5394f, 8.4765f}, // Minimum Initial Position
                    new float[] {7.5664f, 0.5394f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.2991f, 0.5394f, 8.4765f}, // Minimum Initial Position
                    new float[] {8.2991f, 0.5394f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.471f, 0.9162f, 8.4765f}, // Minimum Initial Position
                    new float[] {8.471f, 0.9162f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.6416f, 0.9162f, 8.4765f}, // Minimum Initial Position
                    new float[] {7.6416f, 0.9162f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.311f, 1.293f, 8.4765f}, // Minimum Initial Position
                    new float[] {8.311f, 1.293f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.487f, 1.293f, 8.4765f}, // Minimum Initial Position
                    new float[] {7.487f, 1.293f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
        {
            "Notepad", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.315f, 0.8196f, 3.644f}, // Minimum Initial Position
                    new float[] {0.559f, 0.8196f, 3.644f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.377f, 0.8196f, 4.025f}, // Minimum Initial Position
                    new float[] {0.377f, 0.8196f, 4.916f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.719f, 0.8196f, 3.886f}, // Minimum Initial Position
                    new float[] {0.926f, 0.8196f, 5.306f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.897f, 0.8196f, 6.768f}, // Minimum Initial Position
                    new float[] {0.897f, 0.8196f, 6.768f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.313f, 0.8196f, 6.355f}, // Minimum Initial Position
                    new float[] {0.931f, 0.8196f, 6.615f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.313f, 0.8196f, 6.957f}, // Minimum Initial Position
                    new float[] {0.694f, 0.8196f, 8.033f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.922f, 0.8196f, 7.575f}, // Minimum Initial Position
                    new float[] {0.922f, 0.8196f, 8.033f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.424f, 0.8196f, 8.094f}, // Minimum Initial Position
                    new float[] {6.111f, 0.8196f, 8.71f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 180f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 180f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.08f, 0.8196f, 5.785f}, // Minimum Initial Position
                    new float[] {8.711f, 0.8196f, 6.322f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.478f, 0.8196f, 5.148f}, // Minimum Initial Position
                    new float[] {8.478f, 0.8196f, 5.462f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.559f, 0.8196f, 5.026f}, // Minimum Initial Position
                    new float[] {8.559f, 0.8196f, 5.026f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.821f, 0.8196f, 4.586f}, // Minimum Initial Position
                    new float[] {8.821f, 0.8196f, 5.019f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.088f, 0.8196f, 4.635f}, // Minimum Initial Position
                    new float[] {8.377f, 0.8196f, 5.003f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.438f, 0.6931f, 4.25f}, // Minimum Initial Position
                    new float[] {8.438f, 0.6931f, 4.25f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.416f, 0.599f, 5.424f}, // Minimum Initial Position
                    new float[] {7.416f, 0.599f, 5.424f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.4279f, 0.4913f, 6.4396f}, // Minimum Initial Position
                    new float[] {8.4279f, 0.4913f, 6.4396f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.541f, 0.6336f, 8.57f}, // Minimum Initial Position
                    new float[] {8.541f, 0.6336f, 8.57f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.273f, 1.0082f, 8.57f}, // Minimum Initial Position
                    new float[] {8.273f, 1.0082f, 8.57f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.731f, 1.3848f, 8.57f}, // Minimum Initial Position
                    new float[] {7.731f, 1.3848f, 8.57f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.241f, 1.6648f, 8.57f}, // Minimum Initial Position
                    new float[] {8.241f, 1.6648f, 8.57f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {1.338f, 0.599f, 7.566f}, // Minimum Initial Position
                    new float[] {1.338f, 0.599f, 7.566f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {1.2257f, 0.599f, 4.703f}, // Minimum Initial Position
                    new float[] {1.2257f, 0.599f, 4.703f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
        {
            "NoteRed", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.269f, 0.8222f, 3.728f}, // Minimum Initial Position
                    new float[] {0.618f, 0.8222f, 3.728f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.269f, 0.8222f, 3.99f}, // Minimum Initial Position
                    new float[] {0.269f, 0.8222f, 4.973f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.961f, 0.8222f, 3.719f}, // Minimum Initial Position
                    new float[] {0.961f, 0.8222f, 3.719f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.6704f, 0.8222f, 3.9263f}, // Minimum Initial Position
                    new float[] {0.9693f, 0.8222f, 5.404f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.279f, 0.8222f, 6.421f}, // Minimum Initial Position
                    new float[] {0.9693f, 0.8222f, 6.698f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.811f, 0.8222f, 6.864f}, // Minimum Initial Position
                    new float[] {0.9693f, 0.8222f, 6.864f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.279f, 0.8222f, 7.021f}, // Minimum Initial Position
                    new float[] {0.736f, 0.8222f, 8.129f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.944f, 0.8222f, 7.643f}, // Minimum Initial Position
                    new float[] {0.944f, 0.8222f, 8.129f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.495f, 0.8222f, 8.045f}, // Minimum Initial Position
                    new float[] {6.214f, 0.8222f, 8.759f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.0313f, 0.8222f, 5.685f}, // Minimum Initial Position
                    new float[] {8.757f, 0.8222f, 6.259f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.397f, 0.8222f, 5.063f}, // Minimum Initial Position
                    new float[] {8.397f, 0.8222f, 5.555f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.0313f, 0.8222f, 4.526f}, // Minimum Initial Position
                    new float[] {8.421f, 0.8222f, 4.932f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.749f, 0.8222f, 4.537f}, // Minimum Initial Position
                    new float[] {8.749f, 0.8222f, 5.061f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.562f, 0.8222f, 4.954f}, // Minimum Initial Position
                    new float[] {8.562f, 0.8222f, 4.954f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.6213f, 0.8377f, 5.236f}, // Minimum Initial Position
                    new float[] {8.6213f, 0.8377f, 5.236f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.6213f, 0.8377f, 5.454f}, // Minimum Initial Position
                    new float[] {8.6213f, 0.8377f, 5.454f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.249f, 0.6944f, 3.989f}, // Minimum Initial Position
                    new float[] {8.597f, 0.6944f, 4.325f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.257f, 0.4944f, 6.3629f}, // Minimum Initial Position
                    new float[] {8.589f, 0.4944f, 6.3629f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.5664f, 0.5394f, 8.4765f}, // Minimum Initial Position
                    new float[] {7.5664f, 0.5394f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.2991f, 0.5394f, 8.4765f}, // Minimum Initial Position
                    new float[] {8.2991f, 0.5394f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.471f, 0.9162f, 8.4765f}, // Minimum Initial Position
                    new float[] {8.471f, 0.9162f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.6416f, 0.9162f, 8.4765f}, // Minimum Initial Position
                    new float[] {7.6416f, 0.9162f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.311f, 1.293f, 8.4765f}, // Minimum Initial Position
                    new float[] {8.311f, 1.293f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.487f, 1.293f, 8.4765f}, // Minimum Initial Position
                    new float[] {7.487f, 1.293f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
        {
            "NoteYellow", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.269f, 0.8222f, 3.728f}, // Minimum Initial Position
                    new float[] {0.618f, 0.8222f, 3.728f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.269f, 0.8222f, 3.99f}, // Minimum Initial Position
                    new float[] {0.269f, 0.8222f, 4.973f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.961f, 0.8222f, 3.719f}, // Minimum Initial Position
                    new float[] {0.961f, 0.8222f, 3.719f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.6704f, 0.8222f, 3.9263f}, // Minimum Initial Position
                    new float[] {0.9693f, 0.8222f, 5.404f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.279f, 0.8222f, 6.421f}, // Minimum Initial Position
                    new float[] {0.9693f, 0.8222f, 6.698f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.811f, 0.8222f, 6.864f}, // Minimum Initial Position
                    new float[] {0.9693f, 0.8222f, 6.864f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.279f, 0.8222f, 7.021f}, // Minimum Initial Position
                    new float[] {0.736f, 0.8222f, 8.129f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.944f, 0.8222f, 7.643f}, // Minimum Initial Position
                    new float[] {0.944f, 0.8222f, 8.129f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.495f, 0.8222f, 8.045f}, // Minimum Initial Position
                    new float[] {6.214f, 0.8222f, 8.759f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.0313f, 0.8222f, 5.685f}, // Minimum Initial Position
                    new float[] {8.757f, 0.8222f, 6.259f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.397f, 0.8222f, 5.063f}, // Minimum Initial Position
                    new float[] {8.397f, 0.8222f, 5.555f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.0313f, 0.8222f, 4.526f}, // Minimum Initial Position
                    new float[] {8.421f, 0.8222f, 4.932f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.749f, 0.8222f, 4.537f}, // Minimum Initial Position
                    new float[] {8.749f, 0.8222f, 5.061f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.562f, 0.8222f, 4.954f}, // Minimum Initial Position
                    new float[] {8.562f, 0.8222f, 4.954f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.6213f, 0.8377f, 5.236f}, // Minimum Initial Position
                    new float[] {8.6213f, 0.8377f, 5.236f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.6213f, 0.8377f, 5.454f}, // Minimum Initial Position
                    new float[] {8.6213f, 0.8377f, 5.454f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.249f, 0.6944f, 3.989f}, // Minimum Initial Position
                    new float[] {8.597f, 0.6944f, 4.325f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.257f, 0.4944f, 6.3629f}, // Minimum Initial Position
                    new float[] {8.589f, 0.4944f, 6.3629f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.5664f, 0.5394f, 8.4765f}, // Minimum Initial Position
                    new float[] {7.5664f, 0.5394f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.2991f, 0.5394f, 8.4765f}, // Minimum Initial Position
                    new float[] {8.2991f, 0.5394f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.471f, 0.9162f, 8.4765f}, // Minimum Initial Position
                    new float[] {8.471f, 0.9162f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.6416f, 0.9162f, 8.4765f}, // Minimum Initial Position
                    new float[] {7.6416f, 0.9162f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.311f, 1.293f, 8.4765f}, // Minimum Initial Position
                    new float[] {8.311f, 1.293f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.487f, 1.293f, 8.4765f}, // Minimum Initial Position
                    new float[] {7.487f, 1.293f, 8.4765f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
        {
            "Paper", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.2888f, 0.8196f, 3.728f}, // Minimum Initial Position
                    new float[] {0.559f, 0.8196f, 3.728f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.2888f, 0.8196f, 4.025f}, // Minimum Initial Position
                    new float[] {0.2888f, 0.8196f, 4.9372f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.719f, 0.8196f, 3.978f}, // Minimum Initial Position
                    new float[] {0.926f, 0.8196f, 5.32f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.897f, 0.8196f, 6.86f}, // Minimum Initial Position
                    new float[] {0.897f, 0.8196f, 6.86f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.333f, 0.8196f, 6.479f}, // Minimum Initial Position
                    new float[] {0.926f, 0.8196f, 6.65f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.333f, 0.8196f, 7.07f}, // Minimum Initial Position
                    new float[] {0.678f, 0.8196f, 8.077f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.926f, 0.8196f, 7.683f}, // Minimum Initial Position
                    new float[] {0.926f, 0.8196f, 8.077f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.55f, 0.8196f, 8.094f}, // Minimum Initial Position
                    new float[] {6.144f, 0.8196f, 8.725f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.08f, 0.8196f, 5.785f}, // Minimum Initial Position
                    new float[] {8.69f, 0.8196f, 6.197f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.395f, 0.8196f, 5.148f}, // Minimum Initial Position
                    new float[] {8.395f, 0.8196f, 5.462f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.559f, 0.8196f, 4.952f}, // Minimum Initial Position
                    new float[] {8.559f, 0.8196f, 4.952f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.744f, 0.8196f, 4.586f}, // Minimum Initial Position
                    new float[] {8.744f, 0.8196f, 5.019f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.088f, 0.8196f, 4.608f}, // Minimum Initial Position
                    new float[] {8.359f, 0.8196f, 4.86f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.438f, 0.6931f, 4.156f}, // Minimum Initial Position
                    new float[] {8.438f, 0.6931f, 4.156f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.416f, 0.599f, 5.337f}, // Minimum Initial Position
                    new float[] {7.416f, 0.599f, 5.337f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.4279f, 0.4913f, 6.3592f}, // Minimum Initial Position
                    new float[] {8.4279f, 0.4913f, 6.3592f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.5029f, 0.6336f, 8.497f}, // Minimum Initial Position
                    new float[] {8.5029f, 0.6336f, 8.497f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.273f, 1.0082f, 8.497f}, // Minimum Initial Position
                    new float[] {8.273f, 1.0082f, 8.497f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.731f, 1.3848f, 8.497f}, // Minimum Initial Position
                    new float[] {7.731f, 1.3848f, 8.497f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.241f, 1.6648f, 8.497f}, // Minimum Initial Position
                    new float[] {8.241f, 1.6648f, 8.497f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {1.338f, 0.599f, 7.482f}, // Minimum Initial Position
                    new float[] {1.338f, 0.599f, 7.482f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {1.2257f, 0.599f, 4.628f}, // Minimum Initial Position
                    new float[] {1.2257f, 0.599f, 4.628f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
        {
            "PenPencilEraser", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.2805f, 0.8373f, 3.7402f}, // Minimum Initial Position
                    new float[] {0.5849f, 0.8373f, 3.7402f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.958f, 0.8373f, 3.7402f}, // Minimum Initial Position
                    new float[] {0.958f, 0.8373f, 3.7402f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.705f, 0.8373f, 3.955f}, // Minimum Initial Position
                    new float[] {0.9492f, 0.8373f, 5.373f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.299f, 0.8373f, 6.455f}, // Minimum Initial Position
                    new float[] {0.9492f, 0.8373f, 6.681f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.88f, 0.8373f, 6.878f}, // Minimum Initial Position
                    new float[] {0.88f, 0.8373f, 6.878f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.299f, 0.8373f, 7.049f}, // Minimum Initial Position
                    new float[] {0.715f, 0.8373f, 8.099f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.945f, 0.8373f, 7.63f}, // Minimum Initial Position
                    new float[] {0.945f, 0.8373f, 8.167f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.51f, 0.8373f, 8.058f}, // Minimum Initial Position
                    new float[] {6.161f, 0.8373f, 8.725f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.055f, 0.8373f, 5.716f}, // Minimum Initial Position
                    new float[] {8.717f, 0.8373f, 6.221f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.386f, 0.8373f, 5.116f}, // Minimum Initial Position
                    new float[] {8.386f, 0.8373f, 5.483f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.563f, 0.8373f, 4.928f}, // Minimum Initial Position
                    new float[] {8.563f, 0.8373f, 4.928f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.068f, 0.8373f, 4.557f}, // Minimum Initial Position
                    new float[] {8.391f, 0.8373f, 4.901f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.996f, 0.8373f, 5.132f}, // Minimum Initial Position
                    new float[] {7.996f, 0.8373f, 5.473f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.422f, 0.7054f, 4.171f}, // Minimum Initial Position
                    new float[] {8.422f, 0.7054f, 4.171f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 360f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.438f, 0.5087f, 6.359f}, // Minimum Initial Position
                    new float[] {8.438f, 0.5087f, 6.359f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, -90f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, -90f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.556f, 0.553f, 8.456f}, // Minimum Initial Position
                    new float[] {7.556f, 0.553f, 8.456f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.63f, 0.93f, 8.456f}, // Minimum Initial Position
                    new float[] {7.63f, 0.93f, 8.456f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.2994f, 1.3003f, 8.456f}, // Minimum Initial Position
                    new float[] {8.2994f, 1.3003f, 8.456f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {0f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {0f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
        {
            "Phone", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.253f, 0.8212f, 3.64f}, // Minimum Initial Position
                    new float[] {0.6254f, 0.8212f, 3.8097f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 180f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 180f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.9633f, 0.8212f, 3.7591f}, // Minimum Initial Position
                    new float[] {0.9633f, 0.8212f, 3.7591f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 180f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 180f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.2747f, 0.8212f, 3.988f}, // Minimum Initial Position
                    new float[] {0.2747f, 0.8212f, 5.009f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 180f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 180f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.678f, 0.8212f, 3.926f}, // Minimum Initial Position
                    new float[] {0.962f, 0.8212f, 5.4f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.2858f, 0.9564f, 5.144f}, // Minimum Initial Position
                    new float[] {0.2858f, 0.9564f, 5.144f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 180f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 180f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.291f, 0.8212f, 6.42f}, // Minimum Initial Position
                    new float[] {0.9877f, 0.8212f, 6.702f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.923f, 0.8212f, 6.859f}, // Minimum Initial Position
                    new float[] {0.923f, 0.8212f, 6.859f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 180f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 180f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.253f, 0.8212f, 7.0075f}, // Minimum Initial Position
                    new float[] {0.7521f, 0.8212f, 8.13f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.937f, 0.8212f, 7.6235f}, // Minimum Initial Position
                    new float[] {0.937f, 0.8212f, 8.13f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 180f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 180f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.484f, 0.8212f, 8.043f}, // Minimum Initial Position
                    new float[] {6.209f, 0.8212f, 8.7487f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.029f, 0.8212f, 5.684f}, // Minimum Initial Position
                    new float[] {8.748f, 0.8212f, 6.259f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.392f, 0.8212f, 5.11f}, // Minimum Initial Position
                    new float[] {8.392f, 0.8212f, 5.527f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.031f, 0.8212f, 4.529f}, // Minimum Initial Position
                    new float[] {8.426f, 0.8212f, 4.945f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.75f, 0.8212f, 4.534f}, // Minimum Initial Position
                    new float[] {8.75f, 0.8212f, 5.055f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.585f, 0.8212f, 4.52f}, // Minimum Initial Position
                    new float[] {8.585f, 0.8212f, 4.52f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 0f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.559f, 0.8212f, 4.937f}, // Minimum Initial Position
                    new float[] {8.559f, 0.8212f, 4.937f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.6297f, 0.8308f, 5.4557f}, // Minimum Initial Position
                    new float[] {8.6297f, 0.8308f, 5.4557f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 0f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.6297f, 0.8294f, 5.236f}, // Minimum Initial Position
                    new float[] {8.6297f, 0.8294f, 5.236f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 0f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.68f, 0.8294f, 5.344f}, // Minimum Initial Position
                    new float[] {8.68f, 0.8294f, 5.344f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.246f, 0.6871f, 3.995f}, // Minimum Initial Position
                    new float[] {8.5948f, 0.6871f, 4.324f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.2529f, 0.4876f, 6.367f}, // Minimum Initial Position
                    new float[] {8.6015f, 0.4876f, 6.367f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 360f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.5638f, 0.5354f, 8.4966f}, // Minimum Initial Position
                    new float[] {7.5638f, 0.5354f, 8.4966f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.3f, 0.5354f, 8.4966f}, // Minimum Initial Position
                    new float[] {8.3f, 0.5354f, 8.4966f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.47f, 0.9099f, 8.4966f}, // Minimum Initial Position
                    new float[] {8.47f, 0.9099f, 8.4966f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.639f, 0.9099f, 8.4966f}, // Minimum Initial Position
                    new float[] {7.639f, 0.9099f, 8.4966f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {7.485f, 1.2835f, 8.4966f}, // Minimum Initial Position
                    new float[] {7.485f, 1.2835f, 8.4966f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.3084f, 1.2835f, 8.4966f}, // Minimum Initial Position
                    new float[] {8.3084f, 1.2835f, 8.4966f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {90f, 90f, 90f}, // Minimum Initial Rotation
                    new float[] {90f, 90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
        {
            "TrashBin", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.499f, 0f, 8.524f}, // Minimum Initial Position
                    new float[] {4.167f, 0f, 8.524f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {-90f, 0f, 180f}, // Minimum Initial Rotation
                    new float[] {-90f, 0f, 180f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.477f, 0f, 1.33f}, // Minimum Initial Position
                    new float[] {0.477f, 0f, 3.376f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {-90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {-90f, 0f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.477f, 0f, 5.699f}, // Minimum Initial Position
                    new float[] {0.477f, 0f, 6.123f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {-90f, 0f, 90f}, // Minimum Initial Rotation
                    new float[] {-90f, 0f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {1.203f, 0f, 0.498f}, // Minimum Initial Position
                    new float[] {8.583f, 0f, 0.498f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {-90f, -90f, 90f}, // Minimum Initial Rotation
                    new float[] {-90f, -90f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.583f, 0f, 2.441f}, // Minimum Initial Position
                    new float[] {8.583f, 0f, 3.051f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {-90f, 180f, 90f}, // Minimum Initial Rotation
                    new float[] {-90f, 180f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.583f, 0f, 7.285f}, // Minimum Initial Position
                    new float[] {8.583f, 0f, 7.285f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {-90f, 180f, 90f}, // Minimum Initial Rotation
                    new float[] {-90f, 180f, 90f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
        {
            "TrashCan", 
            new List<PlacementArea>(){
                new PlacementArea(
                    new float[] {0.499f, 0f, 8.524f}, // Minimum Initial Position
                    new float[] {4.167f, 0f, 8.524f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {-90f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {-90f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.477f, 0f, 1.33f}, // Minimum Initial Position
                    new float[] {0.477f, 0f, 3.376f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {-90f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {-90f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.477f, 0f, 5.699f}, // Minimum Initial Position
                    new float[] {0.477f, 0f, 6.123f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {-90f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {-90f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {1.203f, 0f, 0.498f}, // Minimum Initial Position
                    new float[] {8.583f, 0f, 0.498f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {-90f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {-90f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.583f, 0f, 2.441f}, // Minimum Initial Position
                    new float[] {8.583f, 0f, 3.051f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {-90f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {-90f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.583f, 0f, 7.285f}, // Minimum Initial Position
                    new float[] {8.583f, 0f, 7.285f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {-90f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {-90f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {8.583f, 0f, 4.893f}, // Minimum Initial Position
                    new float[] {8.583f, 0f, 5.861f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {-90f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {-90f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.477f, 0f, 4.0859f}, // Minimum Initial Position
                    new float[] {0.477f, 0f, 4.998f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {-90f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {-90f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {0.477f, 0f, 6.828f}, // Minimum Initial Position
                    new float[] {0.477f, 0f, 7.722f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {-90f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {-90f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
                new PlacementArea(
                    new float[] {4.759f, 0f, 8.524f}, // Minimum Initial Position
                    new float[] {5.903f, 0f, 8.524f}, // Maximum Initial Position
                    null, // Minimum Final Position
                    null, // Maximum Final Position
                    new float[] {-90f, 0f, 0f}, // Minimum Initial Rotation
                    new float[] {-90f, 0f, 0f}, // Maximum Initial Rotation
                    null, // Minimum Final Rotation
                    null), // Maximum Final Rotation
            }
        },
    };

    public static void SelectRandomPlacementArea(string objectName){
        // Check if the object is present in the dictionary
        if (placementAreasForEachObject.ContainsKey(objectName)){
            // Generate a random index for the placement area
            int randomPlacementAreaIndex = random.Next(placementAreasForEachObject[objectName].Count);
            randomPlacementArea = placementAreasForEachObject[objectName][randomPlacementAreaIndex];
        }
    }

    // Gets a random initial position from the available placement areas
    public static float [] GetInitialPosition(string objectName){
        // Check if the randomPlacementArea is not null
        if (randomPlacementArea != null){
            return randomPlacementArea.GetInitialPosition();
        }
        return null;
    }

    // Gets a random final position from the available placement areas
    public static float [] GetFinalPosition(string objectName){
        // Check if the randomPlacementArea is not null
        if (randomPlacementArea != null){
            return randomPlacementArea.GetFinalPosition();
        }
        return null;
    }

    // Gets a random initial rotation from the available placement areas
    public static float [] GetInitialRotation(string objectName){
        // Check if the randomPlacementArea is not null
        if (randomPlacementArea != null){
            return randomPlacementArea.GetInitialRotation();
        }
        return null;
    }

    // Gets a random final rotation from the available placement areas
    public static float [] GetFinalRotation(string objectName){
        // Check if the randomPlacementArea is not null
        if (randomPlacementArea != null){
            return randomPlacementArea.GetFinalRotation();
        }
        return null;
    }
}

class PlacementArea{
    private static System.Random random = new System.Random();

    // Minimum and maximum initial position coords
    private float [] minInitialPosition;
    private float [] maxInitialPosition;
    // Minimum and maximum final position coords
    private float [] minFinalPosition;
    private float [] maxFinalPosition;
    // Minimum and maximum initial rotation coords
    private float [] minInitialRotation;
    private float [] maxInitialRotation;
    // Minimum and maximum final rotation coords
    private float [] minFinalRotation;
    private float [] maxFinalRotation;

    // PlacementArea constructor
    public PlacementArea(float [] minInitialPosition, float [] maxInitialPosition, float [] minFinalPosition, float [] maxFinalPosition, float [] minInitialRotation, float [] maxInitialRotation, float [] minFinalRotation, float [] maxFinalRotation){
        this.minInitialPosition = minInitialPosition;
        this.maxInitialPosition = maxInitialPosition;

        this.minFinalPosition = minFinalPosition;
        this.maxFinalPosition = maxFinalPosition;
        
        this.minInitialRotation = minInitialRotation;
        this.maxInitialRotation = maxInitialRotation;

        this.minFinalRotation = minFinalRotation;
        this.maxFinalRotation = maxFinalRotation;
    }

    // Generates a random initial position from min and max initial positions
    public float [] GetInitialPosition(){
        if((minInitialPosition == null) || (maxInitialPosition == null))
            return null;
        return new float[] {RandomFloat(minInitialPosition[0],maxInitialPosition[0]), RandomFloat(minInitialPosition[1],maxInitialPosition[1]), RandomFloat(minInitialPosition[2],maxInitialPosition[2])};
    }

    // Generates a random final position from min and max final positions
    public float [] GetFinalPosition(){
        if((minFinalPosition == null) || (maxFinalPosition == null))
            return null;
        return new float[] {RandomFloat(minFinalPosition[0],maxFinalPosition[0]), RandomFloat(minFinalPosition[1],maxFinalPosition[1]), RandomFloat(minFinalPosition[2],maxFinalPosition[2])};
    }

    // Generates a random initial rotation from min and max initial rotations
    public float [] GetInitialRotation(){
        if((minInitialRotation == null) || (maxInitialRotation == null))
            return null;
        return new float[] {RandomFloat(minInitialRotation[0],maxInitialRotation[0]), RandomFloat(minInitialRotation[1],maxInitialRotation[1]), RandomFloat(minInitialRotation[2],maxInitialRotation[2])};
    }

    // Generates a random final rotation from min and max final rotations
    public float [] GetFinalRotation(){
        if((minFinalRotation== null) || (maxFinalRotation == null))
            return null;
        return new float[] {RandomFloat(minFinalRotation[0],maxFinalRotation[0]), RandomFloat(minFinalRotation[1],maxFinalRotation[1]), RandomFloat(minFinalRotation[2],maxFinalRotation[2])};
    }
    
    // Generates a random float between min and max
    float RandomFloat(float min, float max){
        double val = (random.NextDouble() * (max - min) + min);
        return (float)val;
    }
}