using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;


public class PickItem : Item
{
    public GameObject itemUI;

    protected override void AddItemToInventory()
    {
        
        // Add item only if player didnt exit or time ended
        if (!PlayerPrefs.GetInt("exitButton").Equals(0))
             return;

        if (itemUI != null)
        {
            _audioManager.Play("PickUp");

            // Add item to the inventory
            int index = player.NumberItems();
            if (index > player.itemsSlots.Length - 1)
            {
                // Inventory full (now maximum capacity is 10)
                //Debug.Log("Inventory full");
                textLogControl.LogText("[Inventory] Inventory full.", TextLogItem.Type.OnlyToBd);
                return;
            }

            Instantiate(itemUI, player.itemsSlots[index].transform, false);
            textLogControl.LogText("[Inventory] " + obj.id + " added to inventory.", TextLogItem.Type.OnlyToBd);

            Destroy(gameObject);
            
            player.AddItem(obj.id);
            
            
        }else
            player.AddItem(obj.id, false);
        
    }
    
}
