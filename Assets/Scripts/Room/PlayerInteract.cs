using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

public class PlayerInteract : MonoBehaviour
{
    private InputActions _controls;
    private Vector2 _mouseDelta;
    private Vector2 _mousePosition;
    private bool _rotateCamera = false;
    private Item _item;
    private PlayerStats _playerStats;
    private LayerMask _layerMask;
    private LayerMask _layerNoPlayer;
    private readonly List<Outline> _nearItems = new List<Outline>();

    public new CinemachineFreeLook freeLookCamera;
    public CinemachineVirtualCamera firstPersonCamera;
    private CinemachinePOV _pov;
    public float interactRadius = 1.2f;

    private PlayerController _playerController;
    private GameRoomController _gameRoomController;
    
    private int fingerID = -1;

    private void Awake()
    {
        #if !UNITY_EDITOR
             fingerID = 0; 
        #endif
        
        _playerStats = GameObject.Find("Player").GetComponent<PlayerStats>();
        
        _controls = new InputActions();
        _controls.Player.MouseDelta.performed += context => _mouseDelta = context.ReadValue<Vector2>();
        _controls.Player.MousePosition.performed += context => _mousePosition = context.ReadValue<Vector2>();
        _controls.Player.EnableCameraRotation.started += context => _rotateCamera = true;
        _controls.Player.EnableCameraRotation.canceled += context => _rotateCamera = false;
        _controls.Player.ChangeCamera.performed += context => ChangeCamera();
        
        _layerMask =  LayerMask.NameToLayer("PickItem");
        _layerNoPlayer =  1 << LayerMask.NameToLayer("Player");
        _layerNoPlayer = ~_layerNoPlayer;

        _playerController = GetComponent<PlayerController>();
        _pov = firstPersonCamera.GetCinemachineComponent<CinemachinePOV>();
        _gameRoomController = FindObjectOfType<GameRoomController>();

    }

    private void ChangeCamera()
    {
        // Increase priority of the other
        freeLookCamera.Priority =  freeLookCamera.Priority == 10 ? 11 : 10;
        firstPersonCamera.Priority =  firstPersonCamera.Priority == 10 ? 11 : 10;
        
        // Default is 3rd person
        _playerController.ChangeController();

        // Initialize camera to be in front of player
        if (firstPersonCamera.Priority == 10)
            transform.rotation = Quaternion.Euler(0f, _pov.m_HorizontalAxis.Value, 0f);
        
        _gameRoomController.SendInformationJustToBd("[Camera] Change camera to " + (freeLookCamera.Priority == 11 ? "3rd" : "1st") + " person.");
    }

    // Update is called once per frame
    void Update()
    {
        // Rotate camera
        if (_rotateCamera)
        {
            if (freeLookCamera.Priority > firstPersonCamera.Priority)
            {
                freeLookCamera.m_XAxis.Value += _mouseDelta.x * 0.4f;
                freeLookCamera.m_YAxis.Value += _mouseDelta.y * 0.01f;
            }
            else
            {
                // x -> player itself
                _playerController.AddRotation(_mouseDelta.x * 0.4f);
                // y -> camera
                _pov.m_VerticalAxis.Value =  Mathf.Clamp(_pov.m_VerticalAxis.Value + _mouseDelta.y * 0.5f, -70f, 70f);

            }
        }else
            _playerController.AddRotation(0);
        
        // Illuminate items near the player + items with cursor on top
        foreach (var item in _nearItems)
        {
            if(item != null)
                item.Disable();
        }
        _nearItems.Clear();
        Collider[] collisions = Physics.OverlapSphere(transform.position, interactRadius, _layerMask);
        foreach (Collider collision in collisions)
        {
            if (collision.CompareTag("PickItem")) 
            {
                collision.gameObject.GetComponent<Outline>().Enable();
                _nearItems.Add(collision.gameObject.GetComponent<Outline>());
            }
        }
        RaycastHit hitInfo;
        if (!Physics.Raycast(Camera.main.ScreenPointToRay(_mousePosition), out hitInfo, 1000f, _layerMask)) 
            return;
        var outline = hitInfo.collider.gameObject.GetComponent<Outline>();
        if (outline)
        {
            outline.Enable();
            _nearItems.Add(outline);
        }
    }

    public void Interact()
    {
        // ClickED on UI
        if (EventSystem.current.IsPointerOverGameObject(fingerID))
            return;

        RaycastHit[] hitsInfo = Physics.RaycastAll(Camera.main.ScreenPointToRay(_mousePosition), 1000f, _layerNoPlayer);
        if (hitsInfo.Length == 0) 
            return;

        RaycastHit hitInfo = new RaycastHit();
        foreach(RaycastHit hit in hitsInfo){
            hitInfo = hit;
            if(hit.collider.gameObject.GetComponent<Item>() != null){
                break;
            }
        }

        _item = hitInfo.collider.gameObject.GetComponent<Item>();

        if (_item != null)
        {
            _gameRoomController.PlayButtonClick();
            _gameRoomController.SendInformationJustToBd("[Movement] Automatic movement started to reach " + _item.GetId() + ".");
        }else
            _gameRoomController.SendInformationJustToBd("[Movement] Automatic movement started with " + hitInfo.point + " as final coordinates.");

        // Walk to collider
        _playerController.StartMovement(hitInfo.point);
        
    }

    public void InteractAfterWalking()
    {
        if (_item != null)
        {
            // Only if player is near is able to click on the item
            // && Only if player is not in other item already
            if ( /*_nearItems.Contains(_item.GetComponent<Outline>()) && */_playerStats.IsFree())
            {
                _playerController.SetMovementSpeed(0.2f);
                _item.InteractedByPlayer(_playerStats);
            }

        }
    }

//    void OnDrawGizmosSelected()
//    {
//        Gizmos.color = Color.red;
//        Gizmos.DrawWireSphere(transform.position, interactRadius);
//   }

    public float GetInteractRadius()
    {
        return interactRadius;
    }
    
    public bool HaveItem()
    {
        return _item != null;
    }
    
    private void OnEnable()
    {
        _controls.Player.Enable();
    }

    private void OnDisable()
    {
        _controls.Player.Disable();
    }


}
