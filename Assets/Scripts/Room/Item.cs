using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using UnityEngine.InputSystem.UI;

public class Item : MonoBehaviour
{
    protected IterativeObj obj;
    
    protected TextLogControl textLogControl;
    private bool _gameEnded = false;
    
    // Move item
    private bool _movePosition = false;
    private bool _moveRotation = false;
    private Vector3 _velocityPosition;
    private Vector3 _velocityRotation;
    private Vector3 _finalPosition;
    private Vector3 _finalRotation;
    private float _transformationTime = 0.5f;
    private bool _alreadyMoved = false;
    private bool _alreadyUpdatedLevel = false;
    
    // Video
    //private VideoClip _videoClip;
    private VideoManager _videoManager;
    private TextManager _textManager;
    protected AudioManager _audioManager;
    
    protected PlayerStats player;

    private GameRoomController _gameRoomController;

    private bool _afterInitialIntros = false;

    private void Start()
    {
        textLogControl = FindObjectOfType<TextLogControl>();
        _videoManager = FindObjectOfType<VideoManager>();
        _gameRoomController = FindObjectOfType<GameRoomController>();
        _textManager = FindObjectOfType<TextManager>();
        _audioManager = FindObjectOfType<AudioManager>();
    }

    public void SetItem(IterativeObj ob)
    {
        obj = ob;
        obj.unlockItemsIds ??= new string []{};
    }

    /*public void SetVideo(VideoClip videoClip)
    {
        _videoClip = videoClip;
    }*/
    
    public void InteractedByPlayer(PlayerStats playerStats)
    {
        if (obj == null)
            return;
        player = playerStats;
        playerStats.SetFree(false);
        _gameRoomController.PausePlayer();
        _gameRoomController.SuspendClues(true);
        _audioManager.Stop("Ambient");

        _gameRoomController.SendInformationJustToBd("[Item " + obj.id + "] Starts interacting with item.");
        
        bool asVideo = !string.IsNullOrEmpty(_gameRoomController.FindPath(obj.videoPath));
        bool asText = !string.IsNullOrEmpty(_gameRoomController.FindText(obj.introText)) || !string.IsNullOrEmpty(_gameRoomController.FindText(obj.introTitle));
        bool miniGame = !string.IsNullOrEmpty(obj.miniGame.id);

        var playerController = playerStats.gameObject.GetComponent<PlayerController>();
        if (asVideo || asText || miniGame)
            playerController.SetMovementSpeed(0.1f);
        playerController.enabled = false;
        
        // If player does not have what it needs to interact with this item
        if (obj.unlockItemsIds.Length != 0)
        {
            foreach (var itemsId in obj.unlockItemsIds)
            { 
                if(!playerStats.Contains(itemsId))
                {
                    _gameRoomController.SendInformationJustToBd("[Item " + obj.id + "] Locked, player does not have "+ itemsId + ".");
                    playerStats.SetMessage(_gameRoomController.FindText(obj.messageLocked), null);
                    playerStats.gameObject.GetComponent<PlayerController>().enabled = true;
                    playerStats.SetFree(true);
                    _gameRoomController.ResumePlayer();
                    _gameRoomController.SuspendClues(false);
                    _audioManager.Play("Ambient");
                    if(obj.id.Equals("Door") || obj.id.Equals("DoorTiago"))
                        _audioManager.Play("DoorLocked");
                    return;
                }
            }
        }
        
        // Start Interacting if no intros
        if (!StartIntros(asVideo, asText, _gameRoomController.FindPath(obj.videoPath), _gameRoomController.FindText(obj.introTitle), _gameRoomController.FindText(obj.introText)))
            InteractAfterIntros();
    }

    private bool StartIntros(bool asVideo, bool asText, string videoPath, string title, string text)
    {
        // Prepare video if associated
        if (asVideo)
            _videoManager.PrepareIntroduction(videoPath, this, null);
        
        // Show text if associated
        if (asText)
        {
            _textManager.ShowText(title, text, asVideo, false, this, null);
            return true;
        }
        // Show just video associated
        if (asVideo)
        {
            _videoManager.StartVideo(false);
            return true;
        }
        
        return false;
    }

    public void Interact()
    {
        _gameRoomController.ResetEventSystemInRoom();
        
        bool asVideo = !string.IsNullOrEmpty(_gameRoomController.FindPath(obj.finalVideoPath));
        bool asText = !string.IsNullOrEmpty(_gameRoomController.FindText(obj.finalIntroText)) || !string.IsNullOrEmpty(_gameRoomController.FindText(obj.finalIntroTitle));

        // Continue Interacting if no final intros
        if(!StartIntros(asVideo, asText, _gameRoomController.FindPath(obj.finalVideoPath), _gameRoomController.FindText(obj.finalIntroTitle), _gameRoomController.FindText(obj.finalIntroText)))
            InteractAfterFinalIntros();
    }

    private void InteractAfterFinalIntros()
    {
        // Add to inventory
        AddItemToInventory();
        
        // if player didnt exit or time ended
        if (PlayerPrefs.GetInt("exitButton").Equals(0))
        {
            // Start moving to final position, rotation -> after moving player can win game if winsGame is set to true
            if (!_alreadyMoved)
                Move();

            if (!_alreadyUpdatedLevel) // Don't enter again in old levels
            {
                if(!string.IsNullOrEmpty(obj.entersLevel))
                    _gameRoomController.SetLevelInPlayer(obj.entersLevel);
                _alreadyUpdatedLevel = true;
            }

        }
        
        // Reset
        PlayerPrefs.SetInt("exitButton", 0);
        
        _afterInitialIntros = false;
        player.SetFree(true);
        _gameRoomController.ResumePlayer();
        _gameRoomController.SuspendClues(false);
        _audioManager.Play("Ambient");
    }


    protected virtual void AddItemToInventory()
    {
        // Add item only if player didnt exit or time ended
        if (!PlayerPrefs.GetInt("exitButton").Equals(0))
            return;
        
        player.AddItem(obj.id, false);
    }

    private void Move()
    {
        if (obj.finalPosition is {Length: 3})
        {
            _finalPosition = new Vector3(obj.finalPosition[0], obj.finalPosition[1], obj.finalPosition[2]);
            _movePosition = true;
        }
        if (obj.finalRotation is {Length: 3})
        {
            _finalRotation = new Vector3(obj.finalRotation[0], obj.finalRotation[1], obj.finalRotation[2]);
            _moveRotation = true;
        }

        if (_movePosition || _moveRotation)
        {
            _gameRoomController.SendInformationJustToBd("[Item " + obj.id + "] Moved.");
            _audioManager.Play(obj.id.Equals("Door") || obj.id.Equals("DoorTiago") ? "OpenDoor" : "Move");
            return;
        }

        
        // WINS GAME
        if (obj.winsGame)
            WinsGame();
        
    }

    private void WinsGame()
    {
        _gameRoomController.SendInformationJustToBd("[Item " + obj.id + "] Made player escape.");
        _gameEnded = true;
        FindObjectOfType<GameRoomController>().EndsGame();
    }
    
    public string GetId()
    {
        return obj.id;
    }
    
    public string GetSucceededMsg()
    {
        return _gameRoomController.FindText(obj.messageSucceeded);
    }

    public string GetSucceededWithHelpMsg()
    {
        return _gameRoomController.FindText(obj.messageSucceededWithHelp);
    }
    
    public string GetExitedMsg()
    {
        return _gameRoomController.FindText(obj.messageExited);
    }
    
    public string GetFailedMsg()
    {
        return _gameRoomController.FindText(obj.messageFailed);
    }
    
     private void Update(){ 
         if(_gameEnded)
             return;
         
         if (_movePosition || _moveRotation)
         {
            // WINS GAME after finishing moving
            if ((!_movePosition || Math.Abs((transform.position - _finalPosition).magnitude) < 0.5f) 
                             && (!_moveRotation || Math.Abs(Quaternion.Angle(transform.rotation, Quaternion.Euler(_finalRotation))) < 0.5f))
            {
                _alreadyMoved = true;

                if (obj.winsGame)
                {
                    WinsGame();
                    return;
                }
            }
            
            if(_movePosition)
                transform.position = Vector3.SmoothDamp(transform.position, _finalPosition, ref _velocityPosition, _transformationTime);
            if (_moveRotation)
            {
                transform.rotation = Quaternion.Euler(Mathf.SmoothDampAngle(transform.eulerAngles.x, _finalRotation.x, ref _velocityRotation.x, _transformationTime), 
                    Mathf.SmoothDampAngle(transform.eulerAngles.y, _finalRotation.y, ref _velocityRotation.y, _transformationTime), 
                    Mathf.SmoothDampAngle(transform.eulerAngles.z, _finalRotation.z, ref _velocityRotation.z, _transformationTime));
            }
         }
    }

     public void InteractAfterIntros()
     {
         if (_afterInitialIntros)
         {
             InteractAfterFinalIntros();
             return;
         }
         _afterInitialIntros = true;

         // 2D Game associated
         if (!string.IsNullOrEmpty(_gameRoomController.FindPath(obj.miniGame.pathOptions)) && !string.IsNullOrEmpty(obj.miniGame.id))
         {
             _gameRoomController.SendInformationJustToBd("[Item " + obj.id + "] Enter in mini game " + obj.miniGame.id + ".");
             player.WaitForResultToShowMessage(this);
             PlayerPrefs.SetInt("inMainGame", 0);
             player.InMainGame(false);
            
             //TODO: temporary fix
             FindObjectOfType<InputSystemUIInputModule>().enabled = false;
             FindObjectOfType<AudioListener>().enabled = false;
             FindObjectOfType<Canvas>().enabled = false;
             FindObjectOfType<EventSystem>().enabled = false;

             _gameRoomController.EnterMiniGame(obj.miniGame);
             SceneManager.LoadScene(obj.miniGame.id, LoadSceneMode.Additive);
             return;
         }
        
         // No game associated
         player.SetMessage(_gameRoomController.FindText(obj.messageSucceeded), null);
         Interact();
     }
    
}
