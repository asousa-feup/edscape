using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.Video;

public class TextManager : MonoBehaviour
{
    public GameObject textPanelUI;
    private bool _asVideoAfter = false;
    private Item _currItem = null;
    private MiniGameController _miniGame = null;

    private GameRoomController _gameRoomController;
    private VideoManager _videoManager;
    private TTS tts_player;

    private bool _start = false;
    private string _title = "";
    private string _text = "";
    private bool _asText = false;

    private Item waitItem = null;

    private bool _isGameInfo = false;

    private void Awake()
    {
        InputSystem.EnableDevice(Mouse.current);
        _gameRoomController = FindObjectOfType<GameRoomController>();
        _videoManager = gameObject.GetComponent<VideoManager>();
        tts_player = FindObjectOfType<TTS>();
    }

    public void StopShowingText()
    {
        _gameRoomController.PlayButtonClick();
        _gameRoomController.SuspendClues(true);
        StartCoroutine(Wait());
    }

    IEnumerator Wait()
    {
        InputSystem.DisableDevice(Mouse.current);
        yield return new WaitForSeconds(0.5F);
        InputSystem.EnableDevice(Mouse.current);
        _gameRoomController.SuspendClues(false);


        if (_start)
        {
            _start = false;
            if(_asText)
                ShowText(_title, _text, _asVideoAfter, _isGameInfo, _currItem, _miniGame);
            else
            {
                textPanelUI.SetActive(false);

                if (_asVideoAfter)
                    _videoManager.StartVideo(true);
                else if (_currItem != null)
                    _currItem.InteractAfterIntros();
                else if (_isGameInfo)
                    _gameRoomController.ResumePlayer();
                else if ((_currItem == null) && (_miniGame == null))
                    _gameRoomController.PlayStartPanelTTS();
                
                if (_miniGame != null)
                    _miniGame.InteractAfterIntros();

                _asVideoAfter = false;
                _currItem = null;
                _miniGame = null;
            }
        }
        else
        {
            textPanelUI.SetActive(false);

            waitItem = _currItem;
            _currItem = null;

            if (_asVideoAfter)
                _videoManager.StartVideo(false);
            else if (waitItem != null)
                waitItem.InteractAfterIntros();
            else if (_isGameInfo)
                _gameRoomController.ResumePlayer();
            else if ((_currItem == null) && (_miniGame == null))
                _gameRoomController.PlayStartPanelTTS();
            
            if (_miniGame != null)
                _miniGame.InteractAfterIntros();
            
            _asVideoAfter = false;
            _miniGame = null;
        }
    }

    public void ShowText(string title, string text, bool asVideoAfter, bool isGameInfo, Item item, MiniGameController miniGame)
    {
        if(item != null)
            _gameRoomController.SendInformationJustToBd("[Item " + item.GetId() + "] Show text.");
        else if(miniGame != null)
            _gameRoomController.SendInformationJustToBd("[Mini game  + " + _gameRoomController.GetMiniGamePath() + "] Show text.");
        else
            _gameRoomController.SendInformationJustToBd("[Intro] Show text.");


        _currItem = item;
        _asVideoAfter = asVideoAfter;
        _isGameInfo = isGameInfo;
        var texts = textPanelUI.gameObject.GetComponentsInChildren<TextMeshProUGUI>();
        texts[0].text = title;
        texts[1].rectTransform.offsetMax = new Vector2(texts[1].rectTransform.offsetMax.x, -0);
        texts[1].text = text;
        _miniGame = miniGame;
        if(_miniGame != null)
            _miniGame.DestroyLoadCanvas();
        
        // Build text to be sent to the TTS
        string textToSpeechText = title + "\n" + text;
        tts_player.Play(textToSpeechText, _gameRoomController._langOp);

        textPanelUI.SetActive(true);
        if(_miniGame == null)
            _gameRoomController.PausePlayer();
        
    }
    
    public void ShowText(string title, string text, bool asVideoAfter, Item item, MiniGameController miniGame, string infoTitle, string infoText, bool asText, bool asInfo)
    {
        _asText = asText;
        _title = title;
        _text = text;
        _currItem = item;
        _asVideoAfter = asVideoAfter;
        _isGameInfo = false;
        if (asInfo)
        {
            _gameRoomController.SendInformationJustToBd("[Info/Instructions] Show text.");
            _start = true;

            var texts = textPanelUI.gameObject.GetComponentsInChildren<TextMeshProUGUI>();
            texts[0].text = infoTitle;
            texts[1].rectTransform.offsetMax = new Vector2(texts[1].rectTransform.offsetMax.x, -0);
            texts[1].text = infoText;
            _miniGame = miniGame;
            if(_miniGame != null)
                _miniGame.DestroyLoadCanvas();
            
            // Build text to be sent to the TTS
            string textToSpeechText = infoTitle + "\n" + infoText;
            tts_player.Play(textToSpeechText, _gameRoomController._langOp);
            
            textPanelUI.SetActive(true);
            if(_miniGame == null)
                _gameRoomController.PausePlayer();
        }
        else if(asText)
        {
            _start = false;
            ShowText(_title, _text, _asVideoAfter, _isGameInfo, _currItem, _miniGame);
        }
    }
}
