using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.WindowsRuntime;
using Cinemachine;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class GameRoomController : MonoBehaviour
{
    public GameObject gameEnds;
    public GameObject prefabPanelLoad;
    public GameObject panelEndAfterSurvey;
    private PlayerStats _playerStats;
    private PlayerController _playerController;
    private MiniGame _currMiniGame = null;
    private TextLogControl _textLogControl;
    private readonly Dictionary<Canvas, GameObject> _currLoadPanels = new Dictionary<Canvas, GameObject>();
    private ConfigGameObj _gameObj;
    private int _numHintsUsed = 0;
    private Canvas _canvas;
    public string _langOp = "";
    
    public GameObject startPanel;

    public Scrollbar scrollBarClues;
    
    public GameObject badgesButton;
    private string urlBadges = "";

    private AudioListener _audio;
    private EventSystem _event;

    private AudioManager _audioManager;
    
    private bool _gameStarted = false;
    private bool _gameEnded = false;
    
    private bool _cluesSuspended = false;
    public GameObject cluesButton;
    
    private TTS tts_player;

    private void Awake()
    {
        _canvas = FindObjectOfType<Canvas>();
        _textLogControl =  FindObjectOfType<TextLogControl>();
        _playerStats = FindObjectOfType<PlayerStats>();
        _playerController = FindObjectOfType<PlayerController>();
        _audio = FindObjectOfType<AudioListener>();
        _event = FindObjectOfType<EventSystem>();
        _audioManager = gameObject.GetComponent<AudioManager>();
        tts_player = FindObjectOfType<TTS>();
    }

    public void EndsGame()
    {
        _gameEnded = true;
        _playerStats.GameEnded();// clues and thoughts system
        SendInformationJustToBd("Escapes successfully.");
        CreateLoadPanel(_canvas);
        badgesButton.SetActive(false);
        
        //Send log to db
        _textLogControl.SendLog(true);
    }

    public void RestartsGame()
    {
        SendInformationJustToBd("Restarts game.");
        PlayButtonClick();
        SceneManager.LoadScene("Login");
    }

    public void QuitsGame()
    {
        PlayButtonClick();
        _audioManager.Stop("Ambient");
        if (string.IsNullOrEmpty(FindPath(_gameObj.finalSurveyPath)) || PlayerPrefs.GetInt("test").Equals(1))
        {
            SendInformationJustToBd("Enters final end game panel.");
            SetEndApplicationPanel();
            return;
        }
        
        //TODO: temporary fix
        _audio.enabled = false;
        _event.enabled = false;
        
        SendInformationJustToBd("Starts final survey.");
        SceneManager.LoadScene("Survey", LoadSceneMode.Additive);
    }

    public void QuitsApplication()
    {
        SendInformationJustToBd("EdScape application closed.");
        PlayButtonClick();

        #if !UNITY_WEBGL
            Application.Quit();
        #endif
    }
    
    public void OpensBadgesWindow()
    {
        SendInformationJustToBd("Opens link of badges in browser.");
        PlayButtonClick();
        Application.OpenURL(urlBadges);
    }

    public void GetClue()
    {
        if(_cluesSuspended)
            return;
        
        PlayButtonClick();
        _playerStats.GetClue();
    }

    public Scrollbar GetScrollBarClue()
    {
        return scrollBarClues;
    }

    public void SendInformationJustToBd(string text)
    {
        _playerStats.SetMessage(text, TextLogItem.Type.OnlyToBd);
    }

    public void SetRoomTime(double time)
    {
        _playerStats.SetTime(time);
    }

    public double GetRoomTime()
    {
        return _playerStats.GetTime();
    }

    public void PausePlayer()
    {
        _playerController.enabled = false;
    }

    public void ResumePlayer()
    {
        ResetEventSystemInRoom();
        if(_gameStarted)
            _playerController.enabled = true;
    }

    public void CreateLoadPanel(Canvas canvas)
    {
        var panel = Instantiate(prefabPanelLoad, new Vector3(0, 0, 0), Quaternion.identity, canvas.transform);
        panel.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        _currLoadPanels.Add(canvas, panel);
    }

    public void DestroyLoadPanel(Canvas canvas)
    {
        if (!_currLoadPanels.ContainsKey(canvas))
            return;

        Destroy(_currLoadPanels[canvas]);
        _currLoadPanels.Remove(canvas);
    }

    public void EnterMiniGame(MiniGame objMiniGame)
    {
        _currMiniGame = objMiniGame;
    }

    public string GetMiniGamePath()
    {
        return FindPath(_currMiniGame.pathOptions);
    }

    public void SetGameInfo(ConfigGameObj obj, string op)
    {
        _gameObj = obj;
        _langOp = op;
        
        // Set start button play info
        UpdatePanelText(startPanel, FindText(_gameObj.startText));

        // Set end game panel title
        UpdatePanelText(gameEnds, FindText(_gameObj.congratulationsText));

        // Set end game after survey panel title
        UpdatePanelText(panelEndAfterSurvey, FindText(_gameObj.endAfterSurveyText));
    }

    public string FindText(List<TextOp> textOps)
    {
        if (textOps == null)
            return "";
        
        if (string.IsNullOrEmpty(_langOp))
        {
            foreach (var row in textOps.Where(row => string.IsNullOrEmpty(row.id)))
            {
                return row.text;
            }
            return "";
        }
        
        foreach (var row in textOps.Where(row => row is {id: { }} && row.id.Equals(_langOp)))
        {
            return row.text;
        }
        return "";
    }
    
    public string FindPath(List<PathOp> textOps)
    {
        if (textOps == null)
            return "";

        if (string.IsNullOrEmpty(_langOp))
        {
            foreach (var row in textOps.Where(row => string.IsNullOrEmpty(row.id)))
            {
                // Check if the path is empty or null
                // if(string.IsNullOrEmpty(row.path))
                //     // return url if path is empty
                //     return row.url;
                return row.path;
            }
            return "";
        }
        
        foreach (var row in textOps.Where(row =>  row is {id: { }} && row.id.Equals(_langOp)))
        {
           // Check if the path is empty or null
            // if(string.IsNullOrEmpty(row.path))
            //     // return url if path is empty
            //     return row.url;
            return row.path;
        }
        return "";
    }

    public string FindTextTimeEndText()
    {
        return FindText(_gameObj.timeEndedText);
    }

    private void UpdatePanelText(GameObject panel, string text)
    {
        if(!string.IsNullOrEmpty(text))
            panel.GetComponentInChildren<TextMeshProUGUI>().text = text;
        else
        {
            // remove scroll bar
            panel.GetComponentInChildren<TextMeshProUGUI>().gameObject.SetActive(false);
            panel.GetComponentInChildren<ScrollRect>().gameObject.SetActive(false);
            panel.GetComponentInChildren<Scrollbar>().gameObject.SetActive(false);
        }
    }
    
    public ConfigGameObj GetGameInfo()
    {
        return _gameObj;
    }

    public int GetHintsCount()
    {
        return _numHintsUsed;
    }
    
    public void AddHintsCount(int hints)
    {
        _numHintsUsed += hints;
    }

    public void SetActiveEndPanel(string badgesResult)
    {
        // Add badges info
        if (!string.IsNullOrEmpty(badgesResult))
        {
            urlBadges = badgesResult;
            badgesButton.SetActive(true);
        }
        
        DestroyLoadPanel(_canvas);

        // Check if the text component was set
        if(gameEnds.GetComponentInChildren<TextMeshProUGUI>() != null)
            tts_player.Play(gameEnds.GetComponentInChildren<TextMeshProUGUI>().text, _langOp); // Play TTS

        gameEnds.SetActive(true);
    }

    public void ShowInfo()
    {
        PlayButtonClick();
        var title = FindText(_gameObj.infoIntroductionTitle);
        var text =  FindText(_gameObj.infoIntroductionText);
        if(!string.IsNullOrEmpty(title) || !string.IsNullOrEmpty(text))
            GetComponent<TextManager>().ShowText(title, text, false, true, null, null);
    }

    public void SetEndApplicationPanel()
    {
        // Check if the text component was set
        if(panelEndAfterSurvey.GetComponentInChildren<TextMeshProUGUI>() != null)
            tts_player.Play(panelEndAfterSurvey.GetComponentInChildren<TextMeshProUGUI>().text, _langOp); // Play TTS

        panelEndAfterSurvey.SetActive(true);
    }

    public void ResetEventSystemInRoom()
    {
        //TODO: temporary fix
        _audio.enabled = true;
        _event.enabled = true;
    }

    public void StartGame()
    {
        _gameStarted = true;
        _playerController.enabled = true;
        SendInformationJustToBd("Game starts.");
        PlayButtonClick();
        startPanel.SetActive(false);
        _playerStats.StartTime();
        _audioManager.Play("Ambient");
    }

    public void PlayButtonClick()
    {
        _audioManager.Play("Click");
    }

    public void PlayNotification()
    {
        _audioManager.Play("Notification");
    }
    
    public bool IsInMainGame()
    {
        return _playerStats.IsFree();
    }

    public bool GameEnded()
    {
        return _gameEnded;
    }

    public void SetLevelInPlayer(string level)
    {
        _playerStats.SetLevel(level);
    }


    public void SuspendClues(bool b)
    {
        if (!b)
        {
            StartCoroutine(Wait());
        }else
            _cluesSuspended = true;
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(2F);
        _cluesSuspended = false;
    }

    public void PlayStartPanelTTS(){
        // Check if the text component was set
        if(startPanel.GetComponentInChildren<TextMeshProUGUI>() != null)
            tts_player.Play(startPanel.GetComponentInChildren<TextMeshProUGUI>().text, _langOp); // Play TTS
    }
}
