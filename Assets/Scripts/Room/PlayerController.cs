using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;

public class PlayerController : MonoBehaviour
{
    /// <summary>
    /// Player states (in accordance to the animations in the animator).
    /// Each constant contains the hash value that represents each state.
    /// </summary>
    public static class State
    {
        public static readonly int Moving = Animator.StringToHash("Base Layer.Moving");
        public static readonly int GrabingItem = Animator.StringToHash("Base Layer.GrabingItem");
    }

    /// <summary>
    /// Animator parameters (in accordance to the parameters set in the animator).
    /// Each constant contains the string value of each parameter.
    /// </summary>
    public static class AnimatorParameters
    {
        public static readonly string Movement = "Movement";
        public static readonly string Grab = "Grab";
        public static readonly string GrabT = "GrabT";
    }

    private InputActions _controls;
    private Animator _animator;
    private AnimatorStateInfo _state;

    // Movement
    private Vector2 _movementInput = Vector2.zero;
    private float _movementInputSpeed = 0f;
    private float _movementInputAcceleration = 0f;
    private float _targetAngle;
    private float _turningVelocity;
    private bool _firstPerson = false;
    private float _mouseX;
    private Vector2 _movementInputTmp;
    public bool moveToTheSides = false;
    private Vector3 _target = Vector3.zero;
    private bool _automaticMovement = false;


    // Grab
    private PlayerInteract _interact;

    [Header("Player Movement:")]
    public float movementInputAccelerationTime = 0.5f;
    public float turningTime = 0.1f;
    public float turningTimeFirstPerson = 1f;
    public Transform playerCamera;
    private float _acceleration;
    
    private AudioManager _audioManager;
    private GameRoomController _gameRoomController;

    private float _interactRadius;

    private List<Collision> _collisions = new List<Collision>();


    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _state = _animator.GetCurrentAnimatorStateInfo(0);
        _targetAngle = transform.eulerAngles.y;
        
        _interact = GetComponent<PlayerInteract>();
        _interactRadius = _interact.GetInteractRadius();
        
        _controls = new InputActions();
        _controls.Player.Movement.performed += context => StoreMovement();
        _controls.Player.Movement.performed += context => _movementInput = context.ReadValue<Vector2>();
        _controls.Player.Movement.canceled += context => _movementInput = context.ReadValue<Vector2>();
        _controls.Player.Interact.performed += context => Interact();

        _audioManager = FindObjectOfType<AudioManager>();
        _gameRoomController = FindObjectOfType<GameRoomController>();
        
    }

    private void StoreMovement()
    {
        TurnOffAutomaticMovement();
    }

    private void TurnOffAutomaticMovement()
    {
        _target = Vector3.zero;
        _automaticMovement = false;
        _animator.SetFloat(AnimatorParameters.Movement, 0);
        _turningVelocity = 0;
        _movementInput = Vector2.zero;
    }

    private void Interact()
    {
        _animator.SetFloat(AnimatorParameters.Movement, 0);

        // TODO: trigger future animation
        if(!_gameRoomController.GameEnded())
            _interact.Interact();
    }
    
//     void OnDrawGizmos()
//     {
//         // Draw a yellow sphere at the transform's position
//         Gizmos.color = Color.yellow;
//         Gizmos.DrawSphere(_target, 0.2f);
//    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Floor"))
            return;
        
        _collisions.Add(collision);
        
        if (_automaticMovement)
        {
            _gameRoomController.SendInformationJustToBd("[Movement] Player collided to " + collision.collider.name + " and automatic movement stopped.");
            var tmpTarget = _target;
            TurnOffAutomaticMovement();
            if (Vector3.Distance(transform.position, tmpTarget) < _interactRadius)
            {
                _interact.InteractAfterWalking();
            }
        }

    }

    private void OnCollisionExit(Collision collision)
    {
        if(_collisions.Contains(collision))
            _collisions.Remove(collision);
        
    }

/*    private void OnCollisionStay(Collision collision)
    {
        if (_automaticMovement && !collision.collider.CompareTag("Floor") && _interact.HaveItem())
        {
            _gameRoomController.SendInformationJustToBd("[Movement] Player collided to " + collision.collider.name + " and automatic movement stopped.");
            var tmpTarget = _target;
            TurnOffAutomaticMovement();
            if (Vector3.Distance(transform.position, tmpTarget) < _interactRadius)
            {
                _interact.InteractAfterWalking();
            }
        }
    }*/

    private void Update()
    {
        if (_automaticMovement)
        {
            // Calculate _movementInput
            var diff = _target - transform.position;
            var distance = Vector3.Distance(_target, transform.position);
            _movementInput.x = Mathf.Clamp(diff.x/distance, -1f, 1f);
            _movementInput.y = Mathf.Clamp(diff.z/distance, -1f, 1f);

            // Check if the position of the player and the target are approximately equal
            if (Vector3.Distance(transform.position, _target) < 1.2f)
            {
                //TODO: stop when collide
                TurnOffAutomaticMovement();
                _interact.InteractAfterWalking();
            }
            


        }
        _movementInputTmp = _movementInput;


        _state = _animator.GetCurrentAnimatorStateInfo(0);
        
        if (_state.fullPathHash == State.GrabingItem)
        {
            if (_movementInput.magnitude > 0.4)
                DisableGrabArea();
        }
        
        // Calculate current movement speed
        // No need to calculate if... (avoid unnecessary computing)
        if (_movementInputSpeed <= 0.001 && _movementInputAcceleration < 0)
        {
            _movementInputSpeed = 0;
            _movementInputAcceleration = 0;
        }
        else
        {
            _movementInputSpeed = Mathf.SmoothDamp(_movementInputSpeed,  _movementInputTmp.magnitude,
                ref _movementInputAcceleration, _acceleration);
        }
        
        // Update movement speed on animator to adjust animation
        _animator.SetFloat(AnimatorParameters.Movement, _movementInputSpeed);
        if (_firstPerson && !_automaticMovement && Mathf.Abs(_movementInputTmp.y) < 0.001 && Mathf.Abs(_movementInputTmp.x) > 0.001)
        {
            _animator.SetFloat(AnimatorParameters.Movement, 0.1f);
            _acceleration = 0f;
        }
        else
            _acceleration = movementInputAccelerationTime;

        if (_firstPerson && !_automaticMovement)
        {
            if (_movementInputTmp.y < -0.001f)
            {
                _animator.SetBool("backwards", true);
                //_animator.SetFloat(AnimatorParameters.Movement, 0.1f);
                //_acceleration = 0f;
            }
            else if (_animator.GetBool("backwards"))
            {
                _animator.SetFloat(AnimatorParameters.Movement, 0f);
                _movementInputSpeed = 0;
                _animator.SetBool("backwards", false);
            }
        }
        
        if(_animator.GetFloat(AnimatorParameters.Movement) < 0.2)
            _audioManager.Stop("Footsteps");
        else
        {
            _audioManager.Play("Footsteps");
        }

        // Update rotation
        if (_movementInputTmp.magnitude > 0.001 && _mouseX == 0)
        {
            
            if (_firstPerson && !_automaticMovement && moveToTheSides)
            {
                transform.position += transform.right *_movementInputTmp.x * 0.03f;
            }
            else
            {
                if (_firstPerson && !_automaticMovement && _movementInputTmp.y < 0)
                {
                    _targetAngle = Mathf.Atan2(_movementInputTmp.x, 0) * Mathf.Rad2Deg + (!_automaticMovement ? playerCamera.eulerAngles.y : 0);
                }
                else
                    _targetAngle = Mathf.Atan2(_movementInputTmp.x, _movementInputTmp.y) * Mathf.Rad2Deg + (!_automaticMovement ? playerCamera.eulerAngles.y : 0);

                if (Mathf.Abs(transform.eulerAngles.y - _targetAngle) < 0.001)
                {
                    _turningVelocity = 0;
                    return;
                }

                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, _targetAngle, ref _turningVelocity, _firstPerson && !_automaticMovement ? turningTimeFirstPerson : turningTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f);
            }

        }
        else
        {
            float angle = transform.eulerAngles.y + (_firstPerson && !_automaticMovement ? _mouseX : 0);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
            _turningVelocity = 0;
        }

    }

    /// <summary>
    ///  Event Functions in grab animation
    /// </summary>
    public void EnableGrabArea()
    {
        _animator.SetBool(AnimatorParameters.Grab, true);
    }
    public void DisableGrabArea()
    {
        _animator.SetBool(AnimatorParameters.Grab, false);
    }

    private void OnEnable()
    {
        _controls.Player.Enable();
    }

    private void OnDisable()
    {
        _controls.Player.Disable();
    }

    public void ChangeController()
    {
        _firstPerson = !_firstPerson;
    }

    public void AddRotation(float x)
    {
        _mouseX = x;
        if (x != 0 && _firstPerson && _automaticMovement)
            TurnOffAutomaticMovement();
        
    }
    
    public void StartMovement(Vector3 obj)
    {
        obj.y = 0;
        _target = obj;
        _automaticMovement = true;

        // If is already colliding with something
        if (_collisions.Count > 0 && Vector3.Distance(transform.position, _target) < _interactRadius)
        {
            TurnOffAutomaticMovement();
            _interact.InteractAfterWalking();
        }
        

    }

    public void SetMovementSpeed(float value)
    {
        _movementInputSpeed = value;
    }

}
