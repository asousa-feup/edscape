using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.UIElements;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{
    public GameObject videoUI;
    private VideoPlayer _videoPlayer;
    private double _videoDuration;
    private bool _on = false;
    private bool _ready = false;
    private Item _item = null;
    private MiniGameController _miniGame = null;

    private GameRoomController _gameRoomController;
    
    private bool _isStartVideo = false;

    private void Awake()
    {
        _gameRoomController = FindObjectOfType<GameRoomController>();
        _videoPlayer = FindObjectOfType<VideoPlayer>();
    }

    private void Update()
    {
        if (_on)
        {
            if (!_ready && _videoPlayer.url != null)
            {
                // Wait until video is ready
                // TODO: Even with Prepare() the message "First video frame not zero" appears and first frame is from the last video
                if(!_videoPlayer.isPrepared)
                {
                    //Debug.Log("Preparing Video");
                    return;
                }
                _ready = true;
                //Play Video
                _videoPlayer.frame = 0;
                //Length: For URL sources, this will only be set once the source preparation is completed
                _videoDuration = _videoPlayer.length;
                _videoPlayer.GetComponent<VideoPlayer>().Play();
                //_rawImage.SetActive(true);
            }

            if(_videoDuration <= 0)
            {
                videoUI.SetActive(false);
                _on = false;
                _ready = false;
                var waitItem = _item;
                _videoPlayer.GetComponent<VideoPlayer>().Stop();
                _item = null;
                
                if (waitItem != null)
                    waitItem.InteractAfterIntros();
                if (_miniGame != null)
                    _miniGame.InteractAfterIntros();
                
                _miniGame = null;
                //videoPlayer.url = null;
                
                if(_isStartVideo)
                    _gameRoomController.PlayStartPanelTTS();
            }
            else
                _videoDuration -= Time.deltaTime;
        }
    }

    public void PrepareIntroduction(string videoPath, Item item, MiniGameController miniGame)
    {
        //videoPlayer.clip = videoClip;
        _miniGame = miniGame;
        _item = item;
        StartCoroutine(PrepareVideo(videoPath));
    }

    // Method that will set the video player url and call Prepare method
    IEnumerator PrepareVideo(string videoPath){
        // Check if the video path is an url
        if(IsVideoPathAnURL(videoPath)){
            // Get the video streaming url and set it to the video player url
            yield return Youtube.GetStreamingUrl(videoPath);
            // Get the streaming url value
            string streamingUrl = Youtube.StreamingUrl;
            
            // Check if the streaming url is valid
            if (string.IsNullOrEmpty(streamingUrl)){
                // Turn off the video player
                _ready = true;
                _videoDuration = 0;
                yield break;
            }
            
            // Set the video player url with the streaming url
            _videoPlayer.url = Youtube.StreamingUrl;
        }else{
            // Set the video player url with a file path
            _videoPlayer.url = Path.Combine(Application.streamingAssetsPath, videoPath);
        }
       
        _videoPlayer.Prepare();
    }

    // Function that return true if the video path is an url, and false otherwise
    bool IsVideoPathAnURL(string videoPath){
        Uri uriResult;
        // First we test if it is possible to create an URI from the path
        // Second and third we test if it has an http or https scheme
        return Uri.TryCreate(videoPath, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
    }
    
    public void StopVideo()
    {
        _gameRoomController.PlayButtonClick();
        _gameRoomController.SuspendClues(true);
        StartCoroutine(Wait());
    }
    
    IEnumerator Wait()
    {
        InputSystem.DisableDevice(Mouse.current);
        yield return new WaitForSeconds(0.5F);
        InputSystem.EnableDevice(Mouse.current);
        _gameRoomController.SuspendClues(false);
        
        if(_on)
        {
            _videoDuration = 0;
            if (_item == null && _miniGame == null)
                _gameRoomController.ResumePlayer();
        }
    }


    public void StartVideo(bool isStartVideo)
    {
        _isStartVideo = isStartVideo;
        if(_item != null)
            _gameRoomController.SendInformationJustToBd("[Item " + _item.GetId() + "] Show video.");
        else if(_miniGame != null)
            _gameRoomController.SendInformationJustToBd("[Mini game " + _gameRoomController.GetMiniGamePath() + "] Show video.");
        else
            _gameRoomController.SendInformationJustToBd("[Intro] Show video.");

        if(_miniGame != null)
            _miniGame.DestroyLoadCanvas();
        
        videoUI.SetActive(true);
        _on = true;
        _gameRoomController.PausePlayer();
    }
    
    /*public void ShowVideo(string videoPath, Item item)
    {
        //videoPlayer.clip = videoClip;
        videoPlayer.url = Path.Combine(Application.streamingAssetsPath, videoPath);
        videoPlayer.Prepare();
        _item = item;
        _on = true;
        _gameRoomController.PausePlayer();

    }*/
}
