using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static class Youtube
{
    public static string StreamingUrl { get; private set; }

    // Get the streaming URL that can be used in the video player component
    public static IEnumerator GetStreamingUrl(string url)
    {
        // Get the video ID
        string videoId = GetVideoID(url);
        // Build API Get link
        string apiGetURL = "https://yt-api.p.rapidapi.com/dl?id=" + videoId;

        // Set Get request using the previous URL
        using (UnityWebRequest webRequest = UnityWebRequest.Get(apiGetURL))
        {   
            // Set the two required headers
            webRequest.SetRequestHeader("X-RapidAPI-Key", "d28908db6dmsh095e6ed9e6d9e73p1d5f05jsnc192c44fb046");
            webRequest.SetRequestHeader("X-RapidAPI-Host", "yt-api.p.rapidapi.com");

            // Request and wait for the response.
            yield return webRequest.SendWebRequest();

            // Check if the request went well
            if (webRequest.result == UnityWebRequest.Result.Success){
                // Deserialize the response to get the necessary information about the youtube video
                YoutubeVideoInfo youtubeVideoInfo = JsonUtility.FromJson<YoutubeVideoInfo>(webRequest.downloadHandler.text);
                // Get the streaming URL of the YouTube video
                StreamingUrl = youtubeVideoInfo.GetStreamingUrl();
            }else{
                // If something went wrong.
                Debug.Log("Error!");
            }
        }
    }

    // Get the video ID from a given url
    static string GetVideoID(string url){
        Uri myUri = new Uri(url);
        return HttpUtility.ParseQueryString(myUri.Query).Get("v");
    }
}

[Serializable]
class YoutubeVideoInfo
{
    public Format [] formats;

    public string GetStreamingUrl(){
        if (formats == null)
            return null;

        if (formats.Length != 0)
            return formats[formats.Length - 1].url;
        
        return null;
    }
}

[Serializable]
class Format
{
    public string url;
}