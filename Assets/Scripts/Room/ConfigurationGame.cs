using System;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class ConfigurationGame : MonoBehaviour
{
    public PlayerStats playerStats;
    private GameRoomController _gameController;
    private Canvas _canvas;

    void Awake()
    {
        var gameFile = PlayerPrefs.GetString("gameFile");
        _gameController = FindObjectOfType<GameRoomController>();
        _canvas = FindObjectOfType<Canvas>();
        _gameController.CreateLoadPanel(_canvas);

        StartCoroutine(GetFile(Application.streamingAssetsPath + "/" + gameFile));
    }
    IEnumerator GetFile(string gameFile)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(gameFile))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            var saveString = webRequest.downloadHandler.text;
            _gameController.DestroyLoadPanel(_canvas);
            
            try
            {
                var configGameObj = JsonUtility.FromJson<ConfigGameObj>(saveString);
                configGameObj.userEmail = PlayerPrefs.GetString("userEmail");

                var gameRoom = gameObject.GetComponent<GameRoomController>();
                
                // Update game values
                // - final congratulation text + version game + issuer email + creation date + user email
                gameRoom.SetGameInfo(configGameObj, PlayerPrefs.GetString("lanOpID"));

                configGameObj.PlaceInteractiveObjects();

                // - intro text/video
                bool asText = !string.IsNullOrEmpty(gameRoom.FindText(configGameObj.introductionTitle)) || !string.IsNullOrEmpty(gameRoom.FindText(configGameObj.introductionText));
                bool asVideo = !string.IsNullOrEmpty(gameRoom.FindPath(configGameObj.introductionVideoPath));
                bool asInfo = !string.IsNullOrEmpty(gameRoom.FindText(configGameObj.infoIntroductionTitle)) || !string.IsNullOrEmpty(gameRoom.FindText(configGameObj.infoIntroductionText));
                if (asVideo)
                    GetComponent<VideoManager>().PrepareIntroduction(gameRoom.FindPath(configGameObj.introductionVideoPath), null, null);
                    //gameObject.GetComponent<AddressablesManager>().SetIntroduction(configGameObj.introductionVideoId);
                if (asText || asInfo)
                    GetComponent<TextManager>().ShowText(gameRoom.FindText(configGameObj.introductionTitle),gameRoom.FindText(configGameObj.introductionText), asVideo, null, null, gameRoom.FindText(configGameObj.infoIntroductionTitle),gameRoom.FindText(configGameObj.infoIntroductionText), asText, asInfo);
                else if(asVideo)
                    GetComponent<VideoManager>().StartVideo(true);
                
                // - initial time
                playerStats.SetAndFrozeTime(configGameObj.initialTime);
                // - clues
                playerStats.SetClues(configGameObj.clueObjs);
                // - thoughts
                playerStats.SetThoughts(configGameObj.thoughtObjs);
                // - space
                gameObject.GetComponent<AddressablesManager>().SetSpace(configGameObj.spaceId);
                // - objects
                gameObject.GetComponent<AddressablesManager>().SetItems(configGameObj.interactiveObjs);


            }
            catch(Exception e)
            {
                Debug.LogError(e);
            }
        }
    }
}

 [Serializable] public class ConfigGameObj 
 {
    //---------------------------------------------------------- 
    // Will be generated through code
    public string gameId;
    public string spaceId; // will not be necessary
    public string userEmail; // don't know were it is used
    public string issuerEmail;
    public string creationDate;
    //----------------------------------------------------------
    // New properties
    public string ieerPurpose;
    public string [] ieerLearningObjectives;
    public string ieerType;
    public string requiredKnowlage;
    public int numOfParticipants;
    public string ieerCoursePosition;
    public string ieerStoryType;
    public bool ieerForEvaluation;
    public bool languageBarriers;
    public bool differentLevelsOfImpairments;
    public float selfReflectionTime;
    //----------------------------------------------------------
    public float initialTime;
    public List<string> textOptionIDs;
    public List<TextOp> infoIntroductionTitle;
    public List<TextOp> infoIntroductionText;
    public List<TextOp> introductionTitle;
    public List<TextOp> introductionText;
    public List<PathOp> introductionVideoPath;
    public List<TextOp> startText;
    public List<TextOp> timeEndedText;
    public List<TextOp> congratulationsText;
    public List<TextOp> endAfterSurveyText;
    public List<PathOp> initialSurveyPath;
    public List<PathOp> finalSurveyPath;
    //----------------------------------------------------------
    public List<ClueObj> clueObjs;
    public List<ThoughtObj> thoughtObjs;
    public List<IterativeObj> interactiveObjs;
    //----------------------------------------------------------
    // Private list with all the TextOp that are waiting for the translation
    private List<TextOp> translatableFields;

    // Waits for the translation to be completed
    public IEnumerator WaitForTranslation(){
        bool done = false;
        BuildTranslatableFieldsList();

        // Wait for the translation to complete
        while(!done){
            // Sets done to true, if the translation is completed this value will not be changed
            done = true;
            // Loops through all the translatable fields
            foreach(TextOp textOp in translatableFields){
                //Checks if the translation has not been made
                if (textOp.text == null){
                    done = false;
                    yield return null;
                    break;
                }
                yield return null;
            }
        }

        // Wait for the translation of all the clues
        foreach (ClueObj clueObj in clueObjs){
            yield return clueObj.WaitForTranslation();
        }

        // Wait for the translation of all the thoughts
        foreach (ThoughtObj thoughtObj in thoughtObjs){
            yield return thoughtObj.WaitForTranslation();
        }

        // Wait for the translation of all the clues
        foreach (IterativeObj iterativeObj in interactiveObjs){
            yield return iterativeObj.WaitForTranslation();
        }
    }

    // Populates the translatableFields list
    private void BuildTranslatableFieldsList(){
        translatableFields = new List<TextOp>();

        // Adds all the List<TextOp> to the translatableFields
        translatableFields.AddRange(infoIntroductionTitle);
        translatableFields.AddRange(infoIntroductionText);
        // translatableFields.AddRange(introductionTitle);
        // translatableFields.AddRange(introductionText);
        translatableFields.AddRange(startText);
        translatableFields.AddRange(timeEndedText);
        translatableFields.AddRange(congratulationsText);
        translatableFields.AddRange(endAfterSurveyText);
    }

    // Prepares the class to be saved
    // public IEnumerator PrepareToSave(string tempFolder, string actualFolder){
    //     // Save uploaded files to the IEER directory
    //     yield return SaveUploadedFiles.Save(introductionVideoPath, tempFolder, actualFolder);
    //     yield return SaveUploadedFiles.Save(initialSurveyPath, tempFolder, actualFolder);
    //     yield return SaveUploadedFiles.Save(finalSurveyPath, tempFolder, actualFolder);

    //     foreach (IterativeObj iterativeObj in interactiveObjs){
    //         yield return iterativeObj.PrepareToSave(tempFolder, actualFolder);
    //     }
    // }

    // Generate the position and rotation of this interactive object
    public void PlaceInteractiveObjects(){
        foreach (IterativeObj interactObj in interactiveObjs){
            InteractiveObjectPlacer.SelectRandomPlacementArea(interactObj.id);
            interactObj.initialPosition = InteractiveObjectPlacer.GetInitialPosition(interactObj.id);
            interactObj.finalPosition = InteractiveObjectPlacer.GetFinalPosition(interactObj.id);
            interactObj.initialRotation = InteractiveObjectPlacer.GetInitialRotation(interactObj.id);
            interactObj.finalRotation = InteractiveObjectPlacer.GetFinalRotation(interactObj.id);
        }
    }
 }
    
[Serializable]
public class ClueObj
{
    public string itemIdNotOwned;
    public string inLevel;
    public float showClueAfterTime;
    public List<TextOp> message;
    public float penalization;
    public bool destroyAfterShowing;
    public bool suppressNextCluesUntilGrabItem;    

    // Waits for the translation to be completed
    public IEnumerator WaitForTranslation(){
        bool done = false;

        // Wait for the translation to complete
        while(!done){
            // Sets done to true, if the translation is completed this value will not be changed
            done = true;
            // Loops through all the messages
            foreach(TextOp textOp in message){
                // Checks if the translation has not been made
                if (textOp.text == null){
                    done = false;
                    yield return null;
                    break;
                }
                yield return null;
            }
        }
    }
}

[Serializable]
public class ThoughtObj
{
    public string itemGrabbed;
    public float showThoughtAfterTime;
    public List<TextOp> message;

    // Waits for the translation to be completed
    public IEnumerator WaitForTranslation(){
        bool done = false;

        // Wait for the translation to complete
        while(!done){
            // Sets done to true, if the translation is completed this value will not be changed
            done = true;
            // Loops through all the messages
            foreach(TextOp textOp in message){
                // Checks if the translation has not been made
                if (textOp.text == null){
                    done = false;
                    yield return null;
                    break;
                }
                yield return null;
            }
        }
    }
}

[Serializable] public class IterativeObj
{
    public string id;
    public string entersLevel;
    //----------------------------------------------------------
    // Will be generated using PCG?
    public float[] initialPosition;
    public float[] initialRotation;
    public float[] finalPosition;
    public float[] finalRotation;
    //----------------------------------------------------------
    public string[] unlockItemsIds;
    public List<TextOp> messageLocked;
    public List<TextOp> messageSucceeded;
    public List<TextOp> messageSucceededWithHelp;
    public List<TextOp> messageExited;
    public List<TextOp> messageFailed;
    public MiniGame miniGame;
    public List<TextOp> introTitle;
    public List<TextOp> introText;
    public List<PathOp> videoPath;
    public List<TextOp> finalIntroTitle;
    public List<TextOp> finalIntroText;
    public List<PathOp> finalVideoPath;
    public bool winsGame;
    //----------------------------------------------------------
    // Private list with all the TextOp that are waiting for the translation
    private List<TextOp> translatableFields;

    // Waits for the translation to be completed
    public IEnumerator WaitForTranslation(){
        bool done = false;
        BuildTranslatableFieldsList();

        // Wait for the translation to complete
        while(!done){
            // Sets done to true, if the translation is completed this value will not be changed
            done = true;
            // Loops through all the translatable fields
            foreach(TextOp textOp in translatableFields){
                //Checks if the translation has not been made
                if (textOp.text == null){
                    done = false;
                    yield return null;
                    break;
                }
                yield return null;
            }
        }
    }

    // Populates the translatableFields list
    private void BuildTranslatableFieldsList(){
        translatableFields = new List<TextOp>();

        // Adds all the List<TextOp> to the translatableFields
        translatableFields.AddRange(messageLocked);
        translatableFields.AddRange(messageSucceeded);
        // translatableFields.AddRange(messageSucceededWithHelp);
        translatableFields.AddRange(messageExited);
        // translatableFields.AddRange(messageFailed);
        translatableFields.AddRange(introTitle);
        translatableFields.AddRange(introText);
        translatableFields.AddRange(finalIntroTitle);
        translatableFields.AddRange(finalIntroText);
    }

    // Prepares the class to be saved
    // public IEnumerator PrepareToSave(string tempFolder, string actualFolder){
    //     // Save uploaded files to the IEER directory
    //     yield return SaveUploadedFiles.Save(videoPath, tempFolder, actualFolder);
    //     yield return SaveUploadedFiles.Save(finalVideoPath, tempFolder, actualFolder);

    //     // Check if the miniGame is valid
    //     if (miniGame != null){
    //         yield return miniGame.PrepareToSave(tempFolder, actualFolder);
    //     }
    // }
}

[Serializable]
public class MiniGame
{
    public string id;
    public List<PathOp> pathOptions;

    // Prepares the class to be saved
    // public IEnumerator PrepareToSave(string tempFolder, string actualFolder){
    //     // Save uploaded files to the IEER directory
    //     yield return SaveUploadedFiles.Save(pathOptions, tempFolder, actualFolder); 
    // }
}

[Serializable]
public class PathOp
{
    public string id;
    public string path;
    // public string url;
}

[Serializable]
public class TextOp
{
    public string id;
    public string text;

    // Updates the text value after the translation is completed
    public void UpdateTextAfterTranslation(object sender, DownloadStringCompletedEventArgs e){
        // If the request was not canceled and did not throw an exception, display the resource.
        if (!e.Cancelled && e.Error == null)
        {
            string result = (string) e.Result;
            // Remove external array 
            result = result.Substring(1, result.Length-2);

            // Reverse String to calculate the index of the last comma
            char[] stringArray = result.ToCharArray();
            Array.Reverse(stringArray);
            string reversedStr = new string(stringArray);

            // Calculate commaIndex
            // Get index of the comma in the reversed string
            // Calculate the index of the last comma knowing the index of the comma in the reversed string
            int commaIndex = (result.Length - reversedStr.IndexOf(',')) - 1;

            // Build translation result json
            result = "{\"text\": " + result.Substring(1, commaIndex) + "\"language\": " + result.Substring(commaIndex+1);
            result = result.Substring(0,result.Length-1) + "}";

            // Convert the result json string to an object
            TranslationResult translationResult = JsonUtility.FromJson<TranslationResult>(result);

            // Assign translation result to the text field
            text = translationResult.text;

            return;
        }
        // Otherwise
        text = "An error has occurred during translation!";
    }
}

[Serializable]
class TranslationResult{
    public string text;
    public string language;
}

// static class SaveUploadedFiles
// {
//     public static IEnumerator Save(List<PathOp> uploadedFiles, string tempFolder, string actualFolder){
//         // Checks if the list is empty
//         if (uploadedFiles.Count != 0){
//             foreach(PathOp pathOp in uploadedFiles){
//                 // Check if the file path is empty, null or doesn't exists
//                 if(string.IsNullOrEmpty(pathOp.path) || (!System.IO.File.Exists(pathOp.path)) ){
//                     yield return null;
//                     continue;
//                 }

//                 string tempPath = tempFolder + "/" + Path.GetFileName(pathOp.path);
//                 string actualPath = actualFolder + "/" + Path.GetFileName(pathOp.path);

                
//                 using (UnityWebRequest www = UnityWebRequest.Get(pathOp.path))
//                 {   
//                     // Request and wait for the response.
//                     yield return www.SendWebRequest();
                    
//                     // Check if something went wrong.
//                     if ((www.result == UnityWebRequest.Result.ConnectionError) || (www.result == UnityWebRequest.Result.ProtocolError))
//                     {
//                         yield return null;
//                         continue;
//                     }
//                     else
//                     {
//                         // If all good, save the file in the temporary folder.
//                         System.IO.File.WriteAllBytes(Application.streamingAssetsPath + "/" + tempPath, www.downloadHandler.data);
//                         pathOp.path = actualPath;
//                     }
//                 }
                
//                 yield return null;
//             }
//         }
//     }
// }

