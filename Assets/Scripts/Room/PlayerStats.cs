using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.UI;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour
{
    // Items 
    private List<string> _currentItems = new List<string>();
    private List<string> _path = new List<string>();
    public GameObject[] itemsSlots;
    private bool _isFree = true;
    private int _interactedCount = 0;

    private string _currLevel = "";

    // Clues
    private double _durationUntilShowClue;
    private List<ClueObj> _clueObjs;
    private ClueObj _activeClueObj;
    private Dictionary<ClueObj, bool> _stateClues = new Dictionary <ClueObj, bool>();
    public Image cluesButton;
    private bool _supressClueObjs = false;
    private bool _supressCluesUntilNextLevel = false;
    
    // Thoughts
    private Dictionary<string, List<ThoughtObj>> _thoughtObjs = new Dictionary<string, List<ThoughtObj>>();
    private ThoughtObj _activeThoughtObj;
    private double _durationUntilShowThought;
    private string _activeItemGrabbed = "None";
    
    // 2D games
    private bool _inMainGame = true;
    private Item _currentItemContaining2DGame;
    
    // Time
    private double _timeInSec = 15;
    private double _timeColorPenalty = 0;
    private bool _timeFroze = false;
    private Animator _timeAnimator;
    public TextMeshProUGUI time;
    private double elapsed = 0;
    private double _lastTime = 0;
    private bool _isTimeActive = false;
    private bool _timeEnded = false;
    private bool _gameStarted = false;

    private DateTime _correctEndTime;
    // Text chat
    [SerializeField]
    protected TextLogControl textLogControl;
    
    private GameRoomController _gameRoomController;

    private bool _gameEnded = false;

    public void GameEnded()
    {
        _gameEnded = true;
    }
    

    private void Awake()
    {
        // Reset
        PlayerPrefs.SetInt("penalty", 0);
        PlayerPrefs.SetInt("exitButton", 0);
        PlayerPrefs.SetInt("hintsUsed", 0);
        
        textLogControl = FindObjectOfType<TextLogControl>();
        _timeAnimator = time.transform.parent.GetComponent<Animator>();
        _gameRoomController = FindObjectOfType<GameRoomController>();
    }

    private void Update()
    {
        if(_timeFroze)
            return;
        
        // Update game time
        UpdateTime();
        
        // Add penalization to game time after getting out of a 2D game
        if (!_inMainGame)
            CheckIfOutAlready();
        
        // Update message if the time reaches the respective time to show a clue
        if (_activeClueObj != null && !_supressClueObjs && (!_supressCluesUntilNextLevel || !_activeClueObj.destroyAfterShowing))
        {
            if (_durationUntilShowClue <= 0)
            {
                SetMessageToBd("[Clues] Show clue after specific time if player does not own, item: " + _activeClueObj.itemIdNotOwned + ", clue: " + _gameRoomController.FindText(_activeClueObj.message) + ".");
                ShowClue();
            }
            else
                _durationUntilShowClue -= Time.deltaTime;
        }
        
        // Update message if the time reaches the respective time to show a thought
        if (_activeThoughtObj != null)
        {
            if (_durationUntilShowThought <= 0)
                ShowThought();
            else
                _durationUntilShowThought -= Time.deltaTime;
        }

    }

    private void UpdateClueObj()
    {
        _supressClueObjs = _activeClueObj?.suppressNextCluesUntilGrabItem ?? false;
        if (_supressClueObjs && _activeClueObj != null && (_currentItems.Contains(_activeClueObj.itemIdNotOwned) || string.IsNullOrEmpty(_activeClueObj.itemIdNotOwned)))
        {
            _supressClueObjs = false;
            _activeClueObj.suppressNextCluesUntilGrabItem = false;
        }
        _activeClueObj = null;

        List<ClueObj> tmp = new List<ClueObj>();

        // Check if current items represent a state of the game that a clue is given
        foreach (var clueObj in _clueObjs)
        {
            
            if (string.IsNullOrEmpty(_gameRoomController.FindText(clueObj.message)))
            {
                tmp.Add(clueObj);
                continue;
            }

            // Check if state does not have the clue item or clue is about a state independent of items
            if (!_currentItems.Contains(clueObj.itemIdNotOwned) || string.IsNullOrEmpty(clueObj.itemIdNotOwned))
            {
                _activeClueObj = clueObj;
                // Prepare to show the message clue in a determinate time
                _durationUntilShowClue = _activeClueObj.showClueAfterTime;

                // Current clue
                _supressCluesUntilNextLevel = !((string.IsNullOrEmpty(_activeClueObj.inLevel) || _activeClueObj.inLevel.Equals(_currLevel)));
                // Next clue
                if (!_supressCluesUntilNextLevel && _stateClues[_activeClueObj])
                {
                    var nextClueObjIndex = _clueObjs.IndexOf(_activeClueObj) + 1;
                    if (nextClueObjIndex < _clueObjs.Count - 1)
                    {
                        if (!((string.IsNullOrEmpty(_clueObjs[nextClueObjIndex].inLevel) ||
                               _clueObjs[nextClueObjIndex].inLevel.Equals(_currLevel))))
                            _supressCluesUntilNextLevel = true;
                    }
                }
                
                FadeCluesButton(_supressClueObjs || _supressCluesUntilNextLevel || (_clueObjs.Count.Equals(1) && _stateClues[_activeClueObj]) ? 0.25f: 1f);
                
                foreach (var obj in tmp)
                {
                    _clueObjs.Remove(obj);
                }
                
                return;
            }

            // If contains the item the clue is not needed -> remove clue
            tmp.Add(clueObj);
        }

        foreach (var obj in tmp)
        {
            _clueObjs.Remove(obj);
        }
        
        // Fade the clue button if no clues available
        FadeCluesButton(0.25f);
    }

    private void FadeCluesButton(float value)
    {
        var tempColor = cluesButton.color;
        tempColor.a = value;
        cluesButton.color = tempColor;
    }

    private void CheckIfOutAlready()
    {
       if(PlayerPrefs.GetInt("inMainGame").Equals(1))
       {
           //TODO: temporary fix
           FindObjectOfType<InputSystemUIInputModule>().enabled = true;
           FindObjectOfType<Canvas>().enabled = true;
           FindObjectOfType<EventSystem>().enabled = true;
           
            _inMainGame = true;
            
            var penalization = PlayerPrefs.GetInt("penalty");
            if (penalization > 0)
            {
                if (_isTimeActive)
                {
                    _timeInSec = _timeColorPenalty;
                    elapsed = 0;
                }
                _lastTime = _timeInSec;
                _timeColorPenalty = _timeInSec - penalization;
                if (_timeColorPenalty < 0)
                    _timeColorPenalty = 0;
                _timeAnimator.SetTrigger("triggerOn");
                _isTimeActive = true;
                SetMessage("(-" + penalization + " s)", TextLogItem.Type.Penalty);
            }

            if (PlayerPrefs.GetInt("exitButton").Equals(1))
            {
                SetMessage(_currentItemContaining2DGame.GetExitedMsg(), null);
                SetMessageToBd("[Mini game " + _gameRoomController.GetMiniGamePath() +"] Exited game, penalization: " + penalization + "s , message: " + _currentItemContaining2DGame.GetExitedMsg()+".");
            }
            else if (PlayerPrefs.GetInt("exitButton").Equals(0))
            {
                if (PlayerPrefs.GetInt("hintsUsed").Equals(1))
                {
                    SetMessage(_currentItemContaining2DGame.GetSucceededWithHelpMsg(), null);
                    SetMessageToBd("[Mini game " + _gameRoomController.GetMiniGamePath() +"] Won game with help, penalization: " + penalization + "s , message: " + _currentItemContaining2DGame.GetSucceededWithHelpMsg()+".");
                }
                else
                {
                    SetMessage(_currentItemContaining2DGame.GetSucceededMsg(), null);
                    SetMessageToBd("[Mini game " + _gameRoomController.GetMiniGamePath() +"] Won game without help, penalization: " + penalization + "s , message: " + _currentItemContaining2DGame.GetSucceededMsg()+".");
                }
            }
            else if (PlayerPrefs.GetInt("exitButton").Equals(2))
            {
                SetMessage(_currentItemContaining2DGame.GetFailedMsg(), null);
                SetMessageToBd("[Mini game " + _gameRoomController.GetMiniGamePath() +"] Time ended and game not solved, penalization: " + penalization + "s , message: " + _currentItemContaining2DGame.GetFailedMsg()+".");
            }

            // Reset
            PlayerPrefs.SetInt("penalty", 0);
            PlayerPrefs.SetInt("hintsUsed", 0);

            // Interact with item
            _currentItemContaining2DGame.Interact();
            _currentItemContaining2DGame = null;
       }
    }

    private void UpdateTime()
    {
        //_timeInSec -= Time.deltaTime;
        var timeSpan = _correctEndTime - DateTime.Now;
        _timeInSec = timeSpan.TotalSeconds;

        if (_timeInSec < 0)
        {
            _timeInSec = 0;
        }
        if (_timeInSec <= 0 && !_timeEnded && _gameStarted)
        {
            _timeEnded = true;
            _timeAnimator.SetTrigger("timeEnded");
            textLogControl.SendTimeEndedMessage();
        }


        if (_isTimeActive && _timeColorPenalty < _timeInSec)
        {
            var tmpTime = _timeInSec;
            _timeInSec = Mathf.Lerp((float) _lastTime, (float) _timeColorPenalty, (float) (elapsed / 1f));
            _correctEndTime = _correctEndTime.AddSeconds(-(tmpTime-_timeInSec));
            //Debug.Log(_correctEndTime);
            elapsed += Time.deltaTime;
        }
        else
        {
            elapsed = 0;
            _isTimeActive = false;
        }

        var hours = Math.Floor(_timeInSec/3600f);
        var minutes = Math.Floor((_timeInSec%3600f)/60f);
        var seconds = Math.Floor(_timeInSec % 60f);

        time.text = (hours < 10 ? "0" + hours : hours.ToString()) + ":" + 
                    (minutes < 10 ? "0" + minutes : minutes.ToString()) + ":" +
                    (seconds < 10 ? "0" + seconds : seconds.ToString());
    }

    public void SetMessage(string phrase, Color? color)
    {
        if (string.IsNullOrEmpty(phrase) || _gameEnded)
            return;

        textLogControl.LogText(phrase, color);
    }

    private void SetMessageToBd(string phrase)
    {
        if(_gameEnded)
            return;
        
        textLogControl.LogText(phrase, TextLogItem.Type.OnlyToBd);
    }
    
    public void AddItem(string id)
    {
        _currentItems.Add(id);
        _path.Add(id);
        
        // Update clues system with the new state of the game (current items)
        UpdateClueObj();
        
        // If grabbed other item remove active item thoughts
        _thoughtObjs.Remove(_activeItemGrabbed);
        _activeItemGrabbed = id;
        UpdateThoughtObj();

        _interactedCount++;
    }
    
    public void AddItem(string objID, bool b)
    {
        AddItem(objID);
        _interactedCount--;
        
    }
    
    public void SetLevel(string id)
    {
        _currLevel = id;
        UpdateClueObj();
    }
    
    public bool Contains(string id)
    {
        return _currentItems.Contains(id);
    }

    public int NumberItems()
    {
        return _interactedCount;
    }

    public void InMainGame(bool b)
    {
        _inMainGame = b;
    }
    
    public void WaitForResultToShowMessage(Item item)
    {
        _currentItemContaining2DGame = item;
    }

    /// <summary>
    /// Configuration File Functions
    /// </summary>
    public void SetTime(double initialTime)
    {
        _timeInSec = initialTime;
    }

    public void SetClues(List<ClueObj> clueObjs)
    {
        _clueObjs = clueObjs;
        foreach (var clue in clueObjs)
            _stateClues.Add(clue, false);
        UpdateClueObj();
    }

    /// <summary>
    /// Clues Functions
    /// </summary>
    public void GetClue()
    {
        if (_activeClueObj == null || _supressClueObjs || _supressCluesUntilNextLevel)
        {
            SetMessageToBd("[Clues] Clue asked but none available.");
            return;
        }

        // if active clue was already seen
        if (_activeClueObj != null && _stateClues[_activeClueObj])
        {
            // Delete clues that already were used
            _clueObjs.Remove(_activeClueObj);
            UpdateClueObj();

            if (_activeClueObj == null)
            {
                SetMessageToBd("[Clues] No more clues to show.");
                return;
            }
        }

        if (_activeClueObj.penalization != 0)
        {
            SetMessage("(-" + _activeClueObj.penalization + " s)", TextLogItem.Type.Penalty);
            if (_isTimeActive)
            {
                _timeInSec = _timeColorPenalty;
                elapsed = 0;
            }

            _lastTime = _timeInSec;
            _timeColorPenalty = _timeInSec - _activeClueObj.penalization;
            if (_timeColorPenalty < 0)
                _timeColorPenalty = 0;
            _timeAnimator.SetTrigger("triggerOn");
            _isTimeActive = true;
        }
        SetMessageToBd("[Clues] Clue asked, clue: " + _gameRoomController.FindText(_activeClueObj.message) + ", penalization: -" + _activeClueObj.penalization +" s.");
        ShowClue();
    }

    private void ShowClue()
    {
        SetMessage(_gameRoomController.FindText(_activeClueObj.message), TextLogItem.Type.Clue);
        _stateClues[_activeClueObj] = true;
        
        if(_activeClueObj.destroyAfterShowing)
            _clueObjs.Remove(_activeClueObj);
        
        // Update clues system with the new clues available
        UpdateClueObj();
    }
    
    private void ShowThought()
    {
        SetMessageToBd("[Thoughts] Show thought at specific time after grabbing item, item: " + _activeThoughtObj.itemGrabbed + ", thought: " + _gameRoomController.FindText(_activeThoughtObj.message) + ".");
        SetMessage(_gameRoomController.FindText(_activeThoughtObj.message), TextLogItem.Type.Thought);
        _thoughtObjs[_activeThoughtObj.itemGrabbed].Remove(_activeThoughtObj);
        if(_thoughtObjs[_activeThoughtObj.itemGrabbed].Count == 0)
            _thoughtObjs.Remove(_activeThoughtObj.itemGrabbed);
        UpdateThoughtObj();
    }
    
    private void UpdateThoughtObj()
    {
        _activeThoughtObj = null;

        if(!_thoughtObjs.ContainsKey(_activeItemGrabbed))
            return;
        
        _activeThoughtObj = _thoughtObjs[_activeItemGrabbed][0];
        
        if (string.IsNullOrEmpty(_gameRoomController.FindText(_activeThoughtObj.message)))
        {
            _thoughtObjs[_activeThoughtObj.itemGrabbed].Remove(_activeThoughtObj);
            if(_thoughtObjs[_activeThoughtObj.itemGrabbed].Count == 0)
                _thoughtObjs.Remove(_activeThoughtObj.itemGrabbed);
            UpdateThoughtObj();
        }
        
        _durationUntilShowThought = _activeThoughtObj.showThoughtAfterTime;
    }
    
    public double GetTime()
    {
        return _timeInSec;
    }

    public bool IsFree()
    {
        return _isFree;
    }

    public void SetFree(bool value)
    {
        _isFree = value;
    }

    public void SetThoughts(List<ThoughtObj> thoughtObjs)
    {
        foreach (var thought in thoughtObjs)
        {
            thought.itemGrabbed ??= "";
            
            if (!_thoughtObjs.ContainsKey(thought.itemGrabbed))
            {
                _thoughtObjs.Add(thought.itemGrabbed, new List<ThoughtObj>());
            }
            _thoughtObjs[thought.itemGrabbed].Add(thought);
        }
        UpdateThoughtObj();
    }

    public void StartTime()
    {
        _gameStarted = true;
        _correctEndTime = DateTime.Now.AddSeconds(_timeInSec);
        _timeFroze = false;
        time.transform.parent.gameObject.SetActive(true);
        time.gameObject.SetActive(true);
    }

    public void SetAndFrozeTime(double initialTime)
    {
        time.gameObject.SetActive(false);
        _timeFroze = true;
        _timeInSec = initialTime;
    }
    
}
