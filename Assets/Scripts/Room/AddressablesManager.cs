using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.Serialization;
using UnityEngine.Video;

public class AddressablesManager : MonoBehaviour
{
    [Header("Items Mode")]
    public bool addressables = false;
    public Transform parent;
    public List<string> itemsReferenceOrderIds;
    
    [Header("Addressables")] 
    // All of the possible items
    [SerializeField] private List<AssetReference> itemsReference;
    // Is for unload them when destroyed
    private readonly Dictionary<IterativeObj,  AsyncOperationHandle<GameObject>> _asyncOperationHandles =
        new Dictionary<IterativeObj, AsyncOperationHandle<GameObject>>();

    [Header("No Addressables")] 
    [SerializeField] private List<GameObject> itemsObj;
    //[SerializeField] private List<VideoClip> videosReference;
    
    public void SetSpace(string spaceId)
    {
        if (string.IsNullOrEmpty(spaceId))
            spaceId = "Lab";
        
        var obj = new IterativeObj
        {
            id = spaceId
        };
        if (addressables)
            InstantiateWithAddressable(obj);
        else
            InstantiateFromUnityProject(obj);
        
    }

    private void InstantiateWithAddressable(IterativeObj obj)
    {
        AssetReference itemReference = null;

        var index =  itemsReferenceOrderIds.FindIndex(x => x.Equals(obj.id));
        itemReference = itemsReference[index];

        var op = Addressables.LoadAssetAsync<GameObject>(itemReference);
        _asyncOperationHandles.Add(obj,op);
        op.Completed += (go) =>
        {
            var position = obj.initialPosition is {Length: 3}
                ? new Vector3(obj.initialPosition[0], obj.initialPosition[1], obj.initialPosition[2])
                : Vector3.zero;
            var rotation = obj.initialRotation is {Length: 3}
                ? new Vector3(obj.initialRotation[0], obj.initialRotation[1], obj.initialRotation[2])
                : Vector3.zero;
                    
            itemReference.InstantiateAsync(position, Quaternion.Euler(rotation), parent).Completed += (asyncOperationHandle) =>
            {
                var o = asyncOperationHandle.Result.gameObject.GetComponent<Item>();
                if(o != null)
                    o.SetItem(obj);
                /*index =  itemsReferenceOrderIds.FindIndex(x => x.Equals(obj.videoId));
                if (index != -1)
                {
                    var videoReference = itemsReference[index];
                    var videoOp = Addressables.LoadAssetAsync<VideoClip>(videoReference);
                    videoOp.Completed += (clip) =>
                    {
                        o.SetVideo(clip.Result);
                    };
                }*/
                var notify = asyncOperationHandle.Result.AddComponent<NotifyOnDestroy>();
                notify.Destroyed += Remove;
                notify.IterativeObj = obj;
            };
        };
    }

    private void InstantiateFromUnityProject(IterativeObj obj)
    {
        var position = obj.initialPosition is {Length: 3}
            ? new Vector3(obj.initialPosition[0], obj.initialPosition[1], obj.initialPosition[2])
            : Vector3.zero;
        var rotation = obj.initialRotation is {Length: 3}
            ? new Vector3(obj.initialRotation[0], obj.initialRotation[1], obj.initialRotation[2])
            : Vector3.zero;
                
        var index =  itemsReferenceOrderIds.FindIndex(x => x.Equals(obj.id));
        GameObject go = itemsObj[index];
        var o = Instantiate(go, position, Quaternion.Euler(rotation), parent).GetComponent<Item>();
        if(o != null)
            o.SetItem(obj);
        /*index =  videosReference.FindIndex(x => x.name.Equals(obj.videoId));
        if (index != -1)
        {
            o.SetVideo(videosReference[index]);
        }*/
    }

    public void SetItems(List<IterativeObj> interactiveObjs)
    {
        // Use addressables:
        // - Instantiate assets using local hosting or a server to get them
        if (addressables)
        {
            foreach (var obj in interactiveObjs)
            {
                InstantiateWithAddressable(obj);
            }
        }
        // Instantiate assets directly from unity project
        else
        {
            foreach (var obj in interactiveObjs)
            {
                InstantiateFromUnityProject(obj);
            }

        }
    }

    private void Remove(IterativeObj iterativeObj, NotifyOnDestroy obj)
    {
        if (_asyncOperationHandles[iterativeObj].IsValid())
        {
            Addressables.Release(_asyncOperationHandles[iterativeObj]);
        }
        _asyncOperationHandles.Remove(iterativeObj);
    }

    /*public void SetIntroduction(string videoId)
    {
        if (addressables)
        {
            var index =  itemsReferenceOrderIds.FindIndex(x => x.Equals(videoId));
            if (index != -1)
            {
                var videoReference = itemsReference[index];
                var videoOp = Addressables.LoadAssetAsync<VideoClip>(videoReference);
                videoOp.Completed += (clip) =>
                {
                    GetComponent<VideoManager>().PrepareIntroduction(clip.Result);
                };
            }
        }
        else
        {
            var index =  videosReference.FindIndex(x => x.name.Equals(videoId));
            if (index != -1)
            {
                GetComponent<VideoManager>().PrepareIntroduction(videosReference[index]);
            }
        }
    }*/

}

public class NotifyOnDestroy : MonoBehaviour
{
    public event Action<IterativeObj, NotifyOnDestroy> Destroyed;
    public IterativeObj IterativeObj { get; set; }

    public void OnDestroy()
    {
        Destroyed?.Invoke(IterativeObj, this);
    }
}
