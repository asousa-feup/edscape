using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Zoom : MonoBehaviour
{
    [SerializeField] private int steps = 3;
    [SerializeField] private int maxZoom = 10;
    [SerializeField] private int zoomSpeed = 30;

    private CinemachineFreeLook cinemachine;
    private float defaultFOV;
    private float zoomPerStep;
    private float currentZoom;

    // Start is called before the first frame update
    void Start()
    {
        // Get the cinemachine
        cinemachine = FindObjectOfType<CinemachineFreeLook>();
        // Get the default field of view
        defaultFOV = cinemachine.m_Lens.FieldOfView;
        // Calculate the amount of zoom per step
        zoomPerStep = (defaultFOV - maxZoom) / steps;
        // Set current zoom
        currentZoom = defaultFOV;
    }

    // Update is called once per frame
    void Update()
    {   
        // Check if the mouse wheel is going foward
        if (Input.GetAxis("Mouse ScrollWheel") > 0f ){
            ZoomIn();
        }
        // Check if the mouse wheel is going backwards
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f ){
            ZoomOut();
        }

        SmoothZoomAction();
    }

    // Decrease camera FOV to simulate zoom in
    void ZoomIn(){
        // Check if the minimum zoom has been reached
        if(currentZoom <= maxZoom){
            currentZoom = maxZoom;
            return;
        }

        // Update current zoom
        currentZoom -= zoomPerStep;
    }

    // Increase camera FOV to simulate zoom out
    void ZoomOut(){
        // Check if the default zoom has been reached
        if(currentZoom >= defaultFOV){
            currentZoom = defaultFOV;
            return;
        }

        // Update current zoom
        currentZoom += zoomPerStep;
    }

    // Change camera zoom over time smoothly
    void SmoothZoomAction(){
        cinemachine.m_Lens.FieldOfView = Mathf.MoveTowards(cinemachine.m_Lens.FieldOfView, currentZoom, zoomSpeed * Time.deltaTime);
    }
}
