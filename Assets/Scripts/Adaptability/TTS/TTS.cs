using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Web;

// Class for the text to speech functionality
public class TTS : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;

    // Dictionary with the available languages
    private Dictionary<string, string> languages = new Dictionary<string, string>() { 
        { "English", "en"}, { "French", "fr"}, { "German", "de"} , { "Italian", "it"}, { "Portuguese", "pt"}, { "Spanish", "es"}
    };

    // Converts the text to speech in a specified language
    public void Play(string text, string language){
        if(string.IsNullOrEmpty(text) || string.IsNullOrEmpty(language)){
            return;
        }

        // Call the google translate tts api
        StartCoroutine(GoogleTranslateTTSAPI(text, language));
    }

    // Converts the text to speech in a specified language using the google translate tts api
    IEnumerator GoogleTranslateTTSAPI(string text, string language) {
        // Build google translate tts api request
        string url = $"https://translate.google.com/translate_tts?ie=UTF-8&q={HttpUtility.UrlEncode(text)}&tl={languages[language]}&client=tw-ob";

        // Create the request
        using (UnityWebRequest audio = UnityWebRequestMultimedia.GetAudioClip(url, AudioType.MPEG)) {

            // Send the request
            yield return audio.SendWebRequest();

            // Check if a connection error has occurred
            if (audio.error != null) {
                Debug.Log(audio.error);
            }else { // Otherwise
                // Create audio clip from the downloaded audio content
                AudioClip clip = DownloadHandlerAudioClip.GetContent(audio);

                // Check if the audioSource is not null
                if(audioSource != null){
                    // Set and play the audio clip
                    audioSource.Stop();
                    audioSource.clip = clip;
                    audioSource.Play();
                }else{
                    Debug.Log("Audio Source is null!");
                }
            }
        }
    }
}
