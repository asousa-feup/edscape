using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IEER
{
    private Settings settings;
    private GameSettings gameSettings;
    private ConfigGameObj configGameObj;
    private int indexOfGameSettings = -1;

    // Constructor for IEER creation
    public IEER(Settings settings){
        this.settings = settings;
        this.gameSettings = new GameSettings();
        this.configGameObj = new ConfigGameObj();
    }

    // Constructor for IEER deletion
    public IEER(Settings settings, GameSettings gameSettings){
        this.settings = settings;
        this.gameSettings = gameSettings;
    }

    // Constructor for IEER visualization
    public IEER(GameSettings gameSettings, ConfigGameObj configGameObj){
        this.gameSettings = gameSettings;
        this.configGameObj = configGameObj;
    }

    // Constructor for IEER edition
    public IEER(Settings settings, int indexOfGameSettings){
        this.settings = settings;
        this.gameSettings =  new GameSettings();
        this.configGameObj = new ConfigGameObj();
        this.indexOfGameSettings = indexOfGameSettings;
    }

    // Creates a new IEER with the information from the form
    public IEnumerator Create(Dictionary < string, FormField > formFields){
        yield return PopulateGameInfoClasses(formFields);
        GenerateGameInfo();

        // Wait for all the translatable field to be updated
        yield return configGameObj.WaitForTranslation();

        CreateSettingsGame();
        SaveAllToJson();
    }

    // Displays the IEER information
    public IEnumerator Display(Dictionary < string, FormField > formFields){
        // Gets all the diferent class Types
        Type gameSettingsType = typeof(GameSettings);
        Type configGameObjType = typeof(ConfigGameObj);

        // Assing gameSettings and configGameObj values to formFields
        // Loops through all form fields
        foreach (var formField in formFields)
        {
            // Search for a field in the GameSettings class with the same name as form field key
            FieldInfo gameSettingsField = gameSettingsType.GetField(formField.Key);
            
            // if was found
            if (gameSettingsField != null){
                formField.Value.View(gameSettingsField.GetValue(gameSettings));
                // Skips to the next frame. This way does not wait for the loop to complete.
                yield return null;
                continue;
            }

            // Search for a field in the configGameObj class with the same name as form field key
            FieldInfo configGameObjField = configGameObjType.GetField(formField.Key);

            // if was found
            if (configGameObjField != null){
                formField.Value.View(configGameObjField.GetValue(configGameObj));
                // Skips to the next frame. This way does not wait for the loop to complete.
                yield return null;
                continue;
            }
        }

        bool areAllFieldsSet = false;

        // Wait for all fields to be set
        while (!areAllFieldsSet){
            areAllFieldsSet = true;
            // Loops through all form fields
            foreach (var formField in formFields){
                // Check if there is one field that is not set
                if (!formField.Value.valueIsSet){
                    areAllFieldsSet = false;
                }
                // Skips to the next frame.
                yield return null;
            }
        }
    }

    // Edits the IEER information
    public IEnumerator Edit(Dictionary < string, FormField > formFields){
        yield return PopulateGameInfoClasses(formFields);
        GenerateGameInfo();

        // Wait for all the translatable field to be updated
        yield return configGameObj.WaitForTranslation();

        SaveAllToJson();
    }

    // Deletes the IEER
    public void Delete(){
        string path = Application.streamingAssetsPath + "/" + gameSettings.name;

        // Checks if the file exists
        if (System.IO.Directory.Exists(path)){
            // Delete the folder containing the IEER information
            System.IO.Directory.Delete(path, true);
            // Deletes the meta file
            System.IO.File.Delete(path + ".meta");
        }

        // Update Settings
        DeleteSettingsGame();
        // Rewrite settings file
        SaveSettings();
    }

    // Loops through all the form fields and saves the information in the two game information classes
    private IEnumerator PopulateGameInfoClasses(Dictionary < string, FormField > formFields) {
        // Gets all the diferent class Types
        Type gameSettingsType = typeof(GameSettings);
        Type configGameObjType = typeof(ConfigGameObj);

        // Assing form values to gameSettings and configGameObj
        // Loops through all form fields
        foreach (var formField in formFields)
        {
            // Search for a field in the GameSettings class with the same name as form field key
            FieldInfo gameSettingsField = gameSettingsType.GetField(formField.Key);
            
            // if was found
            if (gameSettingsField != null){
                gameSettingsField.SetValue(gameSettings, formField.Value.GetValue());
                // Skips to the next frame. This way does not wait for the loop to complete.
                yield return null;
                continue;
            }

            // Search for a field in the configGameObj class with the same name as form field key
            FieldInfo configGameObjField = configGameObjType.GetField(formField.Key);

            // if was found
            if (configGameObjField != null){
                configGameObjField.SetValue(configGameObj, formField.Value.GetValue());
                // Skips to the next frame. This way does not wait for the loop to complete.
                yield return null;
                continue;
            }
        }
    }

    private void GenerateGameInfo(){
        // Checks if the IEER name is empty or null
        if (string.IsNullOrEmpty(gameSettings.name)){
            gameSettings.name = "None";
        }

        int counter = 1;
        string gameName = gameSettings.name;

        // Generates a new game name if already exists one with the same name
        while(ExistsGameInSettings(gameName)){
            gameName = string.Format("{0}({1})", gameSettings.name, counter++);
        }

        // Update game name
        gameSettings.name = gameName;
        
        DateTime dateTime = DateTime.Now;
        // Generate game id
        configGameObj.gameId = gameSettings.name + dateTime.ToString("dd_MM_yyyy_HH_mm_ss");
        // Assign issuer email
        configGameObj.issuerEmail = PlayerPrefs.GetString("userEmail");
        // Generate creation date
        configGameObj.creationDate = dateTime.ToString("dd/MM/yyyy HH:mm:ss");
        // Generate configGameObj path
        gameSettings.path = gameSettings.name + "/" + configGameObj.gameId + ".json";
    }

    private bool ExistsGameInSettings(string name){
        // Loops through all the gameSettings
        foreach(GameSettings tempGameSettings in settings.games){
            // if already exists one gameSettings with the same name
            if ( (tempGameSettings.name == name) && (settings.games.IndexOf(tempGameSettings) != indexOfGameSettings) ){
                return true;
            }
        }
        return false;
    }

    // Adds a new game the settings class information
    private void CreateSettingsGame(){
        // Adds gameSettings to settings
        settings.games.Add(gameSettings);
    }

    // Deletes an existing game in the settings class information
    private void DeleteSettingsGame(){
        // Remove game from the settings
        settings.games.Remove(gameSettings);
    }

    // Saves the settings and configGameObj in JSON files
    private void SaveAllToJson(){
        SaveGameInformation();
        SaveSettings();
    }

    private void SaveSettings(){
        // Convert settings information to json
        string settingsData = JsonUtility.ToJson(settings);
        // Save settings information
        System.IO.File.WriteAllText(Application.streamingAssetsPath + "/settings.json", settingsData);
    }

    private void SaveGameInformation(){
        // int counter = 1;
        // string tempFolder = gameSettings.name;

        // // Generates a temporary directory to store the new information if already one with the same name exists
        // while(System.IO.Directory.Exists(Application.streamingAssetsPath + "/" + tempFolder)){
        //     tempFolder = string.Format("{0}({1})", gameSettings.name, counter++);
        // }

        // Create the directory where the files will be saved
        // System.IO.Directory.CreateDirectory(Application.streamingAssetsPath + "/" + tempFolder);
        System.IO.Directory.CreateDirectory(Application.streamingAssetsPath + "/" + gameSettings.name);
        
        // Prepare configGameObj to be saved, moves copies all the uploaded files to the temporary folder
        // yield return configGameObj.PrepareToSave(tempFolder, gameSettings.name);

        // Checks if is indexOfGameSettings was assigned
        // If assinged is editing
        if (indexOfGameSettings > -1){
            DeleteOldInformation();
        }

        // If the temporary folder was created
        // if (counter > 1){
        //     // Rename the folder to the pretended name
        //     System.IO.Directory.Move(Application.streamingAssetsPath + "/" + tempFolder, Application.streamingAssetsPath + "/" + gameSettings.name);
        // }

        // Convert configGameObj information to json
        string configGameObjData = JsonUtility.ToJson(configGameObj);
        // Save configGameObj information
        System.IO.File.WriteAllText(Application.streamingAssetsPath + "/" + gameSettings.path, configGameObjData);
    }

    private void DeleteOldInformation(){
        // string path = Application.streamingAssetsPath + "/" + settings.games[indexOfGameSettings].name;

        // Deletes the old configGameObj
        System.IO.File.Delete(Application.streamingAssetsPath + "/" + settings.games[indexOfGameSettings].path);
        // Deletes the old configGameObj meta
        System.IO.File.Delete(Application.streamingAssetsPath + "/" + settings.games[indexOfGameSettings].path + ".meta");

        // Checks if the directory exists 
        // if (System.IO.Directory.Exists(path)){
        //     // Delete the folder containing the IEER information
        //     System.IO.Directory.Delete(path, true);
        //     // Deletes the meta file
        //     System.IO.File.Delete(path + ".meta");
        // }

        // Overwrite name and test values
        settings.games[indexOfGameSettings] = gameSettings;
    }
}
