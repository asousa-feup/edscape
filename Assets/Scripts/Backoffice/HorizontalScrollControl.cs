using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
Class that will control the horizontal scroll.
Horizontal scroll can be changed using two buttons (one for each direction).
Displays a progress bar.
*/
public class HorizontalScrollControl : MonoBehaviour
{
    [SerializeField] private GameObject horizontalScrollBar;
    [SerializeField] private GameObject progressBar;
    [SerializeField] private Button previousButton;
    [SerializeField] private Button nextButton;

    private Scrollbar scrollBar;
    private GameObject progressGameObject;
    private GameObject maxProgressGameObject;

    private double maxProgress;
    private float numberOfPages;
    private int currentPage = 1;

    // Start is called before the first frame update
    void Start()
    {
        // Verifies if horizontalScrollBar and progressBar were assigned
        if (horizontalScrollBar != null && progressBar != null){

            // Gets scrollBar to crontol the horizontal scroll
            // Gets the gameObjects necessary for the progress bar
            scrollBar = horizontalScrollBar.GetComponent<Scrollbar>();
            progressGameObject = progressBar.transform.Find("Progress").gameObject;
            maxProgressGameObject = progressBar.transform.Find("MaxProgress").gameObject;

            // Verifies if all variables were assigned
            if (scrollBar != null && progressGameObject != null && maxProgressGameObject != null)
                StartCoroutine(Setup());
        }
       
    }

    // Sets up all the necessary variables 
    IEnumerator Setup(){
        // Start counter
        int counter = 0;
        double initialScrollSize = scrollBar.size;
        
        // Verification of scroll size
        while(true){
            // if the scroll bar does not change its value for 5 iterations break
            // this means that the value of the scroll bar size is correct
            if ( (initialScrollSize != scrollBar.size) || (counter == 5) ){
                break;
            }
            
            // Update counter
            counter++;
            // Skip to the next frame
            yield return null;
        }

        // Gets the maximum progress possible
        maxProgress = maxProgressGameObject.GetComponent<RectTransform>().sizeDelta.x;
        // Gets the number of pages available to scroll
        numberOfPages = 1 / scrollBar.size;
        // Sets the number of steps of the scroll bar
        scrollBar.numberOfSteps = (int)Math.Round(numberOfPages);
        CalculateAndSetCurrentProgress();
        DeactivateOrActivateButtons();

    }

    // Calculates and sets the current progress
    void CalculateAndSetCurrentProgress(){

        // calcutates the current progress
        // progress goes from -maxProgress to 0, where 0 equals to full progress bar
        float currentProgress = (float)(maxProgress - ((1 / numberOfPages) * currentPage * maxProgress));
        // sets current progress
        // is -currentProgress because we are changing rectangle right value
        progressGameObject.GetComponent<RectTransform>().offsetMax =  new Vector2(-currentProgress, progressGameObject.GetComponent<RectTransform>().offsetMax.y);
        
    }

    // Deactivates or activates the previous and next buttons.
    void DeactivateOrActivateButtons(){
        
        // Checks if both buttons were assigned
        if (previousButton != null && nextButton != null){

            // if the current page is the first, deactivate previousButton
            if (FirstPage())
                previousButton.interactable = false;
            // otherwise, activate previousButton
            else
                previousButton.interactable = true;

            // if the current page is the last, deactivate nextButton
            if (LastPage())
                nextButton.interactable = false;
             // otherwise, activate nextButton
            else
                nextButton.interactable = true;

        }

    }

    // Checks if the current page is the first
    bool FirstPage(){
        return currentPage == 1;
    }

    // Checks if the current page is the last
    bool LastPage(){
        return (int)currentPage == (int)Math.Round(numberOfPages);
    }

    // Changes to the next page
    public void NextPage(){

        // updates scroll bar value
        // need to add half of the scroll bar size because we are using steps functionality
        // otherwise it would not work properly 
        scrollBar.value = (currentPage * scrollBar.size) + (scrollBar.size / 2);
        // updates current page
        currentPage++;
        CalculateAndSetCurrentProgress();
        DeactivateOrActivateButtons();

    } 

    // Changes to the previous page
    public void PreviousPage(){
        
        // updates current page
        currentPage--;
        
        // updates scroll bar value
        if (FirstPage())
            scrollBar.value = 0;
        else
            // need to add half of the scroll bar size because we are using steps functionality
            // otherwise it would not work properly 
            scrollBar.value = (currentPage * scrollBar.size) - (scrollBar.size / 2);

        CalculateAndSetCurrentProgress();
        DeactivateOrActivateButtons();

    }

    // Resets the horizontal scroll bar
    public void Reset(){
        currentPage = 1;
        scrollBar.value = 0;
        CalculateAndSetCurrentProgress();
        DeactivateOrActivateButtons();
    }
}
