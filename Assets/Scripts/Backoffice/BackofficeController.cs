using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackofficeController : MonoBehaviour
{
    [SerializeField] private GameObject [] createGameObjects;
    [SerializeField] private GameObject [] viewGameObjects;
    [SerializeField] private GameObject [] editGameObjects;
    [SerializeField] private GameObject loadPanel;
    [SerializeField] private GameObject settingsPanel;
    
    private Dictionary < string, FormField > formFields = new Dictionary < string, FormField > ();
    private SettingsController settingsController;
    private HorizontalScrollControl horizontalScrollControl;
    private VerticalScrollControl verticalScrollControl;

    // Start is called before the first frame update
    void Start()
    {
        // Gets all the form fields
        GameObject [] formFieldsGameObject = GameObject.FindGameObjectsWithTag("FormField");
        foreach (GameObject formFieldGameObject in formFieldsGameObject){
            FormField formFieldScript = formFieldGameObject.GetComponent<FormField>();
            formFields[formFieldScript.GetFieldId()] = formFieldScript;
        }

        // Gets the settings controller script
        settingsController = GameObject.Find("SettingsController").GetComponent<SettingsController>();
        // Gets the horizontal scroll controller script
        horizontalScrollControl = transform.Find("Body").GetComponent<HorizontalScrollControl>();
        // Gets the vertical scroll controller script
        verticalScrollControl = transform.Find("Body/HorizontalScroll/Viewport/Content").GetComponent<VerticalScrollControl>();
    }

    //----------------------------- Create -------------------------------
    // Function that is called when the add buttons is pressed
    // Enables the creation of a new IEER
    public void Create(){
        // Resets the horizontal and vertical scroll bar
        horizontalScrollControl.Reset();
        verticalScrollControl.Reset();

        // Loops through the form fields and executes the create function
        foreach (var formField in formFields)
            formField.Value.Create();

        EnableCreate();
        DisableView();
        DisableEdit();
    }
    
    // Creates the IEER
    public void CreateAction(){
        StartCoroutine(CreateCoroutine());
    }

    // Execute IEER creation with a coroutine
    IEnumerator CreateCoroutine(){
        loadPanel.SetActive(true);
        settingsPanel.SetActive(true);

        // Creates the IEER
        IEER ieer = new IEER(settingsController.settings);
        yield return ieer.Create(formFields);

        loadPanel.SetActive(false);
        settingsController.LoadGames();
    }

    // Enables the create game objects
    void EnableCreate(){
        foreach(GameObject gameObject in createGameObjects){
            gameObject.SetActive(true);
        }
    }
    
    // Disables the create game objects
    void DisableCreate(){
        foreach(GameObject gameObject in createGameObjects){
            gameObject.SetActive(false);
        }
    }

    //----------------------------- View -------------------------------
    // Function that is called when the user clicks in one of the games
    // Enables the visualization of the IEER's content
    public void View(){
        // Turn on loading panel
        loadPanel.SetActive(true);

        // Resets the horizontal and vertical scroll bar
        horizontalScrollControl.Reset();
        verticalScrollControl.Reset();

        EnableView();
        DisableCreate();
        DisableEdit();

        StartCoroutine(ViewCoroutine());
    }

    // Execute IEER visualization with a coroutine
    IEnumerator ViewCoroutine(){
        // Displays IEER information
        IEER ieer = new IEER(settingsController.selectedGame, settingsController.selectedGameInformation);
        yield return ieer.Display(formFields);
        
        // Turn off loading panel
        loadPanel.SetActive(false);
    }


    // Enables the view game objects
    void EnableView(){
        foreach(GameObject gameObject in viewGameObjects){
            gameObject.SetActive(true);
        }
    }

    // Disables the view game objects
    void DisableView(){
        foreach(GameObject gameObject in viewGameObjects){
            gameObject.SetActive(false);
        }
    }

    //----------------------------- Edit -------------------------------
    // Function that is called when the user clicks in the edit button
    // Enables the edition of the IEER's content
    public void Edit(){
        // Loops through the form fields and executes the edit function
        foreach (var formField in formFields)
            formField.Value.Edit();

        EnableEdit();
        DisableCreate();
        DisableView();
    }

    // Edits the IEER content
    public void EditAction(){
        StartCoroutine(EditCoroutine());
    }

    // Execute IEER edition with a coroutine
    IEnumerator EditCoroutine(){
        loadPanel.SetActive(true);
        settingsPanel.SetActive(true);

        // Edits IEER information
        IEER ieer = new IEER(settingsController.settings, settingsController.settings.games.IndexOf(settingsController.selectedGame));
        yield return ieer.Edit(formFields);

        loadPanel.SetActive(false);
        settingsController.LoadGames();
    }

    // Enables the edit game objects
    void EnableEdit(){
        
        foreach(GameObject gameObject in editGameObjects){
            gameObject.SetActive(true);
        }
    }

    // Disables the edit game objects
    void DisableEdit(){
        foreach(GameObject gameObject in editGameObjects){
            gameObject.SetActive(false);
        }
    }

    //----------------------------- Delete -------------------------------
    // Deletes the IEER
    public void DeleteAction(){
        loadPanel.SetActive(true);
        settingsPanel.SetActive(true);

        // Deletes the IEER
        IEER ieer = new IEER(settingsController.settings, settingsController.selectedGame);
        ieer.Delete();

        loadPanel.SetActive(false);
        settingsController.LoadGames();
    }

    //----------------------------- Play -------------------------------
    // Plays the IEER
    public void PlayAction(){
        settingsPanel.SetActive(true);
        settingsController.Play();
    }
}
