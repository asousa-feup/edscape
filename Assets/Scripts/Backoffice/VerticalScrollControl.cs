using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VerticalScrollControl : MonoBehaviour
{
    private Scrollbar[] verticalScrollBars;

    // Start is called before the first frame update
    void Start()
    {
        verticalScrollBars = new Scrollbar[transform.childCount];

        // Gets all the vertical scroll bars
        for(int i = 0; i < transform.childCount; i++){
            verticalScrollBars[i] = transform.GetChild(i).Find("Scrollbar Vertical").GetComponent<Scrollbar>();
        }
    }

     // Resets all the vertical scroll bars
    public void Reset(){
        foreach (Scrollbar scrollBar in verticalScrollBars){
            scrollBar.value = 1;
        }
    }
}
