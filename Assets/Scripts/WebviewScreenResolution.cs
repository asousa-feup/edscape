using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebviewScreenResolution : MonoBehaviour
{
    [SerializeField] private SimpleWebBrowser.WebBrowser2D browser2D;

    // Awake is called before the start
    void Awake()
    {
        // Set the webview screen resolution to the current screen width and height
        browser2D.Width =(int)Math.Round(Screen.width * 0.83);
        browser2D.Height = (int)Math.Round(Screen.height * 0.86);
    }
}
