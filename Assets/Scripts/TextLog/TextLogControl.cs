using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class TextLogControl : MonoBehaviour
{
    [SerializeField] private GameObject textTemplate;
    private List<GameObject> textItems;
    private string _filePath;
    private LogInfoForm _log = new LogInfoForm();
    private GameRoomController _gameRoomController;
    public Animator alertAnimator;

    private Scrollbar _scrollBarClues;

    private bool _middle = false;
    private bool _withLogId = false;
    
    private TTS tts_player;
    
    private string url = "https://web.fe.up.pt/~efeupinho/web_badges/unity/unityLogAndBadges.php";

    private void Awake()
    {
        _gameRoomController = FindObjectOfType<GameRoomController>();
        _log.frameworkVersion = Application.version;
        _scrollBarClues = _gameRoomController.GetScrollBarClue();
        var date = DateTime.Now;
        var tmpDate = date.Year + "/" + 
                      (date.Month < 10 ? "0" + date.Month : date.Month.ToString()) + "/" +
                      (date.Day < 10 ? "0" + date.Day : date.Day.ToString());
        _log.date = tmpDate;
        // _log.date = date.Substring(0, 10);
        _log.initialTime = (date.Hour < 10 ? "0" + date.Hour : date.Hour.ToString()) + ":" +
                           (date.Minute < 10 ? "0" + date.Minute : date.Minute.ToString()) + ":" +
                           (date.Second < 10 ? "0" + date.Second : date.Second.ToString());
        //_log.initialTime = date.ToString().Substring(11, 8); 
        
        tts_player = FindObjectOfType<TTS>();
    }

    void Start()
    {
        textItems = new List<GameObject>();
    }

    public void LogText(string newTextString, Color? color)
    {
        if (!color.Equals(TextLogItem.Type.OnlyToBd))
        {
            if (textItems.Count == 100)
            {
                GameObject tempItem = textItems[0];
                Destroy(tempItem.gameObject);
                textItems.Remove(tempItem);
            }

            GameObject newText = Instantiate(textTemplate, textTemplate.transform.parent, false) as GameObject;
            newText.SetActive(true);

            // Play TTS
            tts_player.Play(newTextString, _gameRoomController._langOp);
            newText.GetComponent<TextLogItem>().SetText(newTextString, color);
            
            textItems.Add(newText.gameObject);
            alertAnimator.SetTrigger("triggerOn");
            if(_gameRoomController.IsInMainGame())
                _gameRoomController.PlayNotification();
            // resets scroll bar
            _scrollBarClues.value = 0;
            
            return;
        }
        
        _log.log += "[" + DateTime.Now + "] " + newTextString + "\n";

        if (_gameRoomController == null || _gameRoomController.GetGameInfo() == null)
            return;

        if(_withLogId || !_middle ) // Accumulate until receiving logID
            SendLog(false);
    }

    public void SendLog(bool final)
    {
        WWWForm form = new WWWForm();

        // In the end to make sure the global json was already parsed
        var gameObj =  _gameRoomController.GetGameInfo();
        string post;
        
        if (!_middle) // first
        {
            Debug.Log(".............first");
            post = "first";
            form.AddField("first", "true"); 
            _log.gameId = gameObj.gameId;
            _log.issuerUserEmail = gameObj.issuerEmail;
            _log.studentUserEmail = gameObj.userEmail;
            form.AddField("user", _log.studentUserEmail); 
            form.AddField("issuers",_log.issuerUserEmail ); 
            form.AddField("log", _log.log);
            form.AddField("date", _log.date);
            form.AddField("initial_time",_log.initialTime);
            form.AddField("game_version", _log.gameId);
            form.AddField("framework_version", _log.frameworkVersion);
            _middle = true;
            //Debug.Log("form: " + JsonUtility.ToJson(_log));

        }
        else if (!final) // middle
        {
            Debug.Log(".............middle, log_id:" + _log.id + ", log:\n" + _log.log);
            form.AddField("middle", "true");
            post = "middle";
            form.AddField("log_id", _log.id);
            form.AddField("log", _log.log);
        }
        else // last
        {
            Debug.Log(".............last");
            _log.log += "[" + DateTime.Now + "] [Game Ended] Finished game in time? " + _log.isFinishedInTime + "\n";
            _log.log += "[" + DateTime.Now + "] [Game Ended] Used hints? " + _log.withoutHint + "\n";
            
            post = "last";
            form.AddField("last", "true");
            var date = DateTime.Now;
            _log.finalTime = (date.Hour < 10 ? "0" + date.Hour : date.Hour.ToString()) + ":" +
                             (date.Minute < 10 ? "0" + date.Minute : date.Minute.ToString()) + ":" +
                             (date.Second < 10 ? "0" + date.Second : date.Second.ToString());
            _log.isFinishedInTime = !(_gameRoomController.GetRoomTime() <= 0);
            _log.withoutHint = _gameRoomController.GetHintsCount() <= 0;
            form.AddField("final_time", _log.finalTime);
            form.AddField("log_id", _log.id);
            form.AddField("log", _log.log);
            form.AddField("date", _log.date);
            form.AddField("game_ver", _log.gameId);
            form.AddField("user", _log.studentUserEmail);
            form.AddField("isFinishedInTime", _log.isFinishedInTime ? "true":"false");
            form.AddField("withoutHint", _log.withoutHint ? "true":"false");
            
            //Debug.Log("form: " + JsonUtility.ToJson(_log));
            
        }

        StartCoroutine(Send(form, post));
    }

    private IEnumerator Send(WWWForm form, string post)
    {
        //Debug.Log("form: " + JsonUtility.ToJson(_log));
        //Debug.Log("log: " + _log.log);
        
        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        { 
            www.downloadHandler = new DownloadHandlerBuffer();
            
            //Debug.Log("result: " + www.result);
            //Debug.Log("uri: " + www.uri);
            
            yield return www.SendWebRequest();

            if (!string.IsNullOrEmpty(www.error)) 
            {
                Debug.Log(www.error); 
            }else 
            {
                
                Debug.Log("handler: " + www.downloadHandler.text);
                
                if (post.Equals("first"))
                {
                    _log.id = www.downloadHandler.text.Split(char.Parse("|"))[2];
                    _withLogId = true;
                    Debug.Log("log id: " + _log.id);
                }
                else if (post.Equals("last"))
                {
                    Debug.Log("last log: \n" + _log.log);
                    
                    // badges
                    _gameRoomController.SetActiveEndPanel(www.downloadHandler.text.Split(char.Parse("|"))[0]);
                }

            }
        }
        
        // Create log file path
        /*var json = JsonUtility.ToJson(_log);
        _filePath = Application.streamingAssetsPath + "/" + DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + "_" +
                    DateTime.Now.Hour +  "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second + ".json";
        byte[] bytes = Encoding.ASCII.GetBytes(json);
        File.WriteAllBytes(_filePath, bytes);*/
    }

    public void SendTimeEndedMessage()
    {
        var text = _gameRoomController.FindTextTimeEndText();
        if (!string.IsNullOrEmpty(text))
        {
            LogText("[Time] Time ended, message: " + text + ".", TextLogItem.Type.OnlyToBd);
            LogText(text, TextLogItem.Type.Penalty);
        }else
            LogText("[Time] Time ended.", TextLogItem.Type.OnlyToBd);
    }
}


[Serializable] public class LogInfoForm
{
    public string date;
    public string initialTime;
    public string finalTime;
    public string gameId;
    public string frameworkVersion;
    public string log;
    public string id;
    public string studentUserEmail;
    public string issuerUserEmail;
    public bool isFinishedInTime;
    public bool withoutHint;
}
