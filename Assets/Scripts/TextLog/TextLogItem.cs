using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextLogItem : MonoBehaviour
{
    public static class Type
    {
        public static readonly Color Default = Color.white;
        public static readonly Color Penalty = Color.red;
        public static readonly Color Clue = Color.yellow;
        public static readonly Color Thought = new Color32(0x49, 0xA0, 0x78, 0xFF);
        public static readonly Color OnlyToBd = Color.black;
        
    }
    
    public void SetText(string myText, Color? color)
    {
        GetComponent<Text>().color = (Color) (color ?? Type.Default);
        GetComponent<Text>().text = myText;
    }
}
