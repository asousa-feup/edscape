using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;

public class RegisterControl : MonoBehaviour
{
    public GameObject loginPanel;
    public GameObject registerPanel;
    public TextMeshProUGUI registetErrorText;
    public TMP_InputField codeInput;

    private string email;
    private string password;
    private string code;

    private string url = "https://web.fe.up.pt/~efeupinho/web_badges/unity/unityUserData.php";

    private void Start()
    {
        registetErrorText.enabled = false;
        codeInput.interactable = false;
    }

    public void ReadEmail(string s)
    {
        email = s;
    }

    public void ReadPassword(string s)
    {
        password = s;
    }

    public void ReadCode(string s)
    {
        code = s;
    }

    public void RegisterButton()
    {
        if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(code))
        {
            registetErrorText.enabled = true;
            // registetErrorText.text = "*Erro: Preencha todos os campos.";
            registetErrorText.text = "*Error: Fill in all fields.";
        }
        else
        {
            StartCoroutine(SignUp(email, password,code));
        }
    }

    IEnumerator SignUp(string email, string password, string code)
    {
        WWWForm form = new WWWForm();
        form.AddField("signup", "submit");
        form.AddField("email", email);
        form.AddField("password", password);
        form.AddField("code", code);
        Debug.Log("code:"+code);
        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();

            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.Log(www.error);
            }
            else
            {
                string handler = www.downloadHandler.text;
                string[] aux = handler.Split('|');
                if (aux[1].Equals("green"))
                {
                    registetErrorText.color = Color.green;
                    //GoToLoginPanel();
                }
                else
                {
                    registetErrorText.color = Color.red;
                }
                registetErrorText.enabled = true;
                registetErrorText.text = aux[2];
                Debug.Log(handler);
            }
        }
    }
    public void SendCodeButton()
    {
       
        if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
        {
            registetErrorText.color = Color.red;
            registetErrorText.enabled = true;
            registetErrorText.text = "*Erro: Fill in the email and password fields.";
        }else{
            codeInput.interactable = true;
            StartCoroutine(SendCode(email, password));
        }
    }

    IEnumerator SendCode(string email, string password)
    {
        Debug.Log("email:" + email +".");
        WWWForm form = new WWWForm();
        form.AddField("check", "submit");
        form.AddField("email", email);
        form.AddField("password", password);

        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            Debug.Log("result:" + www.result);
            Debug.Log("uri:"+www.uri);
            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();

            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.Log(www.error);
            }
            else
            {
                string handler = www.downloadHandler.text;
                string[] aux = handler.Split('|');
                //Debug.Log("<"+aux[0]+"> , " + aux[0].Length);
                if(aux[1].Equals("green")){
                    registetErrorText.color = Color.green;
                }
                else
                {
                    registetErrorText.color = Color.red;
                }
                registetErrorText.enabled = true;
                registetErrorText.text = aux[2];
                Debug.Log(handler);
            }
        }
    }


    public void GoToLoginPanel()
    {
        loginPanel.SetActive(true);
        registerPanel.SetActive(false);
    }
}
