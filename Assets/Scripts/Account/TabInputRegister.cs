using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TabInputRegister : MonoBehaviour
{
    public TMP_InputField emailInput; //0
    public TMP_InputField passwordInput; //1
    public TMP_InputField codeInput; //2

    public int InputSelected;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            InputSelected++;
            if (InputSelected > 1 & !codeInput.interactable) InputSelected = 0;
            if (InputSelected > 2 & codeInput.interactable) InputSelected = 0;
            SelectedInputFied();
        }
    }

    void SelectedInputFied()
    {
        switch (InputSelected)
        {
            case 0:
                emailInput.Select();
                break;
            case 1:
                passwordInput.Select();
                break;
            case 2:
                codeInput.Select();
                break;
        }
    }

    public void emailSelected() => InputSelected = 0;
    public void passwordSelected() => InputSelected = 1;
    public void codeSelected() => InputSelected = 2;


}
