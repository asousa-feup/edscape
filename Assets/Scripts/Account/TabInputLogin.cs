using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TabInputLogin : MonoBehaviour
{
    public TMP_InputField emailInput; //0
    public TMP_InputField passwordInput; //1

    public int InputSelected;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            InputSelected++;
            if (InputSelected > 1) InputSelected = 0;
            SelectedInputFied();
        }
    }

    void SelectedInputFied()
    {
        switch (InputSelected)
        {
            case 0: emailInput.Select();
                break;
            case 1: passwordInput.Select();
                break;
        }
    }

    public void emailSelected() => InputSelected = 0;
    public void passwordSelected() => InputSelected = 1;
}
