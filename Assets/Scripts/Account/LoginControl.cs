using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class LoginControl : MonoBehaviour
{
    public TextMeshProUGUI loginErrorText;
    public GameObject loginPanel;
    public GameObject registerPanel;

    private string email;
    private string password;
    private bool loginSuccess;

    private string urlResetPassword = "https://web.fe.up.pt/~efeupinho/web_badges/pages/formForgotPassword.php";
    private string urlNewPassword = "https://web.fe.up.pt/~efeupinho/web_badges/pages/formNewPassword.php?email=";
    private string urlLogin = "https://web.fe.up.pt/~efeupinho/web_badges/unity/unityUserData.php";
    
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetString("userEmail", "");
        loginPanel.SetActive(true);
        registerPanel.SetActive(false);
        loginErrorText.enabled = false;
    }

    
    public void ReadEmail(string s)
    {
        email= s;
    }

    public void ReadPassword(string s)
    {
        password = s;
    }



    public void LoginButton()
    {
        if(string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
        {
            loginErrorText.enabled = true;
            loginErrorText.text = "*Erro: Fill in all fields.";
        }
        else
        {
            StartCoroutine(Login(email,password));
        }
    }

    IEnumerator Login(string email, string password)
    {
        WWWForm form = new WWWForm();
        form.AddField("login", "submit");
        form.AddField("email", email);
        form.AddField("password", password);

        using(UnityWebRequest www = UnityWebRequest.Post(urlLogin, form))
        {
            Debug.Log("result:" + www.result);
            Debug.Log("uri:" + www.uri);

            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();

            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.Log("error: " +www.error);
            }
            else
            {
                string handler = www.downloadHandler.text;
                string[] aux = handler.Split('|');
                if (aux[1].Equals("green"))
                {
                    loginErrorText.color = Color.green;
                    loginErrorText.enabled = true;
                    loginErrorText.text = aux[2];
                }
                else if (aux[1].Equals("red"))
                {
                    loginErrorText.color = Color.red;
                    loginErrorText.enabled = true;
                    loginErrorText.text = aux[2];
                }
                else if (aux[1].Equals("success"))
                {
                    loginErrorText.color = Color.red;
                    loginErrorText.enabled = true;
                    loginErrorText.text = aux[2];
                    PlayerPrefs.SetString("userEmail", email);
                    SceneManager.LoadScene("SettingsScene");
                }
                else if (aux[1].Equals("red-old"))
                {
                    loginErrorText.color = Color.red;
                    loginErrorText.enabled = true;
                    loginErrorText.text = aux[2];
                    Application.OpenURL(urlNewPassword+email);
                }
                Debug.Log(handler);
            }
        }
    }

    public void ResetPassword()
    {
        Application.OpenURL(urlResetPassword);
    }

    public void GoToRegisterPanel()
    {
        loginPanel.SetActive(false);
        registerPanel.SetActive(true);
    }

    public void TestGame()
    {
        PlayerPrefs.SetString("userEmail", "test@email.com");
        SceneManager.LoadScene("SettingsScene");
    }

}
