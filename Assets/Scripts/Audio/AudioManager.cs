using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public List<Audio> audios;
    private Dictionary<string, AudioSource> _sources = new Dictionary<string, AudioSource>();

    private void Awake() {
        
        for(var i= 0; i < audios.Count; i++)
        {
            var source = gameObject.AddComponent<AudioSource>();
            source.clip = audios[i].clip;
            source.volume = audios[i].volume;
            source.pitch = audios[i].pitch;
            source.priority = audios[i].priority;
            source.loop = audios[i].loop;
            _sources.Add(audios[i].id,source);
        }
    }

    public void Play(string id)
    {
        var source = _sources[id];
        if(source != null && !source.isPlaying)
            source.Play();
    }

    public void Stop(string id) 
    {
        var source = _sources[id];
        if(source != null && source.isPlaying)
            source.Stop();
    }

}
