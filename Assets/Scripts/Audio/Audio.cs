using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Audio : MonoBehaviour
{
    public string id;
    public AudioClip clip;
    [Range(0f, 1f)] public float volume;
    [Range(-3f, 3f)] public float pitch;
    public int priority;
    public bool loop;
}
