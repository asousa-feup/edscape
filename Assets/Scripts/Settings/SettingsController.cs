using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SettingsController : MonoBehaviour
{
    public GameObject addButton;
    public GameObject buttons;
    public GameObject languageButton;
    public GameObject loadPanel;
    public GameObject settingsPanel;
    public BackofficeController backofficeController;

    private List<GameObject> _gameList = new List<GameObject>();
    public Settings settings { get; set;}
    public GameSettings selectedGame { get; set;}
    public ConfigGameObj selectedGameInformation { get; set;}

    // Start is called before the first frame update
    void Start()
    {
        LoadGames();
    }
    
    // Loads all the available games.
    public void LoadGames(){
        // Turn on loading panel
        loadPanel.SetActive(true);
        
        StartCoroutine(GetGames(Application.streamingAssetsPath + "/settings.json"));
    }

    IEnumerator GetGames(string path)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(path))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            var saveString = webRequest.downloadHandler.text;
            try
            {
                settings = JsonUtility.FromJson<Settings>(saveString);
                if(settings.games == null)
                    yield break;
                
                RemoveGameButtons();

                foreach (var game in settings.games)
                {
                    var button = Instantiate(languageButton, new Vector3(0, 0,0), Quaternion.identity, buttons.transform);
                    button.transform.position += new Vector3(1, 0, 0);
                    button.GetComponentInChildren<Text>().text = game.name;
                    button.GetComponent<Button>().onClick.AddListener(() => SelectGame(game));
                    _gameList.Add(button);
                }
                
                StartCoroutine(UpdateRect());
            }
            catch(Exception e)
            {
                Debug.LogError(e);
            }
        }

    }

    void SelectGame(GameSettings game){
        // Update selected game
        selectedGame = game;
        
        PlayerPrefs.SetString("gameFile", game.path);
        PlayerPrefs.SetInt("test", game.test ? 1 : 0);

        // Gets game information
        StartCoroutine(GetGameInformation(Application.streamingAssetsPath + "/" + PlayerPrefs.GetString("gameFile")));
    }
    
    IEnumerator GetGameInformation(string filePath){
        using (UnityWebRequest webRequest = UnityWebRequest.Get(filePath))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            var dataAsJson = webRequest.downloadHandler.text;

            //verification
            try
            {
                //conversion from json to game data
                selectedGameInformation = JsonUtility.FromJson<ConfigGameObj>(dataAsJson);
                // Active backoffice to view information
                backofficeController.View();
                
                // Turn off settings panel
                settingsPanel.SetActive(false);
            }
            catch(Exception e)
            {
                Debug.LogError(e);
            }
        }
    }

    // Starts the game.
    public void Play()
    {
        // Turn on loading panel
        loadPanel.SetActive(true);

        PlayerPrefs.SetString("issuers", selectedGameInformation.issuerEmail);
        PlayerPrefs.SetString("game_version", selectedGameInformation.gameId);
        
        // Choose lang from text option
        if (selectedGameInformation.textOptionIDs != null && selectedGameInformation.textOptionIDs.Count > 1)
        {
            Destroy(addButton);
            RemoveGameButtons();
            foreach (var lanOp in selectedGameInformation.textOptionIDs)
            {
                var button = Instantiate(languageButton, new Vector3(0, 0,0), Quaternion.identity, buttons.transform);
                button.transform.position += new Vector3(1, 0, 0);
                button.GetComponentInChildren<Text>().text = lanOp;
                button.GetComponent<Button>().onClick.AddListener(() => SelectLanguageTextOp(lanOp, selectedGameInformation.initialSurveyPath, selectedGameInformation.finalSurveyPath));
            }
            StartCoroutine(UpdateRect());
        }
        else
        {
            if ((selectedGameInformation.textOptionIDs != null) && (selectedGameInformation.textOptionIDs.Count == 1)){
                SelectLanguageTextOp(selectedGameInformation.textOptionIDs[0], selectedGameInformation.initialSurveyPath, selectedGameInformation.finalSurveyPath);
            }
            else{
                SelectLanguageTextOp(null, selectedGameInformation.initialSurveyPath, selectedGameInformation.finalSurveyPath);
            }
        }
    }

    // Remove all game buttons in the rect
    // Cleans the game list
    void RemoveGameButtons(){
        foreach (var button in _gameList)
        {
            Destroy(button);
        }
        _gameList = new List<GameObject>();
    }

    IEnumerator UpdateRect()
    {
        var horizontalLayout = buttons.GetComponent<HorizontalLayoutGroup>();

        if (horizontalLayout != null)
        {
            horizontalLayout.enabled = false;
            yield return new WaitForSeconds(0.0F);
            
            horizontalLayout.enabled = true;
            // Turn off loading panel
            loadPanel.SetActive(false);
        }
    }

    private void SelectLanguageTextOp(string lanOpID, List<PathOp> initialSurvey, List<PathOp> finalSurvey)
    {
        var survey = FindPath(initialSurvey, lanOpID);
        PlayerPrefs.SetString("initialSurveyPath", survey);
        PlayerPrefs.SetString("finalSurveyPath", FindPath(finalSurvey, lanOpID));
        PlayerPrefs.SetString("lanOpID", lanOpID);
        
        if (string.IsNullOrEmpty(survey) || PlayerPrefs.GetInt("test").Equals(1)) // Game
        {
            PlayerPrefs.SetInt("finalSurvey", 1);
            SceneManager.LoadScene("Room"); // Initial survey -> Game
        }
        else
        {
            PlayerPrefs.SetInt("finalSurvey", 0);
            SceneManager.LoadScene("Survey");
        }
    }

    public string FindPath(List<PathOp> textOps, string langOp)
    {
        if (textOps == null)
            return "";

        if (string.IsNullOrEmpty(langOp))
        {
            foreach (var row in textOps.Where(row => string.IsNullOrEmpty(row.id)))
            {
                // Check if the path is empty or null
                // if(string.IsNullOrEmpty(row.path))
                //     // return url if path is empty
                //     return row.url;
                return row.path;
            }
            return "";
        }
        
        foreach (var row in textOps.Where(row =>  row is {id: { }} && row.id.Equals(langOp)))
        {
           // Check if the path is empty or null
            // if(string.IsNullOrEmpty(row.path))
            //     // return url if path is empty
            //     return row.url;
            return row.path;
        }
        return "";
    }
}


[Serializable]
public class Settings
{
    public List<GameSettings> games;
}

[Serializable]
public class GameSettings
{
    public string name;
    public string path;
    public bool test; // test needs to be in the game settings
}