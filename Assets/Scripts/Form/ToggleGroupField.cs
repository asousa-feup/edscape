using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ToggleGroupField : FormField
{
    private List<Toggle> toggleGroup = new List<Toggle>();

    // Start is called before the first frame update
    void Start()
    {   
        // Creates a list to store the initial values
        List<string> initialValues = new List<string>();
        // Gets the toggle group body
        Transform toggleGroupBody = transform.Find("Body");

        // Gets all the toggles
        for (int i = 0; i < toggleGroupBody.childCount; i++){
            // Gets the transform that contains the toggle fields
            Transform group = toggleGroupBody.GetChild(i);
            for (int j = 0; j < group.childCount; j++){
                // Gets the component responsible for the toggle behaviour
                Toggle toggle = group.GetChild(j).gameObject.GetComponent<Toggle>();

                // Update toggle initial state
                isInteractable = toggle.interactable;
                if (toggle.isOn){
                    initialValues.Add(toggle.GetComponentInChildren<TMP_Text>().text);
                }

                // Adds the toggle to the list
                toggleGroup.Add(toggle);
            }
        }

        initialValue = initialValues;
    }

    // Returns the field value(s)
    public override object GetValue(){
        // Creates list to store all the values
        List<string> values = new List<string>();

        // Loops through all the toggles and stores the ones that are on
        foreach (Toggle toggle in toggleGroup){
            if(toggle.isOn){
                values.Add(toggle.GetComponentInChildren<TMP_Text>().text);
            }
        }

        return values;
    }

    // Sets the field value(s)
    protected override void SetValue(object value){
        // Checks if the value is valid
        if (value == null){
            // Update flag indicating that the value has changed
            valueIsSet = true;
            return;
        }
        
        // Variable that will store the value if the cast dont throw an error
        List<string> values;

        // Encapsulate cast so the code dont crash 
        try {
            values = (List<string>) value;
        }catch (Exception){
            // if something goes wrong set to the initial value
            values = (List<string>) initialValue;
        }

        // if the list is empty
        if(values.Count == 0){
            // Update flag indicating that the value has changed
            valueIsSet = true;
            return;
        }
        
        // Loops through all the toggles and turns on the ones that are inside the values list
        foreach (Toggle toggle in toggleGroup){
            if (values.Contains(toggle.GetComponentInChildren<TMP_Text>().text)){
                toggle.isOn = true;
            }
        }

        // Update flag indicating that the value has changed
        valueIsSet = true;
    }

    // Clears the form field value
    protected override void Clear(){
        // Update flag indicating that the value has changed
        valueIsSet = false;

        // Gets all the initial values
        List<string> values =  (List<string>) initialValue;
        
        // Loops through all the toggles and turns on the ones that are inside the values list
        foreach (Toggle toggle in toggleGroup){
            if (values.Contains(toggle.GetComponentInChildren<TMP_Text>().text)){
                toggle.isOn = true;
            }else{
                toggle.isOn = false;
            }
        }
    }

    // Enables the form field
    protected override void Enable(){
        // Check if toggle group is interactable
        if (isInteractable){
            // Loops through all the toggles and turns them on
            foreach (Toggle toggle in toggleGroup){
                toggle.interactable = true;
            }
        }
    }

    // Disables the form field
    protected override void Disable(){
        // Loops through all the toggles and turns them on
        foreach (Toggle toggle in toggleGroup){
            toggle.interactable = false;
        }
    }
}
