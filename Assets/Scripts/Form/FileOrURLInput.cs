using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FileOrURLInput : FormField
{
    [SerializeField] private EqualInputSwitch equalInputSwitch;

    private Dictionary<string, GameObject> inputFieldBodies = new Dictionary<string, GameObject>();
    private FormField availableLanguages;

    // Start is called before the first frame update
    void Start()
    {
        // Get all the file or URL input body scripts
        for(int i = 1; i < transform.childCount; i++){
            if( transform.GetChild(i).name == "StandartBody" ){
                inputFieldBodies[""] = transform.GetChild(i).gameObject;
                continue;
            }
            inputFieldBodies[transform.GetChild(i).name] = transform.GetChild(i).gameObject;
            // Deactivate all bodies except StandartBody
            transform.GetChild(i).gameObject.SetActive(false);
        }

        // Search for the form field responsible for the selection of the available languages
        foreach (GameObject formField in GameObject.FindGameObjectsWithTag("FormField")){
            FormField formFieldScript = formField.GetComponent<FormField>();

            // Checks if the script was found and if the field id equals to "textOptionIDs"
            if( (formFieldScript != null) && (formFieldScript.GetFieldId() == "textOptionIDs") ){
                availableLanguages = formFieldScript;
                break;
            }
        }
        
        // Start language verification
        StartCoroutine("CheckAvailableLanguages");
    }

    // Returns the field value(s)
    public override object GetValue(){
        List<PathOp> values = new List<PathOp>();

        // Loops through all the input field bodies
        foreach (var inputFieldBody in inputFieldBodies){
            // Checks if the gameObject is active
            if (inputFieldBody.Value.activeSelf){
                PathOp pathOp = inputFieldBody.Value.GetComponent<FileOrURLInputBody>().GetValue();
                
                // If the values is not valid
                if (pathOp == null){
                    continue;
                }

                // Sets the pathOp id
                pathOp.id = inputFieldBody.Key;
                // Adds the pathOp to the list
                values.Add(pathOp);
            }
        }

        return values;
    }

    // Sets the field value(s)
    protected override void SetValue(object value){
        List<PathOp> values;

        // Encapsulate cast so the code dont crash 
        try {
            values = (List<PathOp>) value;
        }catch (Exception){
            // if something goes wrong set to the initial value
            values = new List<PathOp>();
        }

        // Checks if the list is empty
        if(values.Count == 0){
            inputFieldBodies[""].SetActive(true);
            // Update flag indicating that the value has changed
            valueIsSet = true;
            return;
        }

        // Deactivate StandartBody
        inputFieldBodies[""].SetActive(false);

        // Loops through all values
        foreach (PathOp pathOp in values){
            // Checks if the id is valid
            if (pathOp.id == null){
                // Activate StandartBody
                inputFieldBodies[""].SetActive(true);
                inputFieldBodies[""].GetComponent<FileOrURLInputBody>().SetValue(pathOp);
                continue;
            }

            // Checks if the key exists
            if(inputFieldBodies.ContainsKey(pathOp.id)){
                // Activates the field and displays the value
                inputFieldBodies[pathOp.id].SetActive(true);
                inputFieldBodies[pathOp.id].GetComponent<FileOrURLInputBody>().SetValue(pathOp);
            }
        }

        // Update flag indicating that the value has changed
        valueIsSet = true;
    }

    // Clears the form field value
    protected override void Clear(){
        // Update flag indicating that the value has changed
        valueIsSet = false;

        // Loops through all the input field bodies
        foreach (var inputFieldBody in inputFieldBodies){
            // Clear the field
            inputFieldBody.Value.GetComponent<FileOrURLInputBody>().Clear();

            // Deactivate all bodies except StandartBody
            if(inputFieldBody.Value.name != "StandartBody"){
                // Deactivate the field
                inputFieldBody.Value.SetActive(false);
            }
        }
    }

    // Enables the form field
    protected override void Enable(){
        // Loops through all the input field bodies
        foreach (var inputFieldBody in inputFieldBodies){
            // Enables the field
            inputFieldBody.Value.GetComponent<FileOrURLInputBody>().Enable();
        }
        // Start language verification
        StartCoroutine("CheckAvailableLanguages");

        // Checks if the equalInputSwitch is set
        if(equalInputSwitch != null){
            equalInputSwitch.Enable();
        }
    }

       

    // Disables the form field
    protected override void Disable(){
        // Loops through all the input field bodies
        foreach (var inputFieldBody in inputFieldBodies){
            // Disables the field
            inputFieldBody.Value.GetComponent<FileOrURLInputBody>().Disable();
        }

        // Stop language verification
        StopCoroutine("CheckAvailableLanguages");

        // Checks if the equalInputSwitch is set
        if(equalInputSwitch != null){
            equalInputSwitch.Disable();
        }
    }

    IEnumerator CheckAvailableLanguages(){
        List<string> languages = new List<string>();

        while (true){
            // Gets the languages available
            languages = (List<string>)availableLanguages.GetValue();

            // Loops through all the input field bodies
            foreach (var inputFieldBody in inputFieldBodies){
                // Checks if the list of languages is empty or null
                if ( (languages.Count == 0) || (languages==null) ){
                    // Deactivate all bodies except StandartBody
                    if(inputFieldBody.Value.name != "StandartBody"){
                        // Deactivate the field
                        inputFieldBody.Value.SetActive(false);
                    }else{
                        // Activate StandartBody
                        inputFieldBody.Value.SetActive(true);
                    }
                // Checks if the gameObject is active
                }else if (languages.Contains(inputFieldBody.Key)){
                    // Activate the field
                    inputFieldBody.Value.SetActive(true);
                }else{
                    // Deactivate the field
                    inputFieldBody.Value.SetActive(false);
                }

                yield return null;
            }
        }
    }
}
