using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Dropdowns : FormField
{
    public TMP_Dropdown [] dropdowns { get; set; }
    public GameObject [] buttons { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        // Array size equals to the number of childs less the label
        dropdowns = new TMP_Dropdown[transform.childCount-1];
        buttons = new GameObject[transform.childCount-1];
        string [] initialValueTemp = new string[transform.childCount-1];

        // Loops through all the childs, skipping the label.
        for(int i = 1; i < transform.childCount; i++){
            
            // Gets the dropdown's transform
            Transform child = transform.GetChild(i);
            // Gets the child tranform with the name "Dropdown"
            Transform dropdownTransform = child.Find("Dropdown");
            // Gets the child tranform with the name "Buttons"
            Transform buttonsTransform = child.Find("Buttons");

            // Checks if dropdownTransform or buttonsTransform was not found
            if (dropdownTransform == null || buttonsTransform == null)
                continue;
            
            // Gets the TMP_Dropdown component from the Dropdown gameObject
            TMP_Dropdown dropdown = dropdownTransform.gameObject.GetComponent<TMP_Dropdown>();

            // Checks if dropdown was not found
            if (dropdown == null)
                continue;
            
            // Stores all the necessary information in the arrays
            dropdowns[i-1] = dropdown;
            buttons[i-1] = buttonsTransform.gameObject;
            initialValueTemp[i-1] = dropdown.options[dropdown.value].text;
            isInteractable = dropdown.interactable;
        }

        initialValue = initialValueTemp;
    }

    // Returns the field value(s)
    public override object GetValue(){
        // Creates a string list that will store all the values
        List<string> values = new List<string>();
        
        // Loops through all the dropdowns
        for(int i = 0; i < dropdowns.Length; i++){
            string value = dropdowns[i].options[dropdowns[i].value].text;
            
            // If the value is empty, null or None just skip
            if (string.IsNullOrEmpty(value) || (value == "None")){
                continue;
            }

            // Stores the dropdown value
            values.Add(value);
        }

        return values.ToArray();
    }

    // Sets the field value(s)
    protected override void SetValue(object value){
        // Checks if the value is valid
        if (value == null){
            // Update flag indicating that the value has changed
            valueIsSet = true;
            return;
        }
        
        // Variable that will store the value if the cast dont throw an error
        string [] values;

        // Encapsulate cast so the code dont crash 
        try {
            values = (string[]) value;
        }catch (Exception){
            // if something goes wrong set to the initial value
            values = (string[]) initialValue;
        }
        
        // If the list of values is empty
        if (values.Length == 0){
            // Update flag indicating that the value has changed
            valueIsSet = true;
            return;
        }
        
        StartCoroutine(SetDropdowns(values));
    }

    // Coroutine that sets the dropdown values
    IEnumerator SetDropdowns(string [] values){
        // Adds as many dropdowns as the values length
        // Starts in 1 because already exist one dropdown
        for(int i = 1; i < values.Length; i++){
            transform.GetChild(i).gameObject.GetComponent<AddAndRemoveDropdowns>().AddDropdown();
        }

        // Skip to next frame, wait for the dropdowns to initialize
        yield return null;

        // Loops through all the dropdowns
        for(int i = 0; i < dropdowns.Length; i++){
            foreach(TMP_Dropdown.OptionData optionData in dropdowns[i].options){
                if (string.Equals(optionData.text, values[i])){
                    // Sets the dropdown value
                    dropdowns[i].value = dropdowns[i].options.IndexOf(optionData);
                    break;
                }
            }
            // Skip to the next frame
            yield return null;
        }

        // Update flag indicating that the value has changed
        valueIsSet = true;
    }

    // Clears the form field value
    protected override void Clear(){
        // Update flag indicating that the value has changed
        valueIsSet = false;

        string [] initialvalues = (string[]) initialValue;

        // Loops through all the dropdowns and removes them
        // Finishes in initialvalues because we need to leave as many dropdowns as the initial values length
        // Loop needs to be in reverse because we are removing gameObjects, which will consequently, change object indexes
        for (int i = (transform.childCount -1) ; i > initialvalues.Length; i--){
            transform.GetChild(i).gameObject.GetComponent<AddAndRemoveDropdowns>().RemoveDropdown();
        }

        // Loops through all the dropdowns
        for(int i = 0; i < dropdowns.Length; i++){
            foreach(TMP_Dropdown.OptionData optionData in dropdowns[i].options){
                if (string.Equals(optionData.text, initialvalues[i])){
                    // Sets the dropdown value
                    dropdowns[i].value = dropdowns[i].options.IndexOf(optionData);
                    break;
                }
            }
        }
    }

    // Enables the form field
    protected override void Enable(){
        // Check if text dropdowns are interactable
        if (isInteractable){
            // Loops through all the dropdowns
            for(int i = 0; i < dropdowns.Length; i++){
                // Sets the dropdown interactivity
                dropdowns[i].interactable = true;
                // Enables the buttons
                buttons[i].SetActive(true);
            }
        }
    }

    // Disables the form field
    protected override void Disable(){
        // Loops through all the dropdowns
        for(int i = 0; i < dropdowns.Length; i++){
            // Sets the dropdown interactivity
            dropdowns[i].interactable = false;
            // Disables the buttons
            buttons[i].SetActive(false);
        }
    }
}
