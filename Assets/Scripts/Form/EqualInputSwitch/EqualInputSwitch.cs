using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EqualInputSwitch : MonoBehaviour
{
    [SerializeField] private FormField firstFormField;
    [SerializeField] private FormField secondFormField;

    private bool isOn = false;

    // Function that is called when the switch changes its state.
    public void SwitchAction(bool value){
        // Sets the isOn value
        isOn = value;

        // If the switch is on
        if (isOn){
            // Start coroutine that will copy the information from the first form field to the second one.
            StartCoroutine("CopyValueFromFirstToSecondField");
            return;
        }
    }

    // Coroutine that will copy the information from the first form field to the second one.
    IEnumerator CopyValueFromFirstToSecondField(){
        // While the switch is on
        while (isOn){
            // Checks if the first and second form field are valid
            if(firstFormField == null || secondFormField == null){
                yield break;
            }
            // Update the second field value with the value of the first form field
            secondFormField.View(firstFormField.GetValue());
            
            yield return null;
        }
        // Change second form file state to create
        secondFormField.Edit();
    }

    // Enables this gameObject and resets its value
    public void Enable(){
        gameObject.SetActive(true);
        GetComponent<Toggle>().isOn = false;
    }

    // Disables this gameObject stops the coroutine
    public void Disable(){
        gameObject.SetActive(false);
        StopCoroutine("CopyValueFromFirstToSecondField");
    }
}
