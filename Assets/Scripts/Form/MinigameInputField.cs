using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MinigameInputField : FormField
{   
    private TMP_Dropdown dropdown;
    private Dictionary<string, GameObject> minigameObjects = new Dictionary<string, GameObject>();
    private string currentMinigame = "None";

    // Start is called before the first frame update
    void Start()
    {   
        // Get all the minigame input fields
        for(int i = 2; i < transform.childCount; i++){
            GameObject child = transform.GetChild(i).gameObject;
            child.SetActive(false);

            // Add them to the dictionary
            minigameObjects.Add(child.name,child);
        }

        // Get dropdown script
        dropdown = transform.Find("Dropdown").GetComponent<TMP_Dropdown>();

        // Set default values
        initialValue = dropdown.options[dropdown.value];
        isInteractable = dropdown.interactable;
    }

    // Returns the field value(s)
    public override object GetValue(){
        // Checks if current minigame is None
        if(currentMinigame == "None"){
            return null;
        }

        List<PathOp> pathOptions = minigameObjects[currentMinigame].GetComponent<MinigameInputFieldBody>().GetValue();
        
        // Check if the pathOptions is empty
        if(pathOptions.Count == 0){
            return null;
        }

        // Build minigame class to be returned
        MiniGame minigame = new MiniGame();
        minigame.id = dropdown.options[dropdown.value].text;
        minigame.pathOptions = pathOptions;

        return minigame;
    }

    // Sets the field value(s)
    protected override void SetValue(object value){
        MiniGame miniGame;

        // Cast to minigame between try catch
        try{
            miniGame = (MiniGame) value;

            // Check if the minigame is exists in the dictionary 
            // if(minigameObjects.ContainsKey(miniGame.id)){
            //     // Update dropdown value 
            //     foreach(TMP_Dropdown.OptionData optionData in dropdown.options){
            //         if (string.Equals(optionData.text, miniGame.id)){
            //             dropdown.value = dropdown.options.IndexOf(optionData);
            //         }
            //     }
            //     // Send value to the minigame input field
            //     minigameObjects[miniGame.id].GetComponent<MinigameInputFieldBody>().SetValue(miniGame.pathOptions);
            //     // Set current minigame
            //     currentMinigame = miniGame.id;
            // }else{
            //     // Set dropdown value with the initial value
            //     dropdown.value = dropdown.options.IndexOf((TMP_Dropdown.OptionData) initialValue);
            //     // Reset current minigame
            //     currentMinigame = dropdown.options[dropdown.value].text;
            // }
            
            if(miniGame != null){
                if(miniGame.id == ""){
                    // Set dropdown value with the URL value
                    dropdown.value = 0;
                }else{
                    // Set dropdown value with the URL value
                    dropdown.value = 1;
                    // Send value to the minigame input field
                    minigameObjects["URL"].GetComponent<MinigameInputFieldBody>().SetValue(miniGame.pathOptions);
                }
                // Reset current minigame
                currentMinigame = dropdown.options[dropdown.value].text;
            }

        }catch(Exception){
            // Set dropdown value with the initial value
            dropdown.value = dropdown.options.IndexOf((TMP_Dropdown.OptionData) initialValue);
            // Reset current minigame
            currentMinigame = dropdown.options[dropdown.value].text;
        }

        // Update flag indicating that the value has changed
        valueIsSet = true;
    }

    // Clears the form field value
    protected override void Clear(){
        // Update flag indicating that the value has changed
        valueIsSet = false;
        // Set dropdown value with the initial value
        dropdown.value = dropdown.options.IndexOf((TMP_Dropdown.OptionData) initialValue);
        // Reset current minigame
        currentMinigame = dropdown.options[dropdown.value].text;

        // Loop through all the minigames
        foreach(var minigame in minigameObjects){
            // Clear the minigame field
            minigame.Value.GetComponent<MinigameInputFieldBody>().Clear();
            // Deactivate minigame field
            minigame.Value.SetActive(false);
        }
    }

    // Enables the form field
    protected override void Enable(){
        if(isInteractable){
            // Check if the dictionary contains the specified key
            if(minigameObjects.ContainsKey(currentMinigame)){
                // Activate current minigame
                minigameObjects[currentMinigame].GetComponent<MinigameInputFieldBody>().Enable();
            }

            dropdown.interactable = true;
        }
    }

    // Disables the form field
    protected override void Disable(){
        // Check if the dictionary contains the specified key
        if(minigameObjects.ContainsKey(currentMinigame)){
            // Deactivate current minigame
            minigameObjects[currentMinigame].GetComponent<MinigameInputFieldBody>().Disable();
        }

        dropdown.interactable = false;
    }

    public void SwitchMinigame(int value){
        // Get the dropdown option text
        string newMinigame = dropdown.options[value].text;

        // Check if the dictionary contains the specified key
        if(minigameObjects.ContainsKey(currentMinigame)){
            // Deactivate current minigame
            minigameObjects[currentMinigame].SetActive(false);
        }

        // Check if the dictionary contains the specified key
        if(minigameObjects.ContainsKey(newMinigame)){
            // Activate new minigame
            minigameObjects[newMinigame].SetActive(true);
        }

        currentMinigame = newMinigame;
    }
}
