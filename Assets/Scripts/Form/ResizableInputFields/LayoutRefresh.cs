using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
Class used to refresh the layout group
*/
public class LayoutRefresh : MonoBehaviour
{
    private RectTransform layoutRoot;
    private RectTransform resizableObject;
    private float resizableObjectHeight;
    private float resizableObjectWidth;

    // Start is called before the first frame update
    void Start()
    {
        resizableObject = GetComponent<RectTransform>();
        // Gets the rectTransform of the parent gameObject that will contain a vertical layout group
        layoutRoot = transform.parent.GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update(){
        
        // Checks if the resizable object was not assigned
        if (resizableObject == null )
            return;
        
        // Calculates the new height and width of the resizable object
        float newResizableObjectHeight = resizableObject.sizeDelta.y * resizableObject.localScale.y;
        float newResizableObjectWidth = resizableObject.sizeDelta.x * resizableObject.localScale.x;

        // Checks if the height or width of the resizable object has changed
        if ( (newResizableObjectHeight != resizableObjectHeight) || (newResizableObjectWidth != resizableObjectWidth) ){

            // If it has changed, update the old height and width and refresh the layout group
            resizableObjectHeight = newResizableObjectHeight;
            resizableObjectWidth = newResizableObjectWidth;
            Refresh();

        }
    }
    
    // Refreshes the layout group
    public void Refresh(){
        
        // Checks if the layout root was not assigned
        if (layoutRoot == null)
            return;
        
        // Calls the function that refreshes the layout group
        LayoutRebuilder.ForceRebuildLayoutImmediate(layoutRoot);

    }
}
