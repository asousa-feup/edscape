using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleField : FormField
{
    private Toggle toggle;

    void Start(){
        // Gets the toggle component from the toggle field
        toggle = GetComponent<Toggle>();

        // Checks if toggle was not found
        if (toggle == null)
            return;
        
        // Gets the initial state of the toggle field
        initialValue = toggle.isOn;
        isInteractable = toggle.interactable;
    }

    // Returns the field value(s)
    public override object GetValue(){
        // Checks if toggle was not found
        if (toggle == null)
            return false;

        return toggle.isOn;
    }

    // Sets the field value(s)
    protected override void SetValue(object value){
        // Checks if toggle was not found
        if (toggle == null){
            // Update flag indicating that the value has changed
            valueIsSet = true;
            return;
        }

        // Variable that will store the value if the cast dont throw an error
        bool newValue;

        // Encapsulate cast so the code dont crash 
        try {
            newValue = (bool) value;
        }catch (Exception){
            // if something goes wrong set to the initial value
            newValue = (bool) initialValue;
        }

        toggle.isOn = newValue;

        // Update flag indicating that the value has changed
        valueIsSet = true;
    }

    // Clears the form field value
    protected override void Clear(){
        // Update flag indicating that the value has changed
        valueIsSet = false;

        // Checks if toggle was not found
        if (toggle == null)
            return;

        toggle.isOn = (bool) initialValue;
    }

    // Enables the form field
    protected override void Enable(){
        // Checks if toggle was not found
        if (toggle == null)
            return;

        // Check if toggle field is interactable
        if (isInteractable)
            toggle.interactable = true; 
    }

    // Disables the form field
    protected override void Disable(){
        // Checks if toggle was not found
        if (toggle == null)
            return;

        toggle.interactable = false; 
    }
}
