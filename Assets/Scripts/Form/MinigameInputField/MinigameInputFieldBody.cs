using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MinigameInputFieldBody : MonoBehaviour
{
    // [SerializeField] bool isURL = false;

    private Dictionary<string, GameObject> fileInputFields = new Dictionary<string, GameObject>();
    private FormField availableLanguages;

    // Awake is called when the script instance is being loaded
    void Awake()
    {
        // Get all the file input gameObjects
        for(int i = 0; i < transform.childCount; i++){
            if( transform.GetChild(i).name == "StandartBody" ){
                fileInputFields[""] = transform.GetChild(i).gameObject;
                continue;
            }
            fileInputFields[transform.GetChild(i).name] = transform.GetChild(i).gameObject;
            // Deactivate all bodies except StandartBody
            transform.GetChild(i).gameObject.SetActive(false);
        }

        // Search for the form field responsible for the selection of the available languages
        foreach (GameObject formField in GameObject.FindGameObjectsWithTag("FormField")){
            FormField formFieldScript = formField.GetComponent<FormField>();

            // Checks if the script was found and if the field id equals to "textOptionIDs"
            if( (formFieldScript != null) && (formFieldScript.GetFieldId() == "textOptionIDs") ){
                availableLanguages = formFieldScript;
                break;
            }
        }
    }

    // Returns the field value(s)
    public List<PathOp> GetValue(){
        List<PathOp> values = new List<PathOp>();

        // Loops through all the input field bodies
        foreach (var fileInputField in fileInputFields){
            // Checks if the gameObject is active
            if (fileInputField.Value.activeSelf){
                PathOp pathOp = new PathOp();

                // if(isURL){
                //     pathOp.url = fileInputField.Value.transform.Find("URL").GetComponent<TMP_InputField>().text;
                // }else{
                //     pathOp.path = fileInputField.Value.transform.Find("UploadFile").GetComponent<FileInput>().GetValue();
                // }

                pathOp.path = fileInputField.Value.transform.Find("URL").GetComponent<TMP_InputField>().text;
               
                // If the pathOp is not valid or if the path and url is not a valid
                if (pathOp == null || string.IsNullOrEmpty(pathOp.path)){ //&& string.IsNullOrEmpty(pathOp.url)) ){
                    continue;
                }

                // Sets the pathOp id
                pathOp.id = fileInputField.Key;
                // Adds the pathOp to the list
                values.Add(pathOp);
            }
        }

        return values;
    }

    // Sets the field value(s)
    public void SetValue(object value){
        List<PathOp> values;

        // Encapsulate cast so the code dont crash 
        try {
            values = (List<PathOp>) value;
        }catch (Exception){
            // if something goes wrong set to the initial value
            values = new List<PathOp>();
        }

        // Checks if the list is empty
        if(values.Count == 0){
            fileInputFields[""].SetActive(true);
            return;
        }

        // Deactivate StandartBody
        fileInputFields[""].SetActive(false);

        // Loops through all values
        foreach (PathOp pathOp in values){
            // Checks if the id is valid
            if (pathOp.id == null){
                // Activate StandartBody
                fileInputFields[""].SetActive(true);
                
                // if(isURL){
                //     fileInputFields[""].transform.Find("URL").GetComponent<TMP_InputField>().text = pathOp.url;
                // }else{
                //     fileInputFields[""].transform.Find("UploadFile").GetComponent<FileInput>().SetValue(pathOp.path);
                // }

                fileInputFields[""].transform.Find("URL").GetComponent<TMP_InputField>().text = pathOp.path;

                continue;
            }
            
            // Checks if the key exists
            if(fileInputFields.ContainsKey(pathOp.id)){
                // Activates the field and displays the value
                fileInputFields[pathOp.id].SetActive(true);
                
                // if(isURL){
                //     fileInputFields[pathOp.id].transform.Find("URL").GetComponent<TMP_InputField>().text = pathOp.url;
                // }else{
                //     fileInputFields[pathOp.id].transform.Find("UploadFile").GetComponent<FileInput>().SetValue(pathOp.path);
                // }

                fileInputFields[pathOp.id].transform.Find("URL").GetComponent<TMP_InputField>().text = pathOp.path;
            }
        }
    }

    // Clears the form field value
    public void Clear(){
        // Loops through all the file input fields
        foreach (var fileInputField in fileInputFields){
            // Clear the field
            // if(isURL){
            //     fileInputField.Value.transform.Find("URL").GetComponent<TMP_InputField>().text = "";
            // }else{
            //     fileInputField.Value.transform.Find("UploadFile").GetComponent<FileInput>().Clear();
            // }

            fileInputField.Value.transform.Find("URL").GetComponent<TMP_InputField>().text = "";

            // Deactivate all bodies except StandartBody
            if(fileInputField.Value.name != "StandartBody"){
                // Deactivate the field
                fileInputField.Value.SetActive(false);
            }
        }
    }

    // Enables the form field
    public void Enable(){
        // Loops through all the input field bodies
        foreach (var fileInputField in fileInputFields){
            // Enables the file input field
            // if(isURL){
            //     fileInputField.Value.transform.Find("URL").GetComponent<TMP_InputField>().interactable = true;
            // }else{
            //     fileInputField.Value.transform.Find("UploadFile").GetComponent<FileInput>().interactable = true;
            // }

            fileInputField.Value.transform.Find("URL").GetComponent<TMP_InputField>().interactable = true;
        }
        // Start language verification
        StartCoroutine("CheckAvailableLanguages");
    }

    // Disables the form field
    public void Disable(){
        // Loops through all the file input field bodies
        foreach (var fileInputField in fileInputFields){
            // Disables the file input field
            // if(isURL){
            //     fileInputField.Value.transform.Find("URL").GetComponent<TMP_InputField>().interactable = false;
            // }else{
            //     fileInputField.Value.transform.Find("UploadFile").GetComponent<FileInput>().interactable = false;
            // }

            fileInputField.Value.transform.Find("URL").GetComponent<TMP_InputField>().interactable = false;
        }

        // Stop language verification
        StopCoroutine("CheckAvailableLanguages");
    }

    IEnumerator CheckAvailableLanguages(){
        List<string> languages = new List<string>();

        while (true){
            // Gets the languages available
            languages = (List<string>)availableLanguages.GetValue();

            // Loops through all the input field bodies
            foreach (var fileInputField in fileInputFields){
                // Checks if the list of languages is empty or null
                if ( (languages.Count == 0) || (languages==null) ){
                    // Deactivate all bodies except StandartBody
                    if(fileInputField.Value.name != "StandartBody"){
                        // Deactivate the field
                        fileInputField.Value.SetActive(false);
                    }else{
                        // Activate StandartBody
                        fileInputField.Value.SetActive(true);
                    }
                // Checks if the gameObject is active
                }else if (languages.Contains(fileInputField.Key)){
                    // Activate the field
                    fileInputField.Value.SetActive(true);
                }else{
                    // Deactivate the field
                    fileInputField.Value.SetActive(false);
                }

                yield return null;
            }
        }
    }

    // This function is called when the behaviour becomes disabled.
    void OnDisable(){
        // Stop language verification
        StopCoroutine("CheckAvailableLanguages");
    }

    // This function is called when the object becomes enabled and active
    void OnEnable(){
        // Start language verification
        StartCoroutine("CheckAvailableLanguages");
    }
}
