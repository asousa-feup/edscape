using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/*
Class that adds and removes text input fields
*/
public class AddAndRemoveTextInputFields : MonoBehaviour
{
    [SerializeField] private bool firstTextInputField = false;
    
    private TextInputFields textInputFields;

    // Start is called before the first frame update
    void Start()
    {
        // Checks if this text input field is not the first one
        if (!firstTextInputField){
            
            // Gets the remove button transform
            Transform removeButtonTransform = transform.Find("Buttons/RemoveButton");

            // Checks if the remove button transform was found
            if (removeButtonTransform != null)
                // Activates the remove button
                removeButtonTransform.gameObject.SetActive(true);
                
        }

    }

    // Adds a text input field next to this one
    public void AddTextInputField(){
        // Check if textInputFields was not assigned
        if (textInputFields == null)
            textInputFields = transform.parent.GetComponent<TextInputFields>();
        
        // Gets the script that will instantiate the text input field
        InstantiateFormFields instantiateScript = transform.parent.GetComponent<InstantiateFormFields>();
        
        // Checks if the script was found
        if (instantiateScript == null)
            return;

        // Calls the function that will instantiate the text input field
        // Sends an attribute that indicates the layout position where the new text input field should be
        instantiateScript.InstantiateFormField(transform.GetSiblingIndex() + 1);

        // Gets the InputField's transform of the new input field
        Transform newInputFieldTransform = transform.parent.GetChild(transform.GetSiblingIndex() + 1);
        // Gets the new input TMP_InputField
        TMP_InputField textInputField = newInputFieldTransform.Find("InputField").gameObject.GetComponent<TMP_InputField>();
        // Gets the new input buttons gameobject
        GameObject buttonsGameObject = newInputFieldTransform.Find("Buttons").gameObject;

        // Create new structures to store the input fields and buttons
        TMP_InputField [] newInputFields = new TMP_InputField[textInputFields.textInputFields.Length + 1];
        GameObject [] newButtons = new GameObject[textInputFields.buttons.Length + 1];

        // Loop through all the previous input fields and buttons
        for (int i = 0; i < textInputFields.textInputFields.Length; i++){
            newInputFields[i] = textInputFields.textInputFields[i];
            newButtons[i] = textInputFields.buttons[i];
        }
        // Add the new input field to the new array
        newInputFields[textInputFields.textInputFields.Length] = textInputField;
        // Updates the text input fields
        textInputFields.textInputFields = newInputFields;

        // Add the new buttons to the new array
        newButtons[textInputFields.buttons.Length] = buttonsGameObject;
        // Updates the buttons
        textInputFields.buttons = newButtons;
    }

    // Removes this text input field from the layout group
    public void RemoveTextInputField(){
        // Check if textInputFields was not assigned
        if (textInputFields == null)
            textInputFields = transform.parent.GetComponent<TextInputFields>();
        
        // Create new structures to store the input fields and buttons
        List<TMP_InputField> newInputFields = new List<TMP_InputField>();
        List<GameObject> newButtons = new List<GameObject>();

        // Loop through all the previous input fields and buttons
        for (int i = 0; i < textInputFields.textInputFields.Length; i++){
            // Skip this inputfield so it is not added to the list
            if (textInputFields.textInputFields[i] == transform.Find("InputField").gameObject.GetComponent<TMP_InputField>())
                continue;
            newInputFields.Add(textInputFields.textInputFields[i]);
            newButtons.Add(textInputFields.buttons[i]);
        }

        // Updates the text input fields
        textInputFields.textInputFields = newInputFields.ToArray();
        // Updates the buttons
        textInputFields.buttons = newButtons.ToArray();

        Destroy(gameObject);
    }

}
