using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AddAndRemoveDropdownOptions : MonoBehaviour
{
    private TMP_Dropdown dropdown;
    private TMP_Dropdown.OptionData previousOption;

    // Start is called before the first frame update
    void Start(){
        // Get the dropdown
        dropdown = transform.Find("Dropdown").GetComponent<TMP_Dropdown>();
        // Set the previous value
        previousOption = dropdown.options[dropdown.value];

        // Order the list by the option text
        dropdown.options.Sort(1,dropdown.options.Count-1,new OptionComparer());
       
        // Get all the dropdowns
        TMP_Dropdown [] dropdownsArray = transform.parent.GetComponent<Dropdowns>().dropdowns;

        if (dropdownsArray == null) return;

        // Loop through all the dropdowns and removes the options that are already selected
        foreach (TMP_Dropdown tempDropdown in dropdownsArray){
            RemoveDropdownOption(dropdown, tempDropdown.options[tempDropdown.value]);
        }
    }

    // Function that is called when the dropdown value changes
    public void DropDownValueChanged(int value){
        // Get the Dropdowns script
        Dropdowns dropdowns = transform.parent.GetComponent<Dropdowns>();

        // Get all the dropdowns
        TMP_Dropdown [] dropdownsArray = dropdowns.dropdowns;

        // Loop through all the dropdowns
        foreach (TMP_Dropdown tempDropdown in dropdownsArray){
            // if is this dropdown just skip
            if(tempDropdown == dropdown){
                continue;
            }

            // Add the old option
            AddDropdownOption(tempDropdown);
            // Add the new option
            RemoveDropdownOption(tempDropdown, dropdown.options[value]);
        }

        // Update the previous state
        previousOption = dropdown.options[value];
    }

    // Adds the option to the dropdown
    private void AddDropdownOption(TMP_Dropdown tmp_dropdown){
        // Check if the previous option is null or the default one
        if ((previousOption == null) || (previousOption.text == "None")){
            return;
        }

        // Gets the text of the selected option
        string optionText = tmp_dropdown.options[tmp_dropdown.value].text;
        // Insert the option in to the list
        tmp_dropdown.options.Add(previousOption);
        // Order the list by the option text
        tmp_dropdown.options.Sort(1,tmp_dropdown.options.Count-1,new OptionComparer());

        // Loop through all the options and get the new value
        for(int i=0; i<tmp_dropdown.options.Count; i++){
            if(tmp_dropdown.options[i].text == optionText){
                tmp_dropdown.value = i;
                break;
            }
        }
    }

    // Removes the option from the dropdown
    private void RemoveDropdownOption(TMP_Dropdown tmp_dropdown, TMP_Dropdown.OptionData option){
        // Check if the new option is null or the default one
        if ((previousOption == null) || (option.text == "None")){
            return;
        }

        // Gets the text of the selected option
        string optionText = tmp_dropdown.options[tmp_dropdown.value].text;

        // Loops through all the options and removes the option with the same text
        foreach (TMP_Dropdown.OptionData temp_option in tmp_dropdown.options){
            if (temp_option.text == option.text){
                // Remove the option from the list
                tmp_dropdown.options.Remove(temp_option);
                break;
            }
        }

        // Order the list by the option text
        tmp_dropdown.options.Sort(1,tmp_dropdown.options.Count-1,new OptionComparer());

        // Loop through all the options and get the new value
        for(int i=0; i<tmp_dropdown.options.Count; i++){
            if(tmp_dropdown.options[i].text == optionText){
                tmp_dropdown.value = i;
                break;
            }
        }
    }

    // Function that will be called when this gameObject is destroyed
    void OnDestroy(){
        // Leave the selected option available
        DropDownValueChanged(0);
    }
}

// Class used to compare two OptionData
class OptionComparer: IComparer<TMP_Dropdown.OptionData>
{
    public int Compare(TMP_Dropdown.OptionData x, TMP_Dropdown.OptionData y){
        return string.Compare(x.text, y.text);
    }
}