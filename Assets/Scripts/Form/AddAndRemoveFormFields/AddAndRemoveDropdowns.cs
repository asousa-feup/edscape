using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/*
Class that adds and removes dropdowns
*/
public class AddAndRemoveDropdowns : MonoBehaviour
{
    [SerializeField] private bool firstDropdown = false;
    
    private Dropdowns dropdowns;

    // Start is called before the first frame update
    void Start()
    {
        // Checks if this dropdown is not the first one
        if (!firstDropdown){
            
            // Gets the remove button transform
            Transform removeButtonTransform = transform.Find("Buttons/RemoveButton");

            // Checks if the remove button transform was found
            if (removeButtonTransform != null)
                // Activates the remove button
                removeButtonTransform.gameObject.SetActive(true);
                
        }

    }

    // Adds a dropdown next to this one
    public void AddDropdown(){
        // Check if dropdowns was not assigned
        if (dropdowns == null)
            dropdowns = transform.parent.GetComponent<Dropdowns>();
        
        // Gets the script that will instantiate the dropdown
        InstantiateFormFields instantiateScript = transform.parent.GetComponent<InstantiateFormFields>();
        
        // Checks if the script was found
        if (instantiateScript == null)
            return;

        // Calls the function that will instantiate the dropdown
        // Sends an attribute that indicates the layout position where the new dropdown should be
        instantiateScript.InstantiateFormField(transform.GetSiblingIndex() + 1);

        // Gets the new dropdown's transform
        Transform newDropdownTransform = transform.parent.GetChild(transform.GetSiblingIndex() + 1);
        // Gets the new input TMP_Dropdown
        TMP_Dropdown dropdown = newDropdownTransform.Find("Dropdown").gameObject.GetComponent<TMP_Dropdown>();
        // Gets the new input buttons gameobject
        GameObject buttonsGameObject = newDropdownTransform.Find("Buttons").gameObject;

        // Create new structures to store the dropdowns and buttons
        TMP_Dropdown [] newDropdowns = new TMP_Dropdown[dropdowns.dropdowns.Length + 1];
        GameObject [] newButtons = new GameObject[dropdowns.buttons.Length + 1];

        // Loop through all the previous dropdowns and buttons
        for (int i = 0; i < dropdowns.dropdowns.Length; i++){
            newDropdowns[i] = dropdowns.dropdowns[i];
            newButtons[i] = dropdowns.buttons[i];
        }
        // Add the new dropdown to the new array
        newDropdowns[dropdowns.dropdowns.Length] = dropdown;
        // Updates the dropdowns
        dropdowns.dropdowns = newDropdowns;

        // Add the new buttons to the new array
        newButtons[dropdowns.buttons.Length] = buttonsGameObject;
        // Updates the buttons
        dropdowns.buttons = newButtons;
    }

    // Removes this dropdown from the layout group
    public void RemoveDropdown(){
        // Check if dropdowns was not assigned
        if (dropdowns == null)
            dropdowns = transform.parent.GetComponent<Dropdowns>();
        
        // Create new structures to store the dropdowns and buttons
        List<TMP_Dropdown> newDropdowns = new List<TMP_Dropdown>();
        List<GameObject> newButtons = new List<GameObject>();

        // Loop through all the previous dropdowns and buttons
        for (int i = 0; i < dropdowns.dropdowns.Length; i++){
            // Skip this dropdown so it is not added to the list
            if (dropdowns.dropdowns[i] == transform.Find("Dropdown").gameObject.GetComponent<TMP_Dropdown>())
                continue;
            newDropdowns.Add(dropdowns.dropdowns[i]);
            newButtons.Add(dropdowns.buttons[i]);
        }

        // Updates the dropdowns
        dropdowns.dropdowns = newDropdowns.ToArray();
        // Updates the buttons
        dropdowns.buttons = newButtons.ToArray();

        Destroy(gameObject);
    }

}
