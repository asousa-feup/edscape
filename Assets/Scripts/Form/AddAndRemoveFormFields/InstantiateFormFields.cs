using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateFormFields : MonoBehaviour
{
    [SerializeField] private GameObject formFieldPrefab;

    // Instantiates a form field in the specified layout position
    public void InstantiateFormField(int layoutPosition){

        // Checks if the form field prefab was assigned
        if (formFieldPrefab == null)
            return;
        
        // Creates a new gameObject with the form fied prefab
        GameObject go = (GameObject)Instantiate(formFieldPrefab);
        // Moves it inside the layout group
        go.transform.SetParent(transform, false);
        // Sets the desired position in the layout group
        go.transform.SetSiblingIndex(layoutPosition);

    }
}
