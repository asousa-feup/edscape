using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Web;
using System.Net;

public class LanguageTranslator
{
    // Dictionary with the available languages
    private Dictionary<string, string> languages = new Dictionary<string, string>() { 
        { "English", "en"}, { "French", "fr"}, { "German", "de"} , { "Italian", "it"}, { "Portuguese", "pt"}, { "Spanish", "es"}
    };

    // Returns the text and the language in which it was translated
    public TextOp Translate(string text, string language){
        // Translate text using google translate api
        return TranslateByGoogleTranslateAPI(text, language);
    }

    // Translates the text using google translate api
    private TextOp TranslateByGoogleTranslateAPI(string text, string language){
        TextOp textOp = new TextOp();
        textOp.id = language;
        textOp.text = null;

        // Build google translate api url
        string url = $"https://translate.googleapis.com/translate_a/t?client=p&sl=auto&tl={languages[language]}&dt=t&q={HttpUtility.UrlEncode(text)}";
        
        WebClient webclient = new WebClient{ Encoding = System.Text.Encoding.UTF8 };
        
        // Specify that the UpdateTextAfterTranslation method gets called when the download completes.
        webclient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(textOp.UpdateTextAfterTranslation);
        webclient.DownloadStringAsync(new Uri(url));
        
        return textOp;
    }
}
