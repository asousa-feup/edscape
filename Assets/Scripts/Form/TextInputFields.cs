using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextInputFields : FormField
{
    public TMP_InputField [] textInputFields { get; set; }
    public GameObject [] buttons { get; set; }

    void Start(){
        // Array size equals to the number of childs less the label
        textInputFields = new TMP_InputField[transform.childCount-1];
        buttons = new GameObject[transform.childCount-1];
        string [] initialValueTemp = new string[transform.childCount-1];

        // Loops through all the childs, skipping the label.
        for(int i = 1; i < transform.childCount; i++){
            
            // Gets the InputField's transform
            Transform child = transform.GetChild(i);
            // Gets the child tranform with the name "InputField"
            Transform textInputFieldTransform = child.Find("InputField");
            // Gets the child tranform with the name "Buttons"
            Transform buttonsTransform = child.Find("Buttons");

            // Checks if textInputFieldTransform or buttonsTransform was not found
            if (textInputFieldTransform == null || buttonsTransform == null)
                continue;
            
            // Gets the TMP_InputField component from the InputField gameObject
            TMP_InputField textInputField = textInputFieldTransform.gameObject.GetComponent<TMP_InputField>();

            // Checks if textInputField was not found
            if (textInputField == null)
                continue;
            
            // Stores all the necessary information in the arrays
            textInputFields[i-1] = textInputField;
            buttons[i-1] = buttonsTransform.gameObject;
            initialValueTemp[i-1] = textInputField.text;
            isInteractable = textInputField.interactable;
        }

        initialValue = initialValueTemp;
    }

    // Returns the field value(s)
    public override object GetValue(){
        // Creates a string array that will store all the values
        List<string> values = new List<string>();
        
        // Loops through all the textInputFields
        for(int i = 0; i < textInputFields.Length; i++){
            string value = textInputFields[i].text.Trim();
            
            // If the value is empty or null just skip
            if (string.IsNullOrEmpty(value)){
                continue;
            }

            // Stores the textInputField value
            values.Add(value);
        }

        return values.ToArray();
    }

    // Sets the field value(s)
    protected override void SetValue(object value){
        // Checks if the value is valid
        if (value == null){
            // Update flag indicating that the value has changed
            valueIsSet = true;
            return;
        }
        
        // Variable that will store the value if the cast dont throw an error
        string [] values;

        // Encapsulate cast so the code dont crash 
        try {
            values = (string[]) value;
        }catch (Exception){
            // if something goes wrong set to the initial value
            values = (string[]) initialValue;
        }
        
        // If the list of values is empty
        if (values.Length == 0){
            // Update flag indicating that the value has changed
            valueIsSet = true;
            return;
        }
        
        // Adds as many resizable input fields as the values length
        // Starts in 1 because already exist one resizable input field
        for(int i = 1; i < values.Length; i++){
            transform.GetChild(i).gameObject.GetComponent<AddAndRemoveTextInputFields>().AddTextInputField();
        }

        // Loops through all the textInputFields
        for(int i = 0; i < textInputFields.Length; i++){
            // Sets the textInputField value
            textInputFields[i].text = values[i];
        }

        // Update flag indicating that the value has changed
        valueIsSet = true;
    }

    // Clears the form field value
    protected override void Clear(){
        // Update flag indicating that the value has changed
        valueIsSet = false;

        string [] initialvalues = (string[]) initialValue;

        // Loops through all the resizable input fields and removes them
        // Finishes in initialvalues because we need to leave as many resizable input fields as the initial values length
        // Loop needs to be in reverse because we are removing gameObjects, which will consequently, change object indexes
        for (int i = (transform.childCount -1) ; i > initialvalues.Length; i--){
            transform.GetChild(i).gameObject.GetComponent<AddAndRemoveTextInputFields>().RemoveTextInputField();
        }

        // Loops through all the textInputFields
        for(int i = 0; i < textInputFields.Length; i++){
            // Sets the textInputField value
            textInputFields[i].text = initialvalues[i];
        }
    }

    // Enables the form field
    protected override void Enable(){
        // Check if text input fields are interactable
        if (isInteractable){
            // Loops through all the textInputFields
            for(int i = 0; i < textInputFields.Length; i++){
                // Sets the textInputField interactivity
                textInputFields[i].interactable = true;
                // Enables the buttons
                buttons[i].SetActive(true);
            }
        }
    }

    // Disables the form field
    protected override void Disable(){
        // Loops through all the textInputFields
        for(int i = 0; i < textInputFields.Length; i++){
            // Sets the textInputField interactivity
            textInputFields[i].interactable = false;
            // Disables the buttons
            buttons[i].SetActive(false);
        }
    }
}
