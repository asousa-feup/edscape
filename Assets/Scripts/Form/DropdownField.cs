using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DropdownField : FormField
{
    private TMP_Dropdown dropdown;

    void Start(){
        // Gets the child tranform with the name "Dropdown"
        Transform dropdownTransform = transform.Find("Dropdown");

        // Checks if dropdownTransform was not found
        if (dropdownTransform == null)
            return;

        // Gets the TMP_Dropdown component from the Dropdown gameObject
        dropdown = dropdownTransform.gameObject.GetComponent<TMP_Dropdown>();

        // Checks if dropdown was not found
        if (dropdown == null)
            return;

        // Checks if the dropdown has an option for None
        if (dropdown.options[0].text == "None"){
            // Order the list by the option text
            dropdown.options.Sort(1,dropdown.options.Count-1,new OptionComparer());
        }else{
            // Order the list by the option text
            dropdown.options.Sort(0,dropdown.options.Count-1,new OptionComparer());
        }
        
        // Refresh dropdown
        dropdown.RefreshShownValue();

        // Gets the initial state of the dropdown field
        initialValue = dropdown.options[dropdown.value];
        isInteractable = dropdown.interactable;
    }

    // Returns the field value(s)
    public override object GetValue(){
        // Checks if dropdown was not found
        if (dropdown == null)
            return "";

        return dropdown.options[dropdown.value].text;
    }

    // Sets the field value(s)
    protected override void SetValue(object value){
        // Checks if dropdown was not found
        if (dropdown == null){
            // Update flag indicating that the value has changed
            valueIsSet = true;
            return;
        }
        
        // Variable that will store the value if the cast dont throw an error
        string newValue;

        // Encapsulate cast so the code dont crash 
        try {
            newValue = (string) value;
        }catch (Exception){
            // if something goes wrong set to the initial value
            dropdown.value = dropdown.options.IndexOf((TMP_Dropdown.OptionData) initialValue);
            // Update flag indicating that the value has changed
            valueIsSet = true;
            return;
        }
        
        foreach(TMP_Dropdown.OptionData optionData in dropdown.options){
            if (string.Equals(optionData.text, newValue)){
                dropdown.value = dropdown.options.IndexOf(optionData);
            }
        }

        // Update flag indicating that the value has changed
        valueIsSet = true;
    }

    // Clears the form field value
    protected override void Clear(){
        // Update flag indicating that the value has changed
        valueIsSet = false;

        // Checks if dropdown was not found
        if (dropdown == null)
            return;

        dropdown.value = dropdown.options.IndexOf((TMP_Dropdown.OptionData) initialValue);
    }

    // Enables the form field
    protected override void Enable(){
        // Checks if dropdown was not found
        if (dropdown == null)
            return;

        // Check if dropdown field is interactable
        if (isInteractable)
            dropdown.interactable = true;
    }

    // Disables the form field
    protected override void Disable(){
        // Checks if dropdown was not found
        if (dropdown == null)
            return;

        dropdown.interactable = false;
    }
}