using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/*
Abstract class for the form field.
This will allow to get, edit and view the form field information.
*/
public abstract class FormField: MonoBehaviour
{
    [SerializeField] private string fieldId;

    public bool valueIsSet { get; protected set; }

    protected object initialValue;
    protected bool isInteractable;

    // Returns the field identifier
    public string GetFieldId(){ return fieldId; }

    // Abstract method that will be implemented by subclasses
    // Returns the field value(s)
    public abstract object GetValue();

    // Executes the create functionality
    public void Create(){
        Clear();
        Enable();
    }

    // Executes the view functionality
    public void View(object value){
        Clear();
        SetValue(value);
        Disable();
    }

    // Executes the edit functionality
    public void Edit(){
        Enable();
    }

    // Abstract method that will be implemented by subclasses
    // Sets the field value(s)
    protected abstract void SetValue(object value);

    // Abstract method that will be implemented by subclasses
    // Clears the form field value
    protected abstract void Clear();

    // Abstract method that will be implemented by subclasses
    // Enables the form field
    protected abstract void Enable();

    // Abstract method that will be implemented by subclasses
    // Disables the form field
    protected abstract void Disable();
}