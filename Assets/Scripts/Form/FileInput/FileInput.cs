using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

#if UNITY_STANDALONE
    using SimpleFileBrowser;
#endif

public class FileInput : MonoBehaviour
{
    [SerializeField] private string typeName = "All Files";
    [SerializeField] private string [] extensions = new string[] {".*"};
    [SerializeField] private int fileSizeLimit = 0;

    private Button fileInputButton;
    private Button clearButton;
    private TMP_InputField fileName;
    private string filePath = null;

    public bool interactable { 
        set{
            // Changes button and fileName state depending on the value
            fileInputButton.gameObject.SetActive(value);
            clearButton.gameObject.SetActive(value);
            fileName.interactable = value;
        } 
    }

    // Start is called before the first frame update
    void Start()
    {
        // Gets the gameObject of the file name
        GameObject fileNameGameObject = transform.Find("FileName").gameObject;
        // Gets the scripts that will display the file name
        fileName = fileNameGameObject.GetComponent<TMP_InputField>();
        // Gets the file input button
        fileInputButton = transform.Find("Button").GetComponent<Button>();
        // Gets the clear button
        clearButton = transform.Find("FileName/ClearButton").GetComponent<Button>();
    }

    // Clears the file input field value
    public void Clear(){
        // Checks if the fileName is valid
        if(fileName == null){
            // Run Start method to initialize the gameObject
            Start();
        }
        // Clears the file name
        fileName.text = "";
        // Clears the file name
        filePath = null;
    }

    // Opens the file browser
    public void OpenFileBrowser(){
        #if UNITY_STANDALONE
            // Set the filters to filter the desired file
            FileBrowser.SetFilters(false, new FileBrowser.Filter(typeName, extensions));
            // Show the file panel
            FileBrowser.ShowLoadDialog(OnSuccess, null, FileBrowser.PickMode.Files, false, title:"Select a file", loadButtonText:"Select" );
        #endif

        #if UNITY_WEBGL
            // TODO: Not implemented
        #endif
    }

    #if UNITY_STANDALONE
        void OnSuccess(string[] paths){
            string path = paths[0];

            // Checks if the path is not valid
            if (string.IsNullOrEmpty(path))
                return;

            // Checks if the file size if respected
            if (!CheckFileSize(path)){
                // Send warning to user
                fileName.text = "The File is too big! The size limit is 5MB.";
                return;
            }

            // Sets the file path
            filePath = path;
            // Sets the file name
            fileName.text = Path.GetFileName(path);
        }
    #endif

    // Returns the file path
    public string GetValue(){
        return filePath;
    }

    // Sets the file name from incoming path
    public void SetValue(string path){
        // If the path is already complete
        if (System.IO.File.Exists(path)){
            // Sets the file name
            fileName.text = Path.GetFileName(path);
            // Sets the file path
            filePath = path;
            
            return;
        }

        // Builds a new path 
        string newPath = Application.streamingAssetsPath + "/" + path;

        // If the path needs to add the beginning
        if (System.IO.File.Exists(newPath)){
            // Sets the file name
            fileName.text = Path.GetFileName(newPath);
            // Sets the file path
            filePath = newPath;

            return;
        }

    }

    private bool CheckFileSize(string path){
        FileInfo fi = new FileInfo(path);

        // If the file exists
        if (fi.Exists){
            // Get file size in Bytes
            long size = fi.Length;
            
            // If the size limit is zero, there is no limit
            if(fileSizeLimit == 0){
                return true;
            }

            // Checks if the file size is respected
            if(size <= fileSizeLimit)
                return true;
        }

        return false;
    }
}
