using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FileOrURLInputBody : MonoBehaviour
{
    private TMP_Dropdown dropdown;
    private GameObject urlInputField;
    private GameObject fileInputField;

    // Start is called before the first frame update
    void Start()
    {
        // Gets the child tranform with the name "Dropdown"
        Transform dropdownTransform = transform.Find("Body/Dropdown");

        // Gets the TMP_Dropdown component from the Dropdown gameObject
        dropdown = dropdownTransform.gameObject.GetComponent<TMP_Dropdown>();

        // Gets the child gameObject with the name "URL" and "UploadFile"
        urlInputField = transform.Find("Body/URL").gameObject;
        // fileInputField = transform.Find("Body/UploadFile").gameObject;

        SwitchBetweenInputField();
    }

    // Returns the field value(s)
    public PathOp GetValue(){
        PathOp value = new PathOp();

        // If the dropdown value has changed to "URL"
        if (dropdown.options[dropdown.value].text == "URL"){
            // Disable url input field
            string url = urlInputField.GetComponent<TMP_InputField>().text;

            // if the path is not valid
            if (string.IsNullOrEmpty(url)){
                return null;
            }

            // Updates the url
            // value.url = url;

             // Updates the path
            value.path = url;
            return value;
        
        // If the dropdown value has changed to "Upload File"
        }else if (dropdown.options[dropdown.value].text == "Upload File"){
            // Disable file input field
            string path = fileInputField.GetComponent<FileInput>().GetValue();
            
            // if the path is not valid
            if (string.IsNullOrEmpty(path)){
                return null;
            }

            // Updates the path
            value.path = path;
            return value;
        }
        
        return null;
    }

    // Sets the field value(s)
    public void SetValue(PathOp value){
        // If path and url are not set
        // if (string.IsNullOrEmpty(value.path) && string.IsNullOrEmpty(value.url)){
        //     // Change dropdown value to default
        //     dropdown.value = 0;
        // // If url is not set
        // }else if (string.IsNullOrEmpty(value.url)){
        //     // Change dropdown value to path = 1
        //     dropdown.value = 1;
        //     // Sets the file value
        //     fileInputField.GetComponent<FileInput>().SetValue(value.path);
        // // If path is not set
        // }else if (string.IsNullOrEmpty(value.path)){
        //     // Change dropdown value to url = 2
        //     dropdown.value = 2;
        //     // Sets the url value
        //     urlInputField.GetComponent<TMP_InputField>().text = value.url;
        // }

        // If path and url are not set
        if (string.IsNullOrEmpty(value.path)){
            // Change dropdown value to default
            dropdown.value = 0;
        }

        // Change dropdown value to url = 1
        dropdown.value = 1;
        // Sets the url value
        urlInputField.GetComponent<TMP_InputField>().text = value.path;
    }

    // Clears the form field value
    public void Clear(){
        // Checks if the dropdown is not valid
        if(dropdown == null){
            // Run Start method to initialize this gameObject
            Start();
        }
        // Clear dropdown
        dropdown.value = 0;

        // Switch to the initial state
        SwitchBetweenInputField();

        // Clear url input field
        urlInputField.GetComponent<TMP_InputField>().text = "";
        // Clear file input field
        // fileInputField.GetComponent<FileInput>().Clear();
    }

    // Enables the form field
    public void Enable(){
        //Enable dropdown
        dropdown.interactable = true;
        // Enable url input field
        urlInputField.GetComponent<TMP_InputField>().interactable = true;
        // Enable file input field
        // fileInputField.GetComponent<FileInput>().interactable = true;
    }

    // Disables the form field
    public void Disable(){
        //Disable dropdown
        dropdown.interactable = false;
        // Disable url input field
        urlInputField.GetComponent<TMP_InputField>().interactable = false;
        // Disable file input field
        // fileInputField.GetComponent<FileInput>().interactable = false; 
    }

    // Switchs between the file and url input field
    public void SwitchBetweenInputField(){
        // If the dropdown value has changed to "URL"
        if (dropdown.options[dropdown.value].text == "URL"){
            urlInputField.SetActive(true);
            // fileInputField.SetActive(false);
            return;
        // If the dropdown value has changed to "Upload File"
        }else if (dropdown.options[dropdown.value].text == "Upload File"){
            urlInputField.SetActive(false);
            // fileInputField.SetActive(true);
            return;
        }
        // If the dropdown value has changed to "None"
        urlInputField.SetActive(false);
        // fileInputField.SetActive(false);
    }
}
