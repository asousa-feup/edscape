using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class IntegerInputField : FormField
{
    private TMP_InputField integerInputField;

    void Start(){
        // Gets the child tranform with the name "InputField"
        Transform integerInputFieldTransform = transform.Find("InputField");

        // Checks if integerInputFieldTransform was not found
        if (integerInputFieldTransform == null)
            return;

        // Gets the TMP_InputField component from the InputField gameObject
        integerInputField = integerInputFieldTransform.gameObject.GetComponent<TMP_InputField>();

        // Checks if integerInputField was not found
        if (integerInputField == null)
            return;

        // Gets the initial state of the integer input field
        initialValue = integerInputField.text;
        isInteractable = integerInputField.interactable;
    }

    // Returns the field value(s)
    public override object GetValue(){
        // Checks if integerInputField was not found
        if (integerInputField == null)
            return "";
        
        int integerValue;
        bool isParsable = int.TryParse(integerInputField.text, out integerValue);

        // Checks if integerInputField's text can be converted to a integer
        if (isParsable)
            return integerValue;
        
        return 0;
    }

    // Sets the field value(s)
    protected override void SetValue(object value){
        // Checks if integerInputField was not found
        if (integerInputField == null){
            // Update flag indicating that the value has changed
            valueIsSet = true;
            return;
        }

        // Variable that will store the value if the cast dont throw an error
        string newValue;

        // Encapsulate cast so the code dont crash 
        try {
            newValue = ((int) value).ToString();
        }catch (Exception){
            // if something goes wrong set to the initial value
            newValue = (string) initialValue; 
        }

        integerInputField.text = newValue;

        // Update flag indicating that the value has changed
        valueIsSet = true;
    }

    // Clears the form field value
    protected override void Clear(){
        // Update flag indicating that the value has changed
        valueIsSet = false;

        // Checks if integerInputField was not found
        if (integerInputField == null)
            return;

        integerInputField.text = (string) initialValue;
    }

    // Enables the form field
    protected override void Enable(){
        // Checks if integerInputField was not found
        if (integerInputField == null)
            return;

        // Check if integer input field is interactable
        if (isInteractable)
            integerInputField.interactable = true;
    }

    // Disables the form field
    protected override void Disable(){
        // Checks if integerInputField was not found
        if (integerInputField == null)
            return;

        integerInputField.interactable = false;
    }
}
