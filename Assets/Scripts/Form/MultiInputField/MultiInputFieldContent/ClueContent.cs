using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClueContent : MultiInputFieldContent
{
    // Returns the field value(s)
    public override object GetValue(){
        // Get ClueObj class Type
        Type contentType = typeof(ClueObj);
        ClueObj content = new ClueObj();

        // Assing form values to each class field
        // Loops through all form fields
        foreach (var formField in formFields)
        {
            // Search for a field in the class with the same name as form field key
            FieldInfo contentField = contentType.GetField(formField.Key);
            
            // if was found
            if (contentField != null){
                contentField.SetValue(content, formField.Value.GetValue());
            }
        }

        return content;
    }

    // Sets the field value(s)
    public override void SetValue(object value){
        // Checks if the value is valid
        if (value == null)
            return;
        
        // Encapsulate cast so the code dont crash 
        try {
            // Get ClueObj class Type
            Type contentType = typeof(ClueObj);
            ClueObj clueObj = (ClueObj) value;
            
            // Assing values to formFields
            // Loops through all form fields
            foreach (var formField in formFields)
            {
                // Search for a field in the class with the same name as form field key
                FieldInfo contentField = contentType.GetField(formField.Key);
                
                // if was found
                if (contentField != null){
                    formField.Value.View(contentField.GetValue(clueObj));
                }
            }
        
            Disable();

        }catch (Exception){
            // if something goes wrong
            return;
        }
    }
}
