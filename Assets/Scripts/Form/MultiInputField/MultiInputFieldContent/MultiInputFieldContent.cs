using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MultiInputFieldContent : MonoBehaviour
{
    protected Dictionary <string, FormField> formFields;
    private MultiInputField multiInputField;
    private GameObject header;

    // Start is called before the first frame update
    void Start()
    {   
        formFields = new Dictionary <string, FormField>();

        // Get all the form fields
        for (int i = 1; i < transform.childCount; i++){
            // Check if the gameObject is active
            if (transform.GetChild(i).gameObject.activeSelf){
                FormField field = transform.GetChild(i).GetComponent<FormField>();
                formFields[field.GetFieldId()] = field;
            }
        }
        
        // Get multiInputField from the parent
        multiInputField = transform.parent.GetComponent<MultiInputField>();
        // Get header
        header = transform.Find("Header").gameObject;
    }

    // Returns the field value(s)
    public abstract object GetValue();

    // Sets the field value(s)
    public abstract void SetValue(object value);

    // Enables the form field
    public void Enable(){
        // Loop through all the form fields and call Edit function
        // Edit function calls form field enable
        foreach (var formField in formFields)
        {
            formField.Value.Edit();
        }

        // Activate Header
        header.SetActive(true);
    }

    // Disables the form field
    public void Disable(){
        // Deactivate Header
        header.SetActive(false);
    }

    // Deletes this gameObject
    public void Delete(){
        multiInputField.RemoveContent(gameObject);
    }
}
