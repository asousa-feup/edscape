using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveObjectContent : MultiInputFieldContent
{
    // Returns the field value(s)
    public override object GetValue(){
        // Get IterativeObj class Type
        Type contentType = typeof(IterativeObj);
        IterativeObj content = new IterativeObj();

        // Assing form values to each class field
        // Loops through all form fields
        foreach (var formField in formFields)
        {
            // Search for a field in the class with the same name as form field key
            FieldInfo contentField = contentType.GetField(formField.Key);
            
            // if was found
            if (contentField != null){
                contentField.SetValue(content, formField.Value.GetValue());
            }
        } 

        return content;
    }

    // Sets the field value(s)
    public override void SetValue(object value){
        // Checks if the value is valid
        if (value == null)
            return;
        
        // Encapsulate cast so the code dont crash 
        try {
            // Get IterativeObj class Type
            Type contentType = typeof(IterativeObj);
            IterativeObj iterativeObj = (IterativeObj) value;

            // Assing values to formFields
            // Loops through all form fields
            foreach (var formField in formFields)
            {
                // Search for a field in the class with the same name as form field key
                FieldInfo contentField = contentType.GetField(formField.Key);
                
                // if was found
                if (contentField != null){
                    formField.Value.View(contentField.GetValue(iterativeObj));
                }
            }

            Disable();

        }catch (Exception){
            // if something goes wrong
            return;
        }
    }
}
