using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class MultiInputField : FormField
{
    protected List<GameObject> multiInputFieldContents;
    private GameObject footer;

    // Start is called before the first frame update
    void Start()
    {
        multiInputFieldContents = new List<GameObject>();

        // Loop through all the childs and add them to the multiInputField less the add button
        for (int i = 0; i < (transform.childCount-1); i++){
            multiInputFieldContents.Add(transform.GetChild(i).gameObject);
        }

        // Get footer
        footer = transform.GetChild(transform.childCount-1).gameObject;
        // Set the initial form state
        isInteractable = footer.transform.Find("Button").GetComponent<Button>().interactable;
    }

    // Returns the field value(s)
    public override object GetValue(){ return null; }

    // Sets the field value(s)
    protected override void SetValue(object value){}

    protected IEnumerator SetValueCoroutine<T>(List<T> values){
        // Loop through all the values
        foreach(var newValue in values){
            int layoutIndex = transform.childCount - 1;

            // Create a new child to display the value
            transform.GetComponent<InstantiateFormFields>().InstantiateFormField(layoutIndex);
            // Refresh vertical layout
            transform.GetComponent<LayoutRefresh>().Refresh();

            // Get the new child and set it to the new value
            GameObject newChild = transform.GetChild(layoutIndex).gameObject;

            // Wait for the new child to start
            yield return null;

            newChild.GetComponent<MultiInputFieldContent>().SetValue(newValue);
            
            // Add the new child to the list
            multiInputFieldContents.Add(newChild);
        }

        // Update flag indicating that the value has changed
        valueIsSet = true;
    }

    // Clears the form field value
    protected override void Clear(){
        // Update flag indicating that the value has changed
        valueIsSet = false;

        // Destroy all the gameObjects
        foreach(GameObject multiInputFieldContent in multiInputFieldContents){
            Destroy(multiInputFieldContent);
        }

        // Clear list
        multiInputFieldContents = new List<GameObject>();
    }

    // Enables the form field
    protected override void Enable(){
        // Check if multi input field is interactable
        if (isInteractable){
            // Loops through all the chidls and enables them
            foreach(GameObject multiInputFieldContent in multiInputFieldContents){
                multiInputFieldContent.GetComponent<MultiInputFieldContent>().Enable();
            }

            // Activate add button
            footer.SetActive(true);
        }
    }

    // Disables the form field
    protected override void Disable(){
        // Deactivate add button
        footer.SetActive(false);
    }

    // Adds a new content
    public void AddContent(){
        int layoutIndex = transform.childCount - 1;

        // Create a new child to display the value
        transform.GetComponent<InstantiateFormFields>().InstantiateFormField(layoutIndex);
        // Refresh vertical layout
        transform.GetComponent<LayoutRefresh>().Refresh();

        // Get the new child and set it to the new value
        GameObject newChild = transform.GetChild(layoutIndex).gameObject;
        
        // Add the new child to the list
        multiInputFieldContents.Add(newChild);
    }

    // Removes a specific content
    public void RemoveContent(GameObject content){
        multiInputFieldContents.Remove(content);
        Destroy(content);
    }
}
