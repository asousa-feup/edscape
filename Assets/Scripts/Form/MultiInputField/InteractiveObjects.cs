using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveObjects : MultiInputField
{
    // Returns the field value(s)
    public override object GetValue(){
        List<IterativeObj> values = new List<IterativeObj>();

        // Loops through all the chidls and gets their values
        foreach(GameObject multiInputFieldContent in multiInputFieldContents){
            values.Add((IterativeObj)multiInputFieldContent.GetComponent<MultiInputFieldContent>().GetValue());
        }

        return values;
    }

    // Sets the field value(s)
    protected override void SetValue(object value){
        // Checks if the value is valid
        if (value == null){
            // Update flag indicating that the value has changed
            valueIsSet = true;
            return;
        }

        // Encapsulate cast so the code dont crash 
        try {
            List<IterativeObj> values = (List<IterativeObj>) value;
            StartCoroutine(SetValueCoroutine(values));
        }catch (Exception){
            // if something goes wrong
            // Update flag indicating that the value has changed
            valueIsSet = true;
            return;
        }
    }
}
