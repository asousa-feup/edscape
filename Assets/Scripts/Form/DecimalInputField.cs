using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DecimalInputField : FormField
{
    [SerializeField] bool convertToSeconds;

    private TMP_InputField decimalInputField;

    void Start(){
        // Gets the child tranform with the name "InputField"
        Transform decimalInputFieldTransform = transform.Find("InputField");

        // Checks if decimalInputFieldTransform was not found
        if (decimalInputFieldTransform == null)
            return;

        // Gets the TMP_InputField component from the InputField gameObject
        decimalInputField = decimalInputFieldTransform.gameObject.GetComponent<TMP_InputField>();

        // Checks if decimalInputField was not found
        if (decimalInputField == null)
            return;
        
        // Gets the initial state of the decimal input field
        initialValue = decimalInputField.text;
        isInteractable = decimalInputField.interactable;
    }

    // Returns the field value(s)
    public override object GetValue(){
        // Checks if decimalInputField was not found
        if (decimalInputField == null)
            return "";
        
        float floatValue;
        bool isParsable = float.TryParse(decimalInputField.text, out floatValue);

        // Checks if decimalInputField's text can be converted to a float
        if (isParsable){
            // Checks if is necessary to convert to minutes
            if (convertToSeconds){
                return floatValue * 60f;
            }
            return floatValue;
        }
        
        return 0f;
    }

    // Sets the field value(s)
    protected override void SetValue(object value){
        // Checks if decimalInputField was not found
        if (decimalInputField == null){
            // Update flag indicating that the value has changed
            valueIsSet = true;
            return;
        }

        // Variable that will store the value if the cast dont throw an error
        string newValue;

        // Encapsulate cast so the code dont crash 
        try {
            float tempValue = (float) value;
            
            // Check if the convertToSeconds flag is true
            if (convertToSeconds){
                tempValue = tempValue / 60f;
            }

            newValue = tempValue.ToString();
        }catch (Exception){
            // if something goes wrong set to the initial value
            newValue = (string) initialValue;
        }

        decimalInputField.text = newValue;

        // Update flag indicating that the value has changed
        valueIsSet = true;
    }
    
    // Clears the form field value
    protected override void Clear(){
        // Update flag indicating that the value has changed
        valueIsSet = false;

        // Checks if decimalInputField was not found
        if (decimalInputField == null)
            return;

        decimalInputField.text = (string) initialValue;
    }

    // Enables the form field
    protected override void Enable(){
        // Checks if decimalInputField was not found
        if (decimalInputField == null)
            return;

        // Check if decimal input field is interactable
        if (isInteractable)
            decimalInputField.interactable = true;
    }
    
    // Disables the form field
    protected override void Disable(){
        // Checks if decimalInputField was not found
        if (decimalInputField == null)
            return;

        decimalInputField.interactable = false;
    }
}
