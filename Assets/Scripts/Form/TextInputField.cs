using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextInputField : FormField
{
    [SerializeField] private bool translateValue = false;

    private TMP_InputField textInputField;
    private LanguageTranslator languageTranslator;
    private FormField availableLanguages;

    void Start(){
        // Gets the child tranform with the name "ResizableInputField"
        Transform textInputFieldTransform = transform.Find("ResizableInputField");

        // Checks if textInputFieldTransform was not found
        if (textInputFieldTransform == null)
            return;

        // Gets the TMP_InputField component from the ResizableInputField gameObject
        textInputField = textInputFieldTransform.gameObject.GetComponent<TMP_InputField>();

        // Checks if textInputField was not found
        if (textInputField == null)
            return;

        // Checks if the inputed value is to be translated 
        if (translateValue){
            languageTranslator = new LanguageTranslator();

            // Search for the form field responsible for the selection of the available languages
            foreach (GameObject formField in GameObject.FindGameObjectsWithTag("FormField")){
                FormField formFieldScript = formField.GetComponent<FormField>();

                // Checks if the script was found and if the field id equals to "textOptionIDs"
                if( (formFieldScript != null) && (formFieldScript.GetFieldId() == "textOptionIDs") ){
                    availableLanguages = formFieldScript;
                    break;
                }
            }
        }   

        // Gets the initial state of the text input field
        initialValue = textInputField.text;
        isInteractable = textInputField.interactable;
    }

    // Returns the field value(s)
    public override object GetValue(){
        // Checks if textInputField was not found
        if (textInputField == null)
            return "";

        // Checks if textInputField is translatable
        if (translateValue){
            return GetTranslatedValue();
        }

        return textInputField.text.Trim();
    }

    // Returns a list of different values in every available language
    private List<TextOp> GetTranslatedValue(){
        List<TextOp> translations = new List<TextOp>();
        // Gets the available languages
        List<string> languages = (List<string>) availableLanguages.GetValue();
        
        // Gets the original value
        string value = textInputField.text.Trim();

        // Checks if the value is empty
        if (value == ""){
            return translations;
        }
        
        // Adds to the list the original value
        TextOp originalValue = new TextOp();
        originalValue.id = "";
        originalValue.text = value;

        translations.Add(originalValue);

        // Checks if the list of languages is empty or null
        if ( (languages.Count == 0) || (languages==null) ){
            return translations;
        }

        // Translate to other languages
        foreach (string language in languages){
            // Gets the translation
            TextOp textOp = languageTranslator.Translate(value, language);

            // Checks is something went wrong
            if (textOp == null){
                continue;
            }
            
            // Adds the translation to the list
            translations.Add(textOp);
        }

        return translations;
    }

    // Sets the field value(s)
    protected override void SetValue(object value){
        // Checks if textInputField was not found
        if (textInputField == null){
            // Update flag indicating that the value has changed
            valueIsSet = true;
            return;
        }

        // Checks if textInputField is translatable
        if (translateValue){
            SetTranslatedValue(value);
            // Update flag indicating that the value has changed
            valueIsSet = true;
            return;
        }

        // Variable that will store the value if the cast dont throw an error
        string newValue;

        // Encapsulate cast so the code dont crash 
        try {
            newValue = (string) value;
        }catch (Exception){
            // if something goes wrong set to the initial value
            newValue = (string) initialValue; 
        }

        textInputField.text = newValue;

        // Update flag indicating that the value has changed
        valueIsSet = true;
    }

    // Sets the value(s) when textInputField is translatable
    private void SetTranslatedValue(object value){
        // Checks if the value is valid
        if (value == null)
            return;

        // Variable that will store the value if the cast dont throw an error
        List<TextOp> values;

        // Encapsulate cast so the code dont crash 
        try {
            values = (List<TextOp>) value;
        }catch (Exception){
            // if something goes wrong set to the initial value
            values = new List<TextOp>();
        }

        // Checks if the list is empty
        if(values.Count == 0){
            return;
        }

        // Set textInputField with the first value of the list
        // First element of the list corresponds to the original version inserted by the user
        textInputField.text = values[0].text;
    }

    // Clears the form field value
    protected override void Clear(){
        // Update flag indicating that the value has changed
        valueIsSet = false;

        // Checks if textInputField was not found
        if (textInputField == null)
            return;

        textInputField.text = (string) initialValue;
    }

    // Enables the form field
    protected override void Enable(){
        // Checks if textInputField was not found
        if (textInputField == null)
            return;

        // Check if text input field is interactable
        if (isInteractable)
            textInputField.interactable = true;
    }

    // Disables the form field
    protected override void Disable(){
        // Checks if textInputField was not found
        if (textInputField == null)
            return;

        textInputField.interactable = false;
    }
}
