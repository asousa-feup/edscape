# EdScape - Framework for Learning with Virtual Escape Rooms - Unity Game
 
EdScape is a framework developed by a team with the objective of giving less technical educators a flexible way to create Immersive Educational Escape Rooms (IEER). 

The framework's base already provides a Unity online game and a [Web app](https://web.fe.up.pt/~efeupinho/web_badges/pages/formLogin.php) that is constantly connected to it to exchange data.

EdScape Unity game is fully portable to PC and Web browser.


## Technology
Unity Game Engine (version 2020.3.30f1).


## Team

|Name| Course from DEI| Role |
|--|--|--|
| Ana Rita Garcia | MM| IEER World Building, Acessibility and Storytelling
| Ana Rita Santos | M.EIC| Co-Developer: Ethics in Engineering
| António Coelho | | Supervisor
| Armando Jorge Sousa | | Supervisor
| Celina Mo Zheng | M.EEC | Co-Developer: Ethics in Engineering
| Diana Sousa | PDMD | Co-Supervisor and Researcher on IEER
| Fátima Coelho | | Researcher on Teaching Ethics in Engineering
| Manuel Firmino | | Supervisor
| Marcelo Reis | M.EIC | IEER Co-Developer: Platform for Educators
| Tiago Rossini | MM| IEER Co-Developer: PCG and Level Design


