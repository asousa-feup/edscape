# Changelog

## (2022-08-18) 0.0.17
- Increase radius of interacting with items
- Add different language options to the messages, videos and mini-games in the same main game JSON file
- Create two simple use cases for educators understand better the JSON files structure
- [fix] Log file messages according to the language chosen
- [fix] Time ended message (in WebGl time ended before enter the game)
- [fix] Wait 0.5 seconds after coming back to the main game to not ask for a clue accidentally because they clicked in the forward button before

## (2022-06-07) 0.0.16
- Add option to change space of the game
- Add a new case study "Broken Timeline" assets and JSONs
- Add option to choose from random main games in setting JSON file
- Add option to enter in a specific level after interacting with an item
- Update clues system:
    - clues associated to a level
    - option to destroy after showing once
    - suppress next clues if desired


## (2022-06-03) 0.0.15 [M.EIC|M.EEC] Test Case Study "Ethics 101 Text"
- Add audio to the main game
- Update log file at every new event on the game
- Add test boolean on settings JSON to ignore surveys
- Put global time as real time of the PC
- [fix] Title overflow on quiz questions

## (2022-05-26) 0.0.14 [MM] Test Case Study "Ethics 101 Text"
- [fix] Change icon of informative button
- [fix] Update pdf viewer with door icon to go back
- [fix] Panel end mini game (puzzle and label): move to the side, feedback message in yellow, 
- [fix] Quiz: Change feedback message to a button
- When player receives a message scroll down and add an alert of a new notification
- Set label contents randomly and best positions according to the number of labels
- Change scenes order: Informative panel -> Intro panels (text/video) -> Start game with a message -> 3D game
- Update case study "Ethics 101 Text" JSONs:
    - Books on the floor
    - Penalty to -1
    - Informative panel with game controls and penalties information
    - First clue being about reading the documents


## (2022-05-25) 0.0.13 
- Set active the red background on time when it's over
- UI player messages: remove time + green scroll bar
- Add a back button to feedback panel
- Update puzzle end game panel to a more transparent one to see puzzle finished
- Add informative button in left corner
- Divide end main game panel in two: 
    - Congratulate player for finishing game panel
    - Final menu panel with also a specific message, badges earned button, restart button to go to login, and exit application button

## (2022-05-24) 0.0.12 
- Add a survey as a parameter in main game JSON file :
    - Between Login and choosing game scenes
    - After end main game panel
- Redirect to badges earned in final menu panel with a button and a link
- Appear the intro panels (text/video) on the mini game side only if wins mini-game
- Update choose game scene (add logo and new buttons)
- Update all game buttons to the new ones
- Update global colors of the game and mini-games with the new palette colors
- Add skybox
- Add time ended message as a parameter in main game JSON file and a animation to it
- Add thoughts
- [fix] Integration of new images with different dimensions on puzzle
- [fix] Feedback on clues and exit buttons of mini-games as a JSON parameter and their overflow text
- [fix] Add replay button on mini-games
- [fix] Add load panel before choosing game scene
- [fix] Overflow of text panel and other messages (add a scroll bar)
- [fix] Change arrow direction in pdf viewer
- Add case study "Ethics 101 Test":
    - Integrate the new items
    - Create hallway 
    - Create the JSON files of the main game and mini-games

## (2022-05-18) 0.0.11
- Add Login scene
- Log file:
    - Send to database
    - [fix] Change date and time format
    - Add teacher e-mail + student e-mail
- Label: put title and feedback as a parameter in JSON mini-game and the score as x/x to not have words
- Put inventory up to 10 items
- [fix] Pdf Viewer: search doesn't work on WebGL, so remove button if in WebGL
- [fix] Puzzle 4x4 pieces transform values
- [fix] Office Lab scene because of interactions with the camera and player (wall in the door + move away chair)
- [fix] Overflow in intro panel text 

## (2022-05-11) 0.0.9 
- Turn off automatic movement if player bumps into something
- Remove the remaining text buttons for icons
- Increase distance tolerance in Label
- Create a global controller for the mini games
- [fix] resolution in:
    - Quiz buttons
    - Intro text panel
    - Videos
- [fix] Quiz end panel appears at start and penalty exit time does not work
- [fix] Load panel in pdf with different ball size due to different resolution
- [fix] Negative time in the countdown
- [fix] Label game bug of grabbing a piece and time ends 
- [fix] Wait for video to finish and close panel

## (2022-05-04) 0.0.8  
- Improve camera:
    - [fix] Put the camera closer to the player's head
    - [fix] Don't go beyond walls and player
    - [fix] Smoother camera pov change
- Improve player movement:
    - In addition to 'W' add 'WASD' in 1st person camera
    - Walk to the sides or rotate with 'AS' in 1st person camera (boolean)
    - Left mouse click to move in that direction
- Put the intro panelS (text/video) as a parameter on the side of the mini-game quiz
- Finish the mini games and redirect them automatically to the end game panel
- Remove end button in mini games

## (2022-04-27) 0.0.7 
- Add load canvas to the mini games
- Add animation to time when it is penalized
- Put the intro panels (text/video) as a parameter, both on the side of the mini-games (puzzle/label/pdf) and on the item.
- Create log file in filesystem of the game

## (2022-04-21) 0.0.6 
- Interact with items even at a distance (trigger the yellow outline when passing the cursor)
- Add items: 
    - "Paper" (can be picked up to inventory) 
    - "Laptop"
- Integration of **label** and **pdf viewer**
- Choose from different mini games of the same type according to path
- Put the puzzle image as a parameter in JSON mini-game 
- Place the label image and instantiate slots/pieces through coordinates in the JSON mini-game 
- Add specific penalty for giving up (exited)
    - Change penalization's to:
        - Exited: exit penalty + no object
        - Doesn't completes it and exits: exit penalty + no object
        - Completes it successfully without clues: take object + no penalty
        - Completes it successfully but used clues or got wrong answers: take item + fail penalty
    - Change messages to:
        - Message Succeeded
        - Message Succeeded With Help
        - Message Exit
- Confirm that the Mini Games are integrated, an example of each:
    - Quiz: "Key"
    - Puzzle: "Coin"
    - Label: "Paper"
    - PDF Viewer: "Laptop"

## (2022-04-14) 0.0.5 
- Choose videos from streaming assets folder with path
- Add global time to mini games
- Pick item only if player didn't exited
- Add a red alert of 3 seconds when player receives a time penalization
- [fix] Click multiple times on a item and don't let load multiple mini-games
- [fix] Disable movement of player when they aren't in the main room
- [fix] Change time penalization's without words (-x s)


## (2022-04-06) 0.0.4 
- Add new main game configurations to the JSON file:
    - "introductionText": add text introduction before game starts
    - "congratulationsText": add final congratulation text
- Add option to choose from different main json games
    - read folder "RoomGame" and display games available
- Change buttons with text to icons
- [fix] Puzzle integration by adding a camera child and layers

## (2022-04-01) 0.0.3 
- Add new main game configurations to the JSON file:
    - "videoId": add videos to items
- Add answers and score to log contents
- Interact only with near items (inside radius)
- Add rotation y to camera
- Add items:
    - "coin" (can be picked up to inventory) 
- Integration of mini game **puzzle** in coin
- Add 1st person camera, changing cameras with 'V'
- [fix] Stop time when videos are played

## (2022-03-23) 0.0.2
- Improve Clues:
    - Change structure (by order and if not contains item)
    - Show clues in the player UI messages 
    - Add button to ask for a clue
- Add new main game configurations to the JSON file:
    - "winsGame"
    - "finalPosition", "finalRotation": move items if interacted with success
- Add items:
    - "TrashBin"
    - "TrashCan"
- Hide key underneath the trash can
- [fix] Disable multiple event systems when changing to mini-games
- [fix] Quiz buttons parent 
- [fix] Messages of succeeded and failed msg + penalty


## (2022-03-16 Wednesday) 0.0.1 
- Add yellow outline + radius that limits interaction
- Interact with items using mouse
- Add countdown
- Add clues related to items
- Change objects to addressables in local hosting
- JSON with the main game configurations:
    - "initialTime"
    - "clueObjs"
    - "interactiveObjs"
    - "game2DScene": integration of mini game **quiz** that wins key
    - "messageSucceeded"
    - "messageFailed"

## (2022-03-09 Wednesday) 0.0.0 
- First version of game:
    - Create office lab scene:
        - Player 
        - "Key" (can be picked up to inventory) 
        - "Door"
    - Player Movement (3rd person/ keyboard keys)
    - Interact with item using 'E'
    - Door opens only with key
    - End panel with menu

